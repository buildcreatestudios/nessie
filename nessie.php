<?php
    /*
    Plugin Name:            Nessie
    Plugin URI:             http://buildcreate.com
    Description:            A full featured events calendar, booking and ticketing solution
    Version:                2.8.0
    Author:                 buildcreate
    Author URI:             http://buildcreate.com
    Text Domain:            nes
    Contributors:           kraftpress, buildcreate, a2rocklobster
    License:                GNU General Public License v2
    License URI:            http://www.gnu.org/licenses/gpl-2.0.html
    Bitbucket Plugin URI:   https://bitbucket.org/buildcreatestudios/nessie
    */

    // If this file is called directly, abort.
    if(!defined('WPINC')){die;}

    // on activation
    register_activation_hook( __FILE__, 'nes_import_tickets_gform');

    // auto include nessie tickets form if it doesnt exist
    function nes_import_tickets_gform(){
        if(class_exists('GFFormsModel')){
            $nessie = new Nessie();
            $has_form = false;
            $forms = GFFormsModel::get_forms();
            if($forms){
                foreach($forms as &$form){
                    if($form->title == 'Nessie Tickets'){
                        $has_form = true;
                        $form_id = $form->id;
                        $nessie->nes_settings['default_ticket_form_id'] = $form->id;
                    }
                }
            }else{
                $has_form = true;
            }

            // create form if necessary
            if(!$has_form){
                GFExport::import_file($nessie->nes_settings['dir'].'nessie/inc/gform-export.json');
            }
        }
    }

    // the main class
    class Nessie {
        public $nes_settings;
        
        function __construct(){

            // ensure sessions are started
            if ((defined('PHP_SESSION_ACTIVE') && session_status() !== PHP_SESSION_ACTIVE) || !session_id()) {
                session_start();
            }

            // helpers
            add_filter('nes_get_path', array($this, 'nes_get_path'), 1, 1);
            add_filter('nes_get_dir', array($this, 'nes_get_dir'), 1, 1);

            // nessie vars
            $this->nes_settings = array(
                'version' => '2.8.0',
                'path' => apply_filters('nes_get_path', __FILE__),
                'dir' => apply_filters('nes_get_dir', __FILE__),
                'hook' => basename( dirname( __FILE__ ) ) . '/' . basename( __FILE__ ),
                'update_url' => 'http://buildcreate.com/nessie/version.txt',
                'venue_single' => __('Venue','nes'),
                'venue_plural' => __('Venues','nes'),
                'location_single' => __('Location','nes'),
                'location_plural' => __('Locations','nes'),
                'event_single' => __('Event','nes'),
                'event_plural' => __('Events','nes'),
                'default_venue' => '',
                'day_start_time' => '08:00',
                'day_end_time' => '22:00',
                'end_time_length_hr' => '0',
                'end_time_length_min' => '00',
                'end_time_min_length_hr' => '0',
                'end_time_min_length_min' => '00',
                'end_time_max_length_hr' => '0',
                'end_time_max_length_min' => '00',
                'end_time_minmax_interval' => 0.25,
                'end_time_type' => 'standard',
                'match_minmax_interval' => 'no',
                'require_login' => 'yes',
                'admin_new_notification' => 'yes',
                'admin_email_label_one' => __('Rentals','nes'),
                'admin_email_one' => get_option('admin_email'),
                'admin_email_label_two' => '',
                'admin_email_two' => '',
                'user_new_notification' => 'yes',
                'user_approved_notification' => 'yes',
                'from_email_name' => get_bloginfo('site_name'),
                'from_email_address' => 'noreply@'. preg_replace("(^https?://)", "", get_bloginfo('url')),
                'hide_admin_bar' => 'no',
                'show_admin_bar_for' => 'administrator',
                'user_subject_line_new' => '',
                'user_subject_line_approved' => '',
                'event_success_message' => '',
                'setup_cleanup' => 'yes',
                'conflict_checking' => 'yes',
                'show_event_details' => 'yes',
                'show_form_fields' => array('setup_time','cleanup_time','title','venue','location','phone','type','description','setup_needs','av_needs'),
                'title_label' => '',
                'name_label' => '',
                'email_label' => '',
                'phone_label' => '',
                'website_label' => '',
                'event_type_label' => '',
                'description_label' => '',
                'setup_needs_label' => '',
                'av_needs_label' => '',
                'offsite_venue_bg' => '666666',
                'offsite_venue_text' => 'ffffff',
                'pending_color' => '74ace8',
                'approved_color' => '43d94d',
                'denied_color' => 'e87f4a',
                'table_header_color' => 'dddddd',
                'unavailable_color' => 'cccccc',
                'custom_css' => '',
                'use_tabbed_layout' => 'yes',
                'use_tabbed_layout_follow' => 'yes',
                'event_content_area_selector' => '.page-content',
                'default_ticket_form_id' => '',
                'event_calendar_page' => '',
                'event_reservation_page' => '',
                'show_reservation_link' => 'yes',
                'max_events_per_day' => '5',
                'menu_visibility' => array(),
            );

            // set default if doesnt exist yet
            if (!get_option('nes_menu_visibility')) {
                $role_permissions = array();
                $menu_items = array('main','events','locations','venues','categories','settings','import','attendees');
                foreach ($menu_items as $item) {
                    $role_permissions["nes_menu_{$item}"] = array(
                        'administrator',
                        'editor',
                        'author',
                        'contributor',
                        'subscriber'
                    );
                }
                update_option('nes_menu_visibility', $role_permissions);
            }
            $this->nes_settings['menu_visibility'] = get_option('nes_menu_visibility'); 

            // get options
            $venue_single = get_option('nes_venue_single');
            if(!empty($venue_single)){$this->nes_settings['venue_single'] = $venue_single;}
            $venue_plural = get_option('nes_venue_plural');
            if(!empty($venue_plural)){$this->nes_settings['venue_plural'] = $venue_plural;}
            $location_single = get_option('nes_location_single');
            if(!empty($location_single)){$this->nes_settings['location_single'] = $location_single;}
            $location_plural = get_option('nes_location_plural');
            if(!empty($location_plural)){$this->nes_settings['location_plural'] = $location_plural;}
            $event_single = get_option('nes_event_single');
            if(!empty($event_single)){$this->nes_settings['event_single'] = $event_single;}
            $event_plural = get_option('nes_event_plural');
            if(!empty($event_plural)){$this->nes_settings['event_plural'] = $event_plural;}            
            $default_venue = get_option('nes_default_venue');
            if(!empty($default_venue)){$this->nes_settings['default_venue'] = $default_venue;} 
            $day_start_time = get_option('nes_day_start_time');
            if(!empty($day_start_time)){$this->nes_settings['day_start_time'] = $day_start_time;} 
            $day_end_time = get_option('nes_day_end_time');
            if(!empty($day_end_time)){$this->nes_settings['day_end_time'] = $day_end_time;} 
            
            $name_label = get_option('nes_name_label');
            if(!empty($name_label)){$this->nes_settings['name_label'] = $name_label;}
            $email_label = get_option('nes_email_label');
            if(!empty($email_label)){$this->nes_settings['email_label'] = $email_label;} 
            $title_label = get_option('nes_title_label');
            if(!empty($title_label)){$this->nes_settings['title_label'] = $title_label;} 
            $phone_label = get_option('nes_phone_label');
            if(!empty($phone_label)){$this->nes_settings['phone_label'] = $phone_label;}             
            $website_label = get_option('nes_website_label');
            if(!empty($website_label)){$this->nes_settings['website_label'] = $website_label;} 
            $event_type_label = get_option('nes_event_type_label');
            if(!empty($event_type_label)){$this->nes_settings['event_type_label'] = $event_type_label;} 
            $description_label = get_option('nes_description_label');
            if(!empty($description_label)){$this->nes_settings['description_label'] = $description_label;} 
            $setup_needs_label = get_option('nes_setup_needs_label');
            if(!empty($setup_needs_label)){$this->nes_settings['setup_needs_label'] = $setup_needs_label;} 
            $av_needs_label = get_option('nes_av_needs_label');
            if(!empty($av_needs_label)){$this->nes_settings['av_needs_label'] = $av_needs_label;} 

            $end_time_type = get_option('nes_end_time_type');
            if(!empty($end_time_type)){$this->nes_settings['end_time_type'] = $end_time_type;} 
            $end_time_length_hr = get_option('nes_end_time_length_hr');
            if(!empty($end_time_length_hr)){$this->nes_settings['end_time_length_hr'] = $end_time_length_hr;} 
            $end_time_length_min = get_option('nes_end_time_length_min');
            if(!empty($end_time_length_min)){$this->nes_settings['end_time_length_min'] = $end_time_length_min;} 
            $end_time_min_length_hr = get_option('nes_end_time_min_length_hr');
            if(!empty($end_time_min_length_hr)){$this->nes_settings['end_time_min_length_hr'] = $end_time_min_length_hr;} 
            $end_time_min_length_min = get_option('nes_end_time_min_length_min');
            if(!empty($end_time_min_length_min)){$this->nes_settings['end_time_min_length_min'] = $end_time_min_length_min;} 
            $end_time_max_length_hr = get_option('nes_end_time_max_length_hr');
            if(!empty($end_time_max_length_hr)){$this->nes_settings['end_time_max_length_hr'] = $end_time_max_length_hr;} 
            $end_time_max_length_min = get_option('nes_end_time_max_length_min');
            if(!empty($end_time_max_length_min)){$this->nes_settings['end_time_max_length_min'] = $end_time_max_length_min;} 
            $end_time_minmax_interval = get_option('nes_end_time_minmax_interval');
            if(!empty($end_time_minmax_interval)){$this->nes_settings['end_time_minmax_interval'] = $end_time_minmax_interval;} 
            $match_minmax_interval = get_option('nes_match_minmax_interval');
            if(!empty($match_minmax_interval)){$this->nes_settings['match_minmax_interval'] = $match_minmax_interval;} 

            $require_login = get_option('nes_require_login');
            if(!empty($require_login)){$this->nes_settings['require_login'] = $require_login;}
            $admin_new_notification = get_option('nes_admin_new_notification');
            if(!empty($admin_new_notification)){$this->nes_settings['admin_new_notification'] = $admin_new_notification;}
            
            $admin_email_one = get_option('nes_admin_email_one');
            if(!empty($admin_email_one)){$this->nes_settings['admin_email_one'] = $admin_email_one;}

            $admin_email_label_one = get_option('nes_admin_email_label_one');
            if(!empty($admin_email_label_one)){$this->nes_settings['admin_email_label_one'] = $admin_email_label_one;} 
            $admin_email_two = get_option('nes_admin_email_two');
            if(isset($admin_email_two)){$this->nes_settings['admin_email_two'] = $admin_email_two;}
            $admin_email_label_two = get_option('nes_admin_email_label_two');
            if(isset($admin_email_label_two)){$this->nes_settings['admin_email_label_two'] = $admin_email_label_two;}
            
            $user_new_notification = get_option('nes_user_new_notification');
            if(!empty($user_new_notification)){$this->nes_settings['user_new_notification'] = $user_new_notification;}
            $user_approved_notification = get_option('nes_user_approved_notification');
            if(!empty($user_approved_notification)){$this->nes_settings['user_approved_notification'] = $user_approved_notification;}
            $from_email_name = get_option('nes_from_email_name');
            if(!empty($from_email_name)){$this->nes_settings['from_email_name'] = $from_email_name;}
            $from_email_address = get_option('nes_from_email_address');
            if(!empty($from_email_address)){$this->nes_settings['from_email_address'] = $from_email_address;}
            $hide_admin_bar = get_option('nes_hide_admin_bar');
            if(!empty($hide_admin_bar)){$this->nes_settings['hide_admin_bar'] = $hide_admin_bar;}
            $this->nes_settings['show_admin_bar_for'] = get_option('nes_show_admin_bar_for');
            $setup_cleanup = get_option('nes_setup_cleanup');
            if(!empty($setup_cleanup)){$this->nes_settings['setup_cleanup'] = $setup_cleanup;}
            $this->nes_settings['user_subject_line_new'] = get_option('nes_user_subject_line_new');
            $this->nes_settings['user_subject_line_approved'] = get_option('nes_user_subject_line_approved');
            $this->nes_settings['event_success_message'] = get_option('nes_event_success_message');
            $conflict_checking = get_option('nes_conflict_checking');
            if(!empty($conflict_checking)){$this->nes_settings['conflict_checking'] = $conflict_checking;}
            $show_event_details = get_option('nes_show_event_details');
            if(!empty($show_event_details)){$this->nes_settings['show_event_details'] = $show_event_details;}
            $this->nes_settings['show_form_fields'] = get_option('nes_show_form_fields');

            $offsite_venue_bg = get_option('nes_offsite_venue_bg');
            if(!empty($offsite_venue_bg)){$this->nes_settings['offsite_venue_bg'] = $offsite_venue_bg;}            
            $offsite_venue_text = get_option('nes_offsite_venue_text');
            if(!empty($offsite_venue_text)){$this->nes_settings['offsite_venue_text'] = $offsite_venue_text;}            
            $pending_color = get_option('nes_pending_color');
            if(!empty($pending_color)){$this->nes_settings['pending_color'] = $pending_color;}
            $approved_color = get_option('nes_approved_color');
            if(!empty($approved_color)){$this->nes_settings['approved_color'] = $approved_color;}
            $denied_color = get_option('nes_denied_color');
            if(!empty($denied_color)){$this->nes_settings['denied_color'] = $denied_color;}
            $table_header_color = get_option('nes_table_header_color');
            if(!empty($table_header_color)){$this->nes_settings['table_header_color'] = $table_header_color;}
            $unavailable_color = get_option('nes_unavailable_color');
            if(!empty($unavailable_color)){$this->nes_settings['unavailable_color'] = $unavailable_color;}
            $custom_css = get_option('nes_custom_css');
            if(!empty($custom_css)){$this->nes_settings['custom_css'] = $custom_css;}
            $use_tabbed_layout = get_option('nes_use_tabbed_layout');
            if(!empty($use_tabbed_layout)){$this->nes_settings['use_tabbed_layout'] = $use_tabbed_layout;}              
            $use_tabbed_layout_follow = get_option('nes_use_tabbed_layout_follow');
            if(!empty($use_tabbed_layout_follow)){$this->nes_settings['use_tabbed_layout_follow'] = $use_tabbed_layout_follow;}             
            $content_area_selector = get_option('nes_event_content_area_selector');
            if(!empty($content_area_selector)){$this->nes_settings['event_content_area_selector'] = $content_area_selector;}            
            $event_calendar_page = get_option('nes_calendar_page');
            if(!empty($event_calendar_page)){$this->nes_settings['event_calendar_page'] = $event_calendar_page;}            
            $event_reservation_page = get_option('nes_reservation_page');
            if(!empty($event_reservation_page)){$this->nes_settings['event_reservation_page'] = $event_reservation_page;}            
            $show_reservation_link = get_option('nes_show_reservation_link');
            if(!empty($show_reservation_link)){$this->nes_settings['show_reservation_link'] = $show_reservation_link;}            
            $max_events_per_day = get_option('nes_max_events_per_day');
            if(!empty($max_events_per_day)){$this->nes_settings['max_events_per_day'] = $max_events_per_day;}


            // filters 
            add_filter('manage_edit-nes_venue_columns', array($this, 'nes_venue_columns_filter'), 999, 1);
            add_filter('manage_edit-nes_location_columns', array($this, 'nes_location_columns_filter'), 999, 1);
            add_filter('manage_edit-nes_event_columns', array($this, 'nes_event_columns_filter'), 999, 1);
            add_filter('parse_query', array($this, 'nes_event_status_filter'));
            add_filter('manage_edit-nes_event_sortable_columns', array($this, 'nes_event_sortable_columns_filter'), 999, 1);
            add_filter('nes_tabbed_layout', array($this, 'nes_tabbed_layout'));
            add_filter('nes_tabs', array($this, 'nes_tab'), 1, 2);
            add_filter('nes_settings', array($this, 'nes_settings'));
            add_filter('nes_event_background', array($this, 'nes_event_background'), 1, 2);
            add_filter('nes_table_header_background', array($this, 'nes_table_header_background'));
            add_filter('nes_unavailable_background', array($this, 'nes_unavailable_background'), 1, 2);
            add_filter('parse_query', array($this, 'nes_event_list_filter'));
            add_filter('body_class', array($this, 'nes_body_class'));

            // actions
            add_action('admin_menu', array($this, 'admin_menu'), 999);
            add_action('admin_head', array($this, 'admin_menu_highlight'));
            add_action('init', array($this, 'init'), 11);
            add_action('admin_notices', array($this, 'nes_setup_message'), 11);
            add_action('admin_enqueue_scripts', array($this, 'nes_styles'), 9);
            add_action('wp_enqueue_scripts', array($this, 'nes_styles'), 9);
            add_action('add_meta_boxes', array($this, 'nes_venue_meta_box'));
            add_action('save_post', array($this, 'nes_save_venue_meta'),11);
            add_action('add_meta_boxes', array($this, 'nes_location_meta_box'));
            add_action('save_post', array($this, 'nes_save_location_meta'),11);
            add_action('add_meta_boxes', array($this, 'nes_event_meta_box'));
            add_action('save_post', array($this, 'nes_save_event_meta'),11,2);  
            add_action('wp_trash_post', array($this, 'nes_trash_event_meta'),11,2);         
            add_action('manage_nes_venue_posts_custom_column', array($this, 'nes_manage_venue_columns'), 11, 2);
            add_action('manage_nes_location_posts_custom_column', array($this, 'nes_manage_location_columns'), 11, 2);
            add_action('manage_nes_event_posts_custom_column', array($this, 'nes_manage_event_columns'), 11, 2);
            add_action('pre_get_posts', array($this, 'nes_column_orderby')); 
            add_action('restrict_manage_posts', array($this, 'nes_add_event_status_filter'));
            add_action('admin_init', array($this, 'nes_export_event_schedule'));
            add_action('wp_head', array($this,'nes_add_content_template'));
            add_action('admin_footer-post.php', array($this, 'nes_append_post_status_list')); 
            add_action('admin_footer-edit.php', array($this, 'nes_append_post_status_bulk_edit'));
            add_action('load-edit.php', array($this,'nes_check_past_events'),11);
            add_action('admin_bar_menu', array($this, 'nes_attendees_toolbar_link'), 81);
            add_action('admin_init', array($this, 'nes_export_attendee_list'));
            add_action('wp_loaded', array($this,'nes_check_for_gf'));
            // add_action('contextual_help', array($this, 'nes_screen_help'), 10, 3); //debugging
            add_action('init', array($this,'nes_check_ics_task'));
            add_action('nes_check_ics_cron', array($this, 'nes_ics_task'));

            // ajax hooks
            add_action('wp_ajax_nes_ajax_vacancy_import', array($this, 'nes_ajax_vacancy_import'));
            add_action('wp_ajax_nes_ajax_ecp_import', array($this, 'nes_ajax_ecp_import'));
            add_action('wp_ajax_nes_ajax_ical_import', array($this, 'nes_ajax_ical_import'));
            add_action('wp_ajax_nes_ajax_get_venue_locations', array($this, 'nes_ajax_get_venue_locations'));
            add_action('wp_ajax_nes_ajax_view_change', array($this, 'nes_ajax_view_change'));
            add_action('wp_ajax_nes_ajax_make_event_form', array($this, 'nes_ajax_make_event_form'));
            add_action('wp_ajax_nes_ajax_get_gravity_form_fields', array($this, 'nes_ajax_get_gravity_form_fields')); // NOT USED RIGHT NOW
            add_action('wp_ajax_nes_ajax_check_availability', array($this, 'nes_ajax_check_availability'));      
            add_action('wp_ajax_nes_ajax_get_time_select', array($this, 'nes_ajax_get_time_select'));      
            add_action('wp_ajax_nes_ajax_attendee_check_in_out', array($this, 'nes_ajax_attendee_check_in_out'));               

            add_action('wp_ajax_nopriv_nes_ajax_vacancy_import', array($this, 'nes_ajax_vacancy_import'));
            add_action('wp_ajax_nopriv_nes_ajax_ecp_import', array($this, 'nes_ajax_ecp_import'));
            add_action('wp_ajax_nopriv_nes_ajax_ical_import', array($this, 'nes_ajax_ical_import'));
            add_action('wp_ajax_nopriv_nes_ajax_get_venue_locations', array($this, 'nes_ajax_get_venue_locations'));
            add_action('wp_ajax_nopriv_nes_ajax_view_change', array($this, 'nes_ajax_view_change'));
            add_action('wp_ajax_nopriv_nes_ajax_make_event_form', array($this, 'nes_ajax_make_event_form'));
            add_action('wp_ajax_nopriv_nes_ajax_get_gravity_form_fields', array($this, 'nes_ajax_get_gravity_form_fields')); // NOT USED RIGHT NOW
            add_action('wp_ajax_nopriv_nes_ajax_check_availability', array($this, 'nes_ajax_check_availability'));      
            add_action('wp_ajax_nopriv_nes_ajax_get_time_select', array($this, 'nes_ajax_get_time_select'));      
            add_action('wp_ajax_nopriv_nes_ajax_attendee_check_in_out', array($this, 'nes_ajax_attendee_check_in_out'));            
    
            // localization
            add_action('plugins_loaded', array($this, 'nes_load_textdomain'));
            
            // shortcode
            add_shortcode('nessie', array($this, 'nes_shortcode_view'));

            // add widget
            add_action('widgets_init', function(){register_widget('NessieList');});
            add_action('widgets_init', function(){register_widget('NessieListAlt');});

            // Add a widget to the dashboard.
            add_action('wp_dashboard_setup', array($this, 'nes_add_dashboard_widgets'));

        }

        // add localization
        function nes_load_textdomain(){
            load_plugin_textdomain('nes', false, basename( dirname( __FILE__ ) ) . '/lang' );
        }

        function init(){

            // define custom ajax handler
            define('NES_AJAX_HANDLER', $this->nes_settings['dir'].'inc/nes-ajax-handler.php');
            define('NES_AJAX_URL', admin_url('admin-ajax.php'));

            // set default timezone
            if($timezone_string = get_option('timezone_string')){
                date_default_timezone_set($timezone_string);
            }

            // set defaults if options dont exist
            if(!get_option('nes_ical_feed_title')){update_option('nes_ical_feed_title', get_bloginfo('name'));}
		
            // set options
            if(isset($_POST['nes_update_settings'])){
                if(!empty($_POST['nes_venue_single'])){update_option('nes_venue_single', sanitize_text_field($_POST['nes_venue_single']));}
                if(!empty($_POST['nes_venue_plural'])){update_option('nes_venue_plural', sanitize_text_field($_POST['nes_venue_plural']));}
                if(!empty($_POST['nes_location_single'])){update_option('nes_location_single', sanitize_text_field($_POST['nes_location_single']));}
                if(!empty($_POST['nes_location_plural'])){update_option('nes_location_plural', sanitize_text_field($_POST['nes_location_plural']));}
                if(!empty($_POST['nes_event_single'])){update_option('nes_event_single', sanitize_text_field($_POST['nes_event_single']));}
                if(!empty($_POST['nes_event_plural'])){update_option('nes_event_plural', sanitize_text_field($_POST['nes_event_plural']));}                
                if(!empty($_POST['nes_default_venue'])){update_option('nes_default_venue', sanitize_text_field($_POST['nes_default_venue']));}
                if(!empty($_POST['nes_day_start_time'])){update_option('nes_day_start_time', sanitize_text_field($_POST['nes_day_start_time']));}
                if(!empty($_POST['nes_day_end_time'])){update_option('nes_day_end_time', sanitize_text_field($_POST['nes_day_end_time']));}
                
                if(isset($_POST['nes_name_label'])){update_option('nes_name_label', sanitize_text_field($_POST['nes_name_label']));}
                if(isset($_POST['nes_email_label'])){update_option('nes_email_label', sanitize_text_field($_POST['nes_email_label']));}
                if(isset($_POST['nes_title_label'])){update_option('nes_title_label', sanitize_text_field($_POST['nes_title_label']));}
                if(isset($_POST['nes_phone_label'])){update_option('nes_phone_label', sanitize_text_field($_POST['nes_phone_label']));}
                if(isset($_POST['nes_website_label'])){update_option('nes_website_label', sanitize_text_field($_POST['nes_website_label']));}
                if(isset($_POST['nes_event_type_label'])){update_option('nes_event_type_label', sanitize_text_field($_POST['nes_event_type_label']));}
                if(isset($_POST['nes_description_label'])){update_option('nes_description_label', sanitize_text_field($_POST['nes_description_label']));}
                if(isset($_POST['nes_setup_needs_label'])){update_option('nes_setup_needs_label', sanitize_text_field($_POST['nes_setup_needs_label']));}
                if(isset($_POST['nes_av_needs_label'])){update_option('nes_av_needs_label', sanitize_text_field($_POST['nes_av_needs_label']));}
                
                if(!empty($_POST['nes_end_time_type'])){update_option('nes_end_time_type', sanitize_text_field($_POST['nes_end_time_type']));}
                if(isset($_POST['nes_end_time_length_hr'])){update_option('nes_end_time_length_hr', sanitize_text_field($_POST['nes_end_time_length_hr']));}
                if(isset($_POST['nes_end_time_length_min'])){update_option('nes_end_time_length_min', sanitize_text_field($_POST['nes_end_time_length_min']));}
                if(isset($_POST['nes_end_time_min_length_hr'])){update_option('nes_end_time_min_length_hr', sanitize_text_field($_POST['nes_end_time_min_length_hr']));}
                if(isset($_POST['nes_end_time_min_length_min'])){update_option('nes_end_time_min_length_min', sanitize_text_field($_POST['nes_end_time_min_length_min']));}
                if(isset($_POST['nes_end_time_max_length_hr'])){update_option('nes_end_time_max_length_hr', sanitize_text_field($_POST['nes_end_time_max_length_hr']));}
                if(isset($_POST['nes_end_time_max_length_min'])){update_option('nes_end_time_max_length_min', sanitize_text_field($_POST['nes_end_time_max_length_min']));}
                if(isset($_POST['nes_end_time_minmax_interval'])){update_option('nes_end_time_minmax_interval', sanitize_text_field($_POST['nes_end_time_minmax_interval']));}
                if(!empty($_POST['nes_match_minmax_interval'])){update_option('nes_match_minmax_interval', sanitize_text_field($_POST['nes_match_minmax_interval']));}

                if(!empty($_POST['nes_require_login'])){update_option('nes_require_login', sanitize_text_field($_POST['nes_require_login']));}
                if(!empty($_POST['nes_admin_new_notification'])){update_option('nes_admin_new_notification', sanitize_text_field($_POST['nes_admin_new_notification']));}
                if(!empty($_POST['nes_admin_email_one'])){update_option('nes_admin_email_one', sanitize_text_field($_POST['nes_admin_email_one']));}
                if(!empty($_POST['nes_admin_email_label_one'])){update_option('nes_admin_email_label_one', sanitize_text_field($_POST['nes_admin_email_label_one']));}
                if(isset($_POST['nes_admin_email_two'])){update_option('nes_admin_email_two', sanitize_text_field($_POST['nes_admin_email_two']));}
                if(isset($_POST['nes_admin_email_label_two'])){update_option('nes_admin_email_label_two', sanitize_text_field($_POST['nes_admin_email_label_two']));}
                if(!empty($_POST['nes_user_new_notification'])){update_option('nes_user_new_notification', sanitize_text_field($_POST['nes_user_new_notification']));}
                if(!empty($_POST['nes_user_approved_notification'])){update_option('nes_user_approved_notification', sanitize_text_field($_POST['nes_user_approved_notification']));}
                if(!empty($_POST['nes_from_email_name'])){update_option('nes_from_email_name', sanitize_text_field($_POST['nes_from_email_name']));}
                if(!empty($_POST['nes_from_email_address'])){update_option('nes_from_email_address', sanitize_text_field($_POST['nes_from_email_address']));}
                if(isset($_POST['nes_notification_header'])){update_option('nes_notification_header', esc_textarea($_POST['nes_notification_header']));}
                if(isset($_POST['nes_notification_footer'])){update_option('nes_notification_footer', esc_textarea($_POST['nes_notification_footer']));}
                if(!empty($_POST['nes_hide_admin_bar'])){update_option('nes_hide_admin_bar', sanitize_text_field($_POST['nes_hide_admin_bar']));}
                if(isset($_POST['nes_hide_admin_bar'])){ // check other field since this one can be not set
                    if(!empty($_POST['nes_show_admin_bar_for'])){
                        $vals = $_POST['nes_show_admin_bar_for'];
                        if(is_array($vals)){
                            foreach($vals as $k => $v){$vals[$k] = sanitize_text_field($v);}
                            update_option('nes_show_admin_bar_for', $vals);
                        }
                    }else{
                        update_option('nes_show_admin_bar_for', array(''));
                    }
                }
                if(!empty($_POST['nes_setup_cleanup'])){update_option('nes_setup_cleanup', sanitize_text_field($_POST['nes_setup_cleanup']));}
                if(isset($_POST['nes_user_subject_line_new'])){update_option('nes_user_subject_line_new', sanitize_text_field($_POST['nes_user_subject_line_new']));}
                if(isset($_POST['nes_user_subject_line_approved'])){update_option('nes_user_subject_line_approved', sanitize_text_field($_POST['nes_user_subject_line_approved']));}
                if(isset($_POST['nes_event_success_message'])){update_option('nes_event_success_message', sanitize_text_field($_POST['nes_event_success_message']));}
                if(isset($_POST['nes_conflict_checking'])){update_option('nes_conflict_checking', sanitize_text_field($_POST['nes_conflict_checking']));}
                if(isset($_POST['nes_show_event_details'])){update_option('nes_show_event_details', sanitize_text_field($_POST['nes_show_event_details']));}
               
                $show_form_fields = get_option('nes_show_form_fields');
                if(!is_array($show_form_fields)){
                    update_option('nes_show_form_fields', $this->nes_settings['show_form_fields']);
                }
                if(isset($_POST['nes_save_form_settings'])){
                    if(isset($_POST['nes_show_form_fields'])){
                        $vals = $_POST['nes_show_form_fields'];
                    }else{
                        $vals = array();
                    }
                    update_option('nes_show_form_fields', $vals);
                }

                if(!empty($_POST['nes_offsite_venue_bg'])){update_option('nes_offsite_venue_bg', sanitize_text_field($_POST['nes_offsite_venue_bg']));}
                if(!empty($_POST['nes_offsite_venue_text'])){update_option('nes_offsite_venue_text', sanitize_text_field($_POST['nes_offsite_venue_text']));}
                if(!empty($_POST['nes_pending_color'])){update_option('nes_pending_color', sanitize_text_field($_POST['nes_pending_color']));}
                if(!empty($_POST['nes_approved_color'])){update_option('nes_approved_color', sanitize_text_field($_POST['nes_approved_color']));}
                if(!empty($_POST['nes_denied_color'])){update_option('nes_denied_color', sanitize_text_field($_POST['nes_denied_color']));}
                if(!empty($_POST['nes_table_header_color'])){update_option('nes_table_header_color', sanitize_text_field($_POST['nes_table_header_color']));}
                if(!empty($_POST['nes_unavailable_color'])){update_option('nes_unavailable_color', sanitize_text_field($_POST['nes_unavailable_color']));}
                if(!empty($_POST['nes_use_tabbed_layout'])){update_option('nes_use_tabbed_layout', sanitize_text_field($_POST['nes_use_tabbed_layout']));}
                if(!empty($_POST['nes_use_tabbed_layout_follow'])){update_option('nes_use_tabbed_layout_follow', sanitize_text_field($_POST['nes_use_tabbed_layout_follow']));}
                
                if(isset($_POST['nes_custom_css'])){update_option('nes_custom_css', sanitize_text_field($_POST['nes_custom_css']));}
                if(isset($_POST['nes_event_content_area_selector'])){update_option('nes_event_content_area_selector', sanitize_text_field($_POST['nes_event_content_area_selector']));}
                if(isset($_POST['nes_calendar_page'])){update_option('nes_calendar_page', sanitize_text_field($_POST['nes_calendar_page']));}
                if(isset($_POST['nes_reservation_page'])){update_option('nes_reservation_page', sanitize_text_field($_POST['nes_reservation_page']));}
                if(isset($_POST['nes_show_reservation_link'])){update_option('nes_show_reservation_link', sanitize_text_field($_POST['nes_show_reservation_link']));}
                if(isset($_POST['nes_max_events_per_day'])){update_option('nes_max_events_per_day', sanitize_text_field($_POST['nes_max_events_per_day']));}
                if(isset($_POST['nes_ical_feed_title'])){update_option('nes_ical_feed_title', sanitize_text_field($_POST['nes_ical_feed_title']));}
                
                // update role permissions
                if(isset($_POST['nes_menu_main'])){
                    $nes_menu_visibility = array();
                    $nes_menu_visibility['nes_menu_main'] = $_POST['nes_menu_main'];
                    $nes_menu_visibility['nes_menu_events'] = $_POST['nes_menu_events'];
                    $nes_menu_visibility['nes_menu_locations'] = $_POST['nes_menu_locations'];
                    $nes_menu_visibility['nes_menu_venues'] = $_POST['nes_menu_venues'];
                    $nes_menu_visibility['nes_menu_categories'] = $_POST['nes_menu_categories'];
                    $nes_menu_visibility['nes_menu_settings'] = $_POST['nes_menu_settings'];
                    $nes_menu_visibility['nes_menu_import'] = $_POST['nes_menu_import'];
                    $nes_menu_visibility['nes_menu_attendees'] = $_POST['nes_menu_attendees'];
                    update_option('nes_menu_visibility', $nes_menu_visibility);
                    $this->nes_settings['menu_visibility'] = $nes_menu_visibility;
                }
            }
            
            // Create Venue post type
            $labels = array(
                'venue' => array(
                    'name' => $this->nes_settings['venue_plural'],
                    'singular_name' => $this->nes_settings['venue_single'],
                    'add_new' => 'Add New',
                    'add_new_item' => 'Add New ' . $this->nes_settings['venue_single'],
                    'edit_item' =>  'Edit ' . $this->nes_settings['venue_single'],
                    'new_item' => 'New ' . $this->nes_settings['venue_single'],
                    'view_item' => 'View ' . $this->nes_settings['venue_single'],
                    'search_items' => 'Search ' .$this->nes_settings['venue_plural'],
                    'not_found' => 'No ' . $this->nes_settings['venue_plural'] . ' Found',
                    'not_found_in_trash' => 'No ' . $this->nes_settings['venue_plural'] . ' found in Trash'
                ),    
                'location' => array(
                    'name' => $this->nes_settings['location_plural'],
                    'singular_name' => $this->nes_settings['location_single'],
                    'add_new' => 'Add New',
                    'add_new_item' => 'Add New ' . $this->nes_settings['location_single'],
                    'edit_item' =>  'Edit ' . $this->nes_settings['location_single'],
                    'new_item' => 'New ' . $this->nes_settings['location_single'],
                    'view_item' => 'View ' . $this->nes_settings['location_single'],
                    'search_items' => 'Search ' .$this->nes_settings['location_plural'],
                    'not_found' => 'No ' . $this->nes_settings['location_plural'] . ' Found',
                    'not_found_in_trash' => 'No ' . $this->nes_settings['location_plural'] . ' found in Trash'
                ),     
            );
            
            $user = wp_get_current_user();
            foreach($labels as $k => $label){

                $args = array(
                    'labels' => $label,
                    'public' => true,
                    'publicly_queryable' => true,
                    'show_ui' => true,
                    'capability_type' => 'post',
                    'hierarchical' => false,
                    'rewrite' => array('slug' => $k),
                    'query_var' => 'nes_'.$k,
                    'supports' => array('title','editor','revisions'),
                    'show_in_menu'  => false,
                );

                if (is_array($this->nes_settings['menu_visibility']["nes_menu_{$k}s"]) && !empty(array_intersect($user->roles, $this->nes_settings['menu_visibility']["nes_menu_{$k}s"]))) {
                    $args['show_in_admin_bar'] = true;
                }
                register_post_type('nes_'.$k, $args);     
            }

            // events
            $labels = array(
                'event' => array(
                    'name' => $this->nes_settings['event_plural'],
                    'singular_name' => $this->nes_settings['event_single'],
                    'add_new' => 'Add New',
                    'add_new_item' => 'Add New ' . $this->nes_settings['event_single'],
                    'edit_item' =>  'Edit ' . $this->nes_settings['event_single'],
                    'new_item' => 'New ' . $this->nes_settings['event_single'],
                    'view_item' => 'View ' . $this->nes_settings['event_single'],
                    'search_items' => 'Search ' .$this->nes_settings['event_plural'],
                    'not_found' => 'No ' . $this->nes_settings['event_plural'] . ' Found',
                    'not_found_in_trash' => 'No ' . $this->nes_settings['event_plural'] . ' found in Trash'
                )
            );

            foreach($labels as $k => $label){
                $args = array(
                    'labels' => $label,
                    'public' => true,
                    'publicly_queryable' => true,
                    'show_ui' => true,
                    'capability_type' => 'post',
                    'hierarchical' => false,
                    'rewrite' => array('slug' => $k),
                    'query_var' => 'nes_'.$k,
                    'supports' => array('title','editor','revisions','thumbnail'),
                    'show_in_menu'  => false,
                );

                if (is_array($this->nes_settings['menu_visibility']["nes_menu_{$k}s"]) && !empty(array_intersect($user->roles, $this->nes_settings['menu_visibility']["nes_menu_{$k}s"]))) {
                    $args['show_in_admin_bar'] = true;
                }

                register_post_type('nes_'.$k, $args);     
            }

            // create a new taxonomy
            register_taxonomy(
                'event_category',
                'nes_event',
                array(
                    'hierarchical'          => true,
                    'label'                => 'Event Category',
                    'show_ui'               => true,
                    'show_admin_column'     => true,
                    'update_count_callback' => '_update_post_term_count',
                    'query_var'             => true,
                    'rewrite'               => array('slug' => 'event-category')
                )
            );

            // create "Past Events" status
            register_post_status('past_events', array(
                'label'                     => _x('Past Events', 'nes_event'),
                'public'                    => true,
                'show_in_admin_all_list'    => true,
                'show_in_admin_status_list' => true,
                'label_count'               => _n_noop('Past Events <span class="count">(%s)</span>', 'Past Events <span class="count">(%s)</span>')
            ));
        
            // check for WP admin bar
            if($this->nes_settings['hide_admin_bar'] == 'yes'){
                $nes_user = wp_get_current_user();
                $user_roles = $nes_user->roles;
                $user_role = array_shift($user_roles);
                if(is_array($this->nes_settings['show_admin_bar_for'])){
                    if(!in_array($user_role, $this->nes_settings['show_admin_bar_for'])){
                        show_admin_bar(false);
                    }
                }else if($user_role != $this->nes_settings['show_admin_bar_for']){
                   show_admin_bar(false);
                }
            }    
        }

        // the cron check
        function nes_check_ics_task(){
            if(!wp_next_scheduled('nes_check_ics_cron')){
                wp_schedule_event(time(), 'hourly', 'nes_check_ics_cron');
            }
        }

        // the ics task
        function nes_ics_task(){
            include_once('inc/nes-generate-ical.php');
        }

        function nes_check_for_gf(){
             // gravity forms is required
            if(!class_exists('GFFormsModel')){

                add_action('admin_notices', array($this, 'nes_plugin_admin_notice'));            
                add_action('admin_init', array($this, 'nes_plugin_deactivate'));            
                
                if(isset($_GET['activate'])){
                     unset($_GET['activate']);
                }
            }else{
                // include the gravity forms helper functions 
                add_filter('gform_pre_render', array($this,'nes_gform_helpers'));
                add_filter('gform_pre_validation', array($this,'nes_gform_helpers'));
                add_filter('gform_validation', array($this, 'nes_gf_limit_to_one'));
                add_filter('gform_validation', array($this, 'nes_gf_ticket_limit'));
                add_filter('gform_pre_submission_filter', array($this,'nes_gform_helpers'));
                add_filter('gform_admin_pre_render', array($this,'nes_gform_helpers'));
                add_action('gform_entry_created', array($this,'nes_after_event_ticket_purchase'), 10, 2);
                add_action('gform_post_form_trashed', array($this, 'nes_gf_tickets_form_untrash'),10, 1);
                add_filter('gform_field_value_nessie_tickets', array($this, 'nes_populate_event_id'));
                add_filter('gform_notification', array($this, 'nes_gf_tickets_notification'), 10, 3);

                // include auto population hooks if user logged in
                if(is_user_logged_in()){
                    $nes_user = wp_get_current_user();
                    add_filter('gform_field_value_nes_first', function() {
                        return get_usermeta($nes_user->ID,'billing_first_name');
                    });
                    add_filter('gform_field_value_nes_last', function() {
                        return get_usermeta($nes_user->ID,'billing_last_name');
                    });
                    add_filter('gform_field_value_nes_email', function() {
                        return get_usermeta($nes_user->ID,'billing_email');
                    });
                    add_filter('gform_field_value_nes_address_1', function() {
                        return get_usermeta($nes_user->ID,'billing_address_1');
                    });
                    add_filter('gform_field_value_nes_address_2', function() {
                        return get_usermeta($nes_user->ID,'billing_address_2');
                    });
                    add_filter('gform_field_value_nes_city', function() {
                        return get_usermeta($nes_user->ID,'billing_city');
                    });
                    add_filter('gform_field_value_nes_state', function() {
                        return get_usermeta($nes_user->ID,'billing_state');
                    });
                    add_filter('gform_field_value_nes_zip', function() {
                        return get_usermeta($nes_user->ID,'billing_postcode');
                    });
                    add_filter('gform_field_value_nes_country', function() {
                        return get_usermeta($nes_user->ID,'billing_country');
                    });
                }

                // include gravity forms "Nessie Tickets" helper functions
                include_once('inc/nes-gform-functions.php');

                // require list fields
                new GWRequireListColumns();
            }
        }

        // no gforms notice
        function nes_plugin_admin_notice(){
            echo '<div class="error"><p><strong>Nessie</strong> requires Gravity Forms to function properly and has been <strong>deactivated</strong>.</p></div>';
        }

        function nes_plugin_deactivate(){
            deactivate_plugins( plugin_basename( __FILE__ ) );
        }

        // limit to 1 ticket per order
        function nes_gf_limit_to_one($validation_result){
            global $post;

            // check if only 1 ticket per purchase
            if(get_post_meta($post->ID,'nes_ticket_per_purchase',true) == 'yes'){
               
                $form = $validation_result['form'];
                $has_error = false;
                
                if(!GFCommon::get_fields_by_type($form, array('quantity')))
                    return $validation_result;
                
                $total_qty = 0;
                foreach($form['fields'] as &$field) {
                    
                    if(RGFormsModel::get_input_type($field) != 'number')
                        continue;
                    
                    $raw_value = rgpost( "input_{$field['id']}" );

                    if(rgblank($raw_value)){
                        continue;
                    }        

                    $quantity = GFCommon::to_number( $raw_value );
                    $total_qty += $quantity;

                    if($total_qty > 1){
                        $has_error = true;
                        $field['failed_validation'] = true;
                        $field['validation_message'] = 'Only 1 Ticket can be purchased per order';
                    }
                }
                
                if(!$has_error)
                    return $validation_result;
                
                $validation_result['is_valid'] = false;
                $validation_result['form'] = $form;
            }
            
            return $validation_result;
        }

        // check if tickets submitted are available
        function nes_gf_ticket_limit($validation_result){
            global $post;

            // get available ticket limit
            if($limit = get_post_meta($post->ID,'nes_tickets_available',true)){

                // get tickets already sold
                $attendees = count(get_post_meta($post->ID,'nes_attendees',true));

                // calculate how many tickets are left
                $remaining = $limit - $attendees;
               
                $form = $validation_result['form'];
                $has_error = false;
                
                if(!GFCommon::get_fields_by_type($form, array('quantity')))
                    return $validation_result;
                
                $total_qty = 0;
                foreach($form['fields'] as &$field) {
                    
                    if(RGFormsModel::get_input_type($field) != 'number')
                        continue;
                    
                    $raw_value = rgpost( "input_{$field['id']}" );

                    if(rgblank($raw_value)){
                        continue;
                    }        

                    $quantity = GFCommon::to_number( $raw_value );
                    $total_qty += $quantity;

                    if($total_qty > $remaining){
                        $has_error = true;
                        $field['failed_validation'] = true;
                        $field['validation_message'] = 'You have exceeded the number of total tickets available ('.$remaining.')';

                        
                    }
                }
                
                if(!$has_error)
                    return $validation_result;
                
                $validation_result['is_valid'] = false;
                $validation_result['form'] = $form;
            
            }
            return $validation_result;

        }

        // add an attendees on an event page link to the WP Toolbar
        function nes_attendees_toolbar_link($wp_admin_bar){
            if(is_singular('nes_event')){
                $user = wp_get_current_user();
                if (is_array($this->nes_settings['menu_visibility']["nes_menu_attendees"]) && !empty(array_intersect($user->roles, $this->nes_settings['menu_visibility']["nes_menu_attendees"]))) {
                    $args = array(
                        'id' => 'nes-attendees',
                        'title' => 'View Attendee List', 
                        'href' => '/wp-admin/admin.php?page=nes-attendee-lists&event_id='.get_the_ID(),
                    );
                    $wp_admin_bar->add_node($args);
                }
           }
        }

        // ajax function to check attendees in/out
        function nes_ajax_attendee_check_in_out(){

            // get posted data
            $meta_key = $_POST['meta_key'];
            $action = $_POST['in_out'];
            $event_id = $_POST['event_id'];

            // get attendee meta
            $attendees = get_post_meta($event_id, 'nes_attendees', true);

            if($action == 'in'){
                $attendees[$meta_key]['status'] = 'in';
                echo 'Check Out';
            }else{
                $attendees[$meta_key]['status'] = 'out';
                echo 'Check In';
            }

            // update the attendees list
            update_post_meta($event_id, 'nes_attendees', $attendees);
            
            die();
        }

        // call all the helper classes on form pre_render
        function nes_gform_helpers($form){
            $is_nessie = false;
         
            // get form fields
            if($form['fields']){
                foreach($form['fields'] as &$field){
                    // check for hidden "nessie_form" field
                    if($field->inputName == 'nessie_tickets'){
                        $is_nessie = true; 
                    }
                }
            }

            // only run if is nessie ticket form on event page
            global $post;
            if($is_nessie && $post && $post->post_type == 'nes_event'){   

                // get this events ticket meta
                $types = get_post_meta($post->ID,'nes_ticket_type',true);
                $tickets = get_post_meta($post->ID,'nes_ticket_description',true);
                $prices = get_post_meta($post->ID,'nes_ticket_price',true);
                $suggested = get_post_meta($post->ID,'nes_ticket_suggested',true);
                $product_lists = array();

                if(!empty($tickets)){
                    foreach($tickets as $k => $ticket){

                        // check whether or not this is a user defined price
                        if($types[$k] == 'user-defined'){
                            foreach($form['fields'] as $key => &$field){
                                if($field->label == 'Price '.($k+1)){

                                    // check for price field and update values
                                    $form['fields'][$key]->label = $ticket;
                                    $form['fields'][$key]->defaultValue = GFCommon::to_money($suggested[$k]);
                                    $form['fields'][$key]->rangeMin = $prices[$k];
                                    $form['fields'][$key]->description =  "Suggested: ".GFCommon::to_money($suggested[$k]) . " / Minimum: ".GFCommon::to_money($prices[$k]);

                                    // show this field
                                    $form['fields'][$key]->conditionalLogic = array(
                                        'actionType' => 'show',
                                        'logicType' => 'all',
                                        'rules' => array()
                                    );

                                    $product_lists['Price '.($k+1)]['form_id'] = $form['id'];
                                }

                                if($field->adminLabel == 'Price '.($k+1)){
                                    // save field ids for dynamic list fields
                                    if($field->type == 'quantity'){
                                        $product_lists['Price '.($k+1)]['quantity_id'] = $form['fields'][$key]->id;

                                        // show quantity and attendee fields
                                        $form['fields'][$key]->conditionalLogic = array(
                                            'actionType' => 'show',
                                            'logicType' => 'all',
                                            'rules' => array()
                                        );
                                    }elseif($field->type == 'list'){
                                        //$form['fields'][$key]->label = 'Attendee(s) for: ' . $ticket;
                                        $product_lists['Price '.($k+1)]['list_id'] = $form['fields'][$key]->id;
                                    }
                                }
                            }
                        }

                        // check if this ticket is just a standard ticket
                        if($types[$k] == 'standard'){
                            foreach($form['fields'] as $key => &$field){

                                // check for ticket (product) field and update values
                                if($field->label == 'Ticket '.($k+1)){
                                    $form['fields'][$key]->label = $ticket;
                                    $form['fields'][$key]->basePrice = $prices[$k];

                                    // show ticket field
                                    $form['fields'][$key]->conditionalLogic = array(
                                        'actionType' => 'show',
                                        'logicType' => 'all',
                                        'rules' => array()
                                    );

                                    $product_lists['Ticket '.($k+1)]['form_id'] = $form['id'];
                                }

                                if($field->adminLabel == 'Ticket '.($k+1)){

                                    // save field ids for dynamic list fields
                                    if($field->type == 'quantity'){
                                        $product_lists['Ticket '.($k+1)]['quantity_id'] = $form['fields'][$key]->id;

                                        // show quantity and attendee fields
                                        $form['fields'][$key]->conditionalLogic = array(
                                            'actionType' => 'show',
                                            'logicType' => 'all',
                                            'rules' => array()
                                        );
                                    }elseif($field->type == 'list'){
                                        //$form['fields'][$key]->label = 'Attendee(s) for: ' . $ticket;
                                        $product_lists['Ticket '.($k+1)]['list_id'] = $form['fields'][$key]->id;
                                    }
                                }
                            }
                        }
                    }

                    // hook up the list fields to the product quantities
                    foreach($product_lists as $list){
                        new GWAutoListFieldRows(
                            array(
                                'form_id' => $list['form_id'],
                                'list_field_id' => $list['list_id'],
                                'input_html_id' => "#input_{$list['form_id']}_{$list['quantity_id']}"
                            )
                        );
                    }
                }
            }

            return $form;
        }

        // populate the nessie tickets form with the event id it belongs to
        function nes_populate_event_id($value){
            global $post;
            return $post->ID;
        }

        // update the ticket quantities and attendee list after ticket submission
        function nes_after_event_ticket_purchase($entry, $form){

            // check if this is an nessie events form
            $nessie_event_id = false;
            foreach($form['fields'] as $field){

                if(!empty($field->inputName)){
                    if($field->inputName == 'nessie_tickets'){
                        $nessie_event_id = rgar($entry, $field->id);
                    }
                }
            }

            // only run if its an entry from an event
            if($nessie_event_id){

                // get ticket fields
                $tickets = get_post_meta($nessie_event_id,'nes_ticket_description',true);
                $types = get_post_meta($nessie_event_id,'nes_ticket_type',true);
                $ticket_entry_ids = array();

                foreach($tickets as $k => $ticket){

                    // check whether or not this is a user defined price
                    if($types[$k] == 'user-defined'){

                        foreach($form['fields'] as $key => &$field){

                            // set ticket description
                            if($field->label == $ticket){
                                $ticket_entry_ids[$k]['description'] = $ticket;
                                $ticket_entry_ids[$k]['price'] = GFCommon::to_money(rgar($entry, $field->id));
                            }

                            // match attendee list to description
                            if(($field->type == 'list') && ($field->adminLabel == 'Price '.($k+1))){
                                $ticket_entry_ids[$k]['attendees'] = unserialize(rgar($entry, $form['fields'][$key]->id));
                            }

                            // get purchaser
                            foreach($form['fields'] as $key => &$field){
                                if($field->adminLabel == 'purchaser'){
                                    $ticket_entry_ids[$k]['purchaser'] = rgar($entry, $form['fields'][$key]->id . '.3') . ' ' . rgar($entry, $form['fields'][$key]->id . '.6');
                                }
                            }
                        }
                    }

                    // check if this ticket is just a standard ticket
                    if($types[$k] == 'standard'){
                        foreach($form['fields'] as $key => &$field){

                            // set ticket description
                            if($field->label == $ticket){
                                $ticket_entry_ids[$k]['description'] = $ticket;
                                $ticket_entry_ids[$k]['price'] = GFCommon::to_money(rgar($entry, $field->id . '.2'));
                            }

                            // match attendee list to description
                            if(($field->type == 'list') && ($field->adminLabel == 'Ticket '.($k+1))){
                                $ticket_entry_ids[$k]['attendees'] = unserialize(rgar($entry, $form['fields'][$key]->id));
                            }

                            // get purchaser
                            foreach($form['fields'] as $key => &$field){
                                if($field->adminLabel == 'purchaser'){
                                    $ticket_entry_ids[$k]['purchaser'] = rgar($entry, $form['fields'][$key]->id . '.3') . ' ' . rgar($entry, $form['fields'][$key]->id . '.6');
                                }
                            }
                        }
                    }
                }


                // add new attendees to list
                $attendees = get_post_meta($nessie_event_id, 'nes_attendees', true);
                if($ticket_entry_ids){
                    foreach($ticket_entry_ids as $entry_id){
                        $attendee_arr = $entry_id['attendees'];
                        if($attendee_arr){
                            foreach($attendee_arr as $attendee){
                                $number = strtotime('now') . rand(1000,9999);
                                $attendees[] = array('number' => $number, 'purchaser' => $entry_id['purchaser'], 'attendee' => $attendee, 'description' => $entry_id['description'], 'price' => $entry_id['price'], 'status' => 'out');
                            }
                        }
                        
                    }
                    // update ticket inventory
                    update_post_meta($nessie_event_id, 'nes_attendees', $attendees);
                }
            }
        }

        // custom notification to user
        function nes_gf_tickets_notification($notification, $form, $entry){

            // check if this is an nessie events form
            $nessie_event_id = false;
            foreach($form['fields'] as $field){
                if(!empty($field->inputName)){
                    if($field->inputName == 'nessie_tickets'){
                        $nessie_event_id = rgar($entry, $field->id);
                    }
                }
            }

            // only run if its an entry from an event
            if($nessie_event_id && ($notification['name'] == 'User Notification')){

                // instantiate entry arrays
                $event_arr = array();
                $billing_arr = array();
                $num_products = 0;
                $total = 0;

                // get submitted fields
                foreach($entry as $entry_id => $entry_item){
                    if(!empty($entry_item)){

                        // explode for comparison to form object
                        $entry_id_arr = explode('.', $entry_id);

                        // loop through form to get field type
                        foreach($form['fields'] as &$field){
                            if($field->id == $entry_id_arr[0]){
                                if($field->type == 'address'){
                                    switch($entry_id_arr[1]){
                                        case 1:
                                            $billing_arr['address']['street'] = $entry_item; break;
                                        case 2: 
                                            $billing_arr['address']['line2'] = $entry_item; break;                                        
                                        case 3: 
                                            $billing_arr['address']['city'] = $entry_item; break;                                        
                                        case 4: 
                                            $billing_arr['address']['state'] = $entry_item; break;                                        
                                        case 5: 
                                            $billing_arr['address']['zipcode'] = $entry_item; break;                                        
                                        case 6: 
                                            $billing_arr['address']['country'] = $entry_item; break;
                                        default: break;
                                    }
                                }

                                if($field->type == 'name'){
                                    switch($entry_id_arr[1]){
                                        case 3:
                                            $billing_arr['name']['first'] = $entry_item; break;                                        
                                        case 6:
                                            $billing_arr['name']['last'] = $entry_item; break;
                                        default: break;
                                    }
                                }           

                                if($field->type == 'quantity'){
                                    $num_products = $num_products + $entry_item;
                                }

                                if($field->type == 'email'){
                                    $billing_arr['email'] = $entry_item;
                                }

                                if($field->type == 'total'){
                                    $total = $entry_item;
                                }

                                if($field->inputName == 'nessie_tickets'){

                                    // get title
                                    $event_arr['title'] = get_the_title($entry_item);

                                    // get date
                                    $event_arr['date'] = date('F jS, Y', strtotime(get_post_meta($entry_item, 'nes_event_date', true)));
                                    
                                    // get time
                                    $event_arr['time'] = date('l - F jS, Y', strtotime(get_post_meta($entry_item, 'nes_event_date', true))) .' @ '. date('g:ia', strtotime(get_post_meta($entry_item, 'nes_start_time', true))) .' - '. date('g:ia', strtotime(get_post_meta($entry_item, 'nes_end_time', true)));

                                    // get venue
                                    $event_arr['venue'] = get_the_title(get_post_meta($entry_item, 'nes_venue_id', true));  
                                    
                                    // check for locations              
                                    $location_ids = get_post_meta($entry_item, 'nes_location_id', true);
                                    if($location_ids){
                                        $delimiter = ' - ';
                                        foreach($location_ids as $location_id){
                                            $where .= $delimiter . get_the_title($location_id);
                                            $delimiter = ', ';
                                        }
                                        $event_arr['location'] = $where;
                                    }

                                    // get ticket info
                                    $attendees = get_post_meta($entry_item, 'nes_attendees', true);
                                }
                            }
                        }
                    }
                }

                // get ticket info
                $ticket_arr = array();
                $event_attendees = count($attendees) - 1;
                $products_ordered = $event_attendees - $num_products;
                for($i = $event_attendees; $i > $products_ordered; $i--){
                    $ticket_arr[] = array(
                        'number' => $attendees[$i]['number'],
                        'attendee' => $attendees[$i]['attendee'],
                        'description' => $attendees[$i]['description'],
                        'price' => $attendees[$i]['price']
                    );
                }
                ob_start();
                ?>

                <table>
                    <thead>
                        <tr>
                            <th style="background:#eeeeee;padding:20px;text-align:center;"><h3>Ticket Order Confirmation:</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="background:#eaf2fa;padding:10px;">Event Details</td>
                        </tr>
                        <tr>
                            <td style="padding:5px;">
                                <ul>
                                    <li><strong>Title:</strong> <?php echo $event_arr['title']; ?></li>
                                    <li><strong>Date:</strong> <?php echo $event_arr['date']; ?></li>
                                    <li><strong>Time:</strong> <?php echo $event_arr['time']; ?></li>
                                    <li><strong>Venue:</strong> <?php echo $event_arr['venue']; ?></li>
                                    <li><strong>Location:</strong> <?php echo $event_arr['location']; ?></li>
                               </ul>
                            </td>
                        </tr>
                        <tr>
                            <td style="background:#eaf2fa;padding:10px;">Ticket Details</td>
                        </tr>
                        <tr>
                            <td style="padding:20px;">
                                <table style="width:100%;">
                                    <thead>
                                        <tr>
                                            <th style="background:#eeeeee;padding:5px;font-weight:normal;">Ticket Number</th>
                                            <th style="background:#eeeeee;padding:5px;font-weight:normal;">Attendee Name</th>
                                            <th style="background:#eeeeee;padding:5px;font-weight:normal;">Ticket Description</th>
                                            <th style="background:#eeeeee;padding:5px;font-weight:normal;">Ticket Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($ticket_arr as $ticket) : ?>
                                        <tr>
                                            <td style="padding:5px;">#<?php echo $ticket['number']; ?></td>
                                            <td style="padding:5px;"><?php echo $ticket['attendee']; ?></td>
                                            <td style="padding:5px;"><?php echo $ticket['description']; ?></td>
                                            <td style="padding:5px;"><?php echo $ticket['price']; ?></td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="background:#eaf2fa;padding:10px;">Billing Details</td>
                        </tr>                        
                        <tr>
                            <td style="padding:5px;">
                                <ul>
                                    <li><strong>Name:</strong> <?php echo $billing_arr['name']['first']; ?> <?php echo $billing_arr['name']['last']; ?></li>
                                    <li><strong>Email:</strong> <?php echo $billing_arr['email']; ?></li>
                                    <li><strong>Address:</strong> <?php echo $billing_arr['address']['street']; ?><?php if($line2 = $billing_arr['address']['line2']){echo ' '.$line2;} ?>, <?php echo $billing_arr['address']['city']; ?> <?php echo $billing_arr['address']['state']; ?> <?php echo $billing_arr['address']['zipcode']; ?>, <?php echo $billing_arr['address']['country']; ?></li>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td style="background:#eeeeee;padding:20px;text-align:center;"><h3>Order Total: $<?php echo $total; ?></h3></td>
                        </tr>
                    </tfoot>
                </table>

                <?php 
                $notification['message'] = ob_get_clean();
            }

            return $notification;
        }
        

        // disallow trashing of Nessie Tickets form
        function nes_gf_tickets_form_untrash($form_id){
            $form_obj = GFAPI::get_form($form_id);
            if($form_obj['title'] == 'Nessie Tickets'){
                RGFormsModel::restore_form($form_id);
            }
        }

        // update post status is the event date is in the past
        function nes_check_past_events(){
            $screen = get_current_screen();
            if($screen->id == 'edit-nes_event'){
                global $wpdb;
                $now = date('Y-m-d',strtotime('now'));
                $query_string = "
                    UPDATE $wpdb->posts p
                    LEFT JOIN $wpdb->postmeta m1 ON p.ID = m1.post_id
                    SET p.post_status = 'past_events'
                    WHERE p.post_type = 'nes_event'
                    AND p.post_status IN ('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit')
                    AND m1.meta_key = 'nes_event_date'
                    AND m1.meta_value < '$now'
                ";
                $events = $wpdb->get_results($query_string, OBJECT);
            }
        }

        function nes_setup_message(){
            // check if clicked 
            $clicked = get_option('nes_setup_usage');
            if($clicked != 1){
            ?>
            <div class="updated">
                <p><strong><?php _e('Welcome to Nessie!','nes'); ?></strong> <?php _e('Please take a moment to learn about how to setup and use your new event system!','nes'); ?> <a href="/wp-admin/admin.php?page=nes-settings&tab=nes-setup-usage&notice=1"><?php _e('Click here for more','nes'); ?></a></p>
            </div>
            <?php
            }
        }

        function admin_menu(){
            global $submenu;

            // check if this user role can see this menu
            $user = wp_get_current_user();

            if (is_array($this->nes_settings['menu_visibility']['nes_menu_main']) && !empty(array_intersect($user->roles, $this->nes_settings['menu_visibility']['nes_menu_main']))) {
                add_menu_page(__('Nessie','nes'), __('Nessie','nes'), 'read', 'nes-settings', null, null, '6.66');
            }
            if (is_array($this->nes_settings['menu_visibility']['nes_menu_events']) && !empty(array_intersect($user->roles, $this->nes_settings['menu_visibility']['nes_menu_events']))) {
                add_submenu_page('nes-settings', $this->nes_settings['event_plural'], $this->nes_settings['event_plural'], 'read', 'edit.php?post_type=nes_event&post_status=publish');
            }
            if (is_array($this->nes_settings['menu_visibility']['nes_menu_locations']) && !empty(array_intersect($user->roles, $this->nes_settings['menu_visibility']['nes_menu_locations']))) {
                add_submenu_page('nes-settings', $this->nes_settings['location_plural'], $this->nes_settings['location_plural'], 'read', 'edit.php?post_type=nes_location');
            }
            if (is_array($this->nes_settings['menu_visibility']['nes_menu_venues']) && !empty(array_intersect($user->roles, $this->nes_settings['menu_visibility']['nes_menu_venues']))) {
                add_submenu_page('nes-settings', $this->nes_settings['venue_plural'], $this->nes_settings['venue_plural'], 'read', 'edit.php?post_type=nes_venue');
            }
            if (is_array($this->nes_settings['menu_visibility']['nes_menu_categories']) && !empty(array_intersect($user->roles, $this->nes_settings['menu_visibility']['nes_menu_categories']))) {
                add_submenu_page('nes-settings', 'Categories', 'Categories', 'read', 'edit-tags.php?taxonomy=event_category&post_type=nes_event');
            }
            if (is_array($this->nes_settings['menu_visibility']['nes_menu_settings']) && !empty(array_intersect($user->roles, $this->nes_settings['menu_visibility']['nes_menu_settings']))) {
                add_submenu_page('nes-settings', __('Settings','nes'), __('Settings','nes'), 'read', 'nes-settings', array($this, 'nes_settings_callback'));
            }
            if (is_array($this->nes_settings['menu_visibility']['nes_menu_import']) && !empty(array_intersect($user->roles, $this->nes_settings['menu_visibility']['nes_menu_import']))) {
                add_submenu_page('nes-settings', 'Import/Export', 'Import/Export', 'read', 'nes-import-export', array($this, 'nes_import_export_callback')); 
            }
            if (is_array($this->nes_settings['menu_visibility']['nes_menu_attendees']) && !empty(array_intersect($user->roles, $this->nes_settings['menu_visibility']['nes_menu_attendees']))) {    
                add_submenu_page(null, 'Attendee Lists', 'Attendee Lists', 'read', 'nes-attendee-lists', array($this, 'nes_attendee_lists_callback'));
            } 
            if (is_array($this->nes_settings['menu_visibility']['nes_menu_main']) && !empty(array_intersect($user->roles, $this->nes_settings['menu_visibility']['nes_menu_main']))) {
                unset($submenu['nes-settings'][0]);
            }
        }

        function admin_menu_highlight() {
            global $parent_file, $submenu_file, $post_type;
            switch($post_type){
                case 'nes_location' :
                case 'nes_venue' :
                    $parent_file = 'nes-settings';
                break;

                case 'nes_event' :
                    $parent_file = 'nes-settings';
                    $submenu_file = 'edit.php?post_status=publish&post_type=nes_event';
                break;
            }
        }

        function nes_settings_callback(){
            $path = $this->nes_settings['path'];
            include_once($path . 'views/admin/nes-settings.php');
        }          

        function nes_attendee_lists_callback(){
            $path = $this->nes_settings['path'];
            include_once($path . 'views/admin/nes-attendee-lists.php');
        }    

        function nes_import_export_callback(){
            $path = $this->nes_settings['path'];
            include_once($path . 'views/admin/nes-import-export.php');
        }

        function nes_styles() { 
            if(!wp_style_is('font-awesome')){
                wp_enqueue_style('font-awesome', $this->nes_settings['dir'] . 'css/font-awesome/css/fontawesome-all.min.css', false, $this->nes_settings['version']); 
            }            
            if(!wp_style_is('chosen')){
                wp_enqueue_style('chosen', $this->nes_settings['dir'] . 'js/chosen_v1.2.0/chosen.css', false, $this->nes_settings['version']); 
            }
            if(!wp_script_is('chosen')){
                wp_enqueue_script('nes-chosen', $this->nes_settings['dir'] . 'js/chosen_v1.2.0/chosen.jquery.js', array('jquery'), $this->nes_settings['version']); 
            }
            if(!wp_style_is('jquery-ui-css')){
                wp_enqueue_style('jquery-ui-css', "https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css", false, $this->nes_settings['version']); 
            }
            if(!wp_script_is('jquery-ui')){
                wp_enqueue_script('jquery-ui', "https://code.jquery.com/ui/1.12.1/jquery-ui.min.js", array('jquery'));
            }
            if(!wp_script_is('float-thead')){
                wp_enqueue_script('float-thead', $this->nes_settings['dir'] . 'js/floatThead/dist/jquery.floatThead.js', array('jquery'), $this->nes_settings['version']);
            }
            if(!wp_style_is('colpick')){
                wp_enqueue_style('colpick', $this->nes_settings['dir'] . 'js/colpick/css/colpick.css', false, $this->nes_settings['version']); 
            }
            if(!wp_script_is('colpick')){
                wp_enqueue_script('colpick', $this->nes_settings['dir'] . 'js/colpick/js/colpick.js', array('jquery'), $this->nes_settings['version']);
            }
            if(!wp_script_is('multidates-picker')){
                wp_enqueue_script('multidates-picker', $this->nes_settings['dir'] . 'js/jquery-ui.multidatespicker.js', array('jquery'), $this->nes_settings['version']); 
            }
            if(!wp_script_is('tablesorter')){
                wp_enqueue_script('tablesorter', $this->nes_settings['dir'] . 'js/tablesorter/jquery.tablesorter.js', array('jquery'), $this->nes_settings['version']); 
            }
            if(!wp_script_is('moment')){
                wp_enqueue_script('moment', $this->nes_settings['dir'] . 'js/moment.js', array('jquery'), $this->nes_settings['version']); 
            }
            if(!wp_script_is('intl-tel-input')){  
                wp_enqueue_script('intl-tel-input', $this->nes_settings['dir'] . 'js/intl-tel-input/build/js/intlTelInput.js', array('jquery'), $this->nes_settings['version']); 
            }
            if(!wp_style_is('intl-tel-input')){
                wp_enqueue_style('intl-tel-input', $this->nes_settings['dir'] . 'js/intl-tel-input/build/css/intlTelInput.css', false, $this->nes_settings['version']); 
            }

            if(is_admin()){
                wp_enqueue_style('nes-admin-css', $this->nes_settings['dir'] . 'css/nes-admin.css', false, $this->nes_settings['version']); 
            }else{
                wp_enqueue_script('touch-punch', $this->nes_settings['dir'] . 'js/jquery.ui.touch-punch.min.js', array('jquery'), $this->nes_settings['version']); 
                wp_enqueue_style('nes-css', $this->nes_settings['dir'] . 'css/nes.css', false, $this->nes_settings['version']); 
            }
        }

        function nes_venue_columns_filter($columns){
            $columns = array(
                'cb' => '<input type="checkbox" />',
                'title' => __('Title','nes'),
                'address' => __('Address','nes'),
                'color' => __('Display Color','nes'),
                'text' => __('Text Color','nes'),
                'date' => __('Date','nes')
            );
            return $columns;
        }

        function nes_manage_venue_columns($column, $post_id){
            switch($column){
                case 'color' :
                    echo '<div style="width:20px;height:20px;border:1px solid #000; background:#'.get_post_meta($post_id, 'nes_venue_color', true).';"></div>';
                    break;                
                case 'text' :
                    echo '<div style="width:20px;height:20px;border:1px solid #000; background:#'.get_post_meta($post_id, 'nes_venue_text_color', true).';"></div>';
                    break;
                case 'address' :
                    $full = null;
                    if($address = get_post_meta($post_id, 'nes_address', true)){
                        $full .= $address . ', ';
                    }
                    if($city = get_post_meta($post_id, 'nes_city', true)){
                        $full .= $city . ' ';
                    }
                    if($state = get_post_meta($post_id, 'nes_state', true)){
                        $full .= $state . ' ';
                    }
                    if($zipcode = get_post_meta($post_id, 'nes_zipcode', true)){
                        $full .= $zipcode . ' ';
                    }
                    if($country = get_post_meta($post_id, 'nes_country', true)){
                        $full .= $country . ' ';
                    }
                    if(!$full){
                        $full = '-';
                    }
                    echo $full;
                    break;
            }
        }

        function nes_location_columns_filter($columns){
            $columns = array(
                'cb' => '<input type="checkbox" />',
                'title' => __('Title','nes'),
                'venue' => $this->nes_settings['venue_single'],
                'date' => __('Date','nes')
            );
            return $columns;
        }

        function nes_manage_location_columns($column, $post_id){
            echo '<a href="/wp-admin/post.php?post='.get_post_meta($post_id,'nes_venue_id',true).'&action=edit">'.get_the_title(get_post_meta($post_id,'nes_venue_id',true)).'</a>';
        }

        function nes_add_event_status_filter(){
            global $typenow;

            //only add filter to post type you want
            if($typenow == 'nes_event'){
                $values = array(
                    'Pending' => 'pending', 
                    'Approved' => 'approved',
                    'Denied' => 'denied',
                );
                ?>
                <select name="nes_event_status_filter">
                <option value="">All statuses</option>
                <?php foreach ($values as $label => $value) : ?>
                    <?php 
                        $selected = '';
                        if(isset($_GET['nes_event_status_filter']) && $value == $_GET['nes_event_status_filter']){
                            $selected = ' selected';
                        }
                    ?>
                    <option value="<?php echo $value; ?>" <?php echo $selected; ?>><?php echo $label; ?></option>
                <?php endforeach; ?>
                </select>
                <?php
            }
        }

        function nes_event_status_filter($query){
            global $pagenow;
            global $typenow;

            if($typenow == 'nes_event' && $pagenow == 'edit.php' && isset($_GET['nes_event_status_filter']) && $_GET['nes_event_status_filter'] != '') {
                $args = array(
                    'meta_query' => array(
                        array(
                            'key' => 'nes_event_status',
                            'value' => $_GET['nes_event_status_filter']
                        )
                    )
                );
                $query->set('meta_query', $args);
            }

            return $query;
        }

        function nes_event_columns_filter($columns){
            $columns = array(
                'cb' => '<input type="checkbox" />',
                'title' => __('Title','nes'),
                'status' => __('Status','nes'),
                'venue' => $this->nes_settings['venue_single'],
                'location' => $this->nes_settings['location_plural'],
                'category' => __('Categories', 'nes'),
                'attendees' => __('Attendees','nes'),
                'recurring' => __('Recurring','nes'),
                'time' => __('Time','nes'),
                'event-date' => $this->nes_settings['event_single'].' '.__('Date','nes'),
                //'setup' => __('Setup Start','nes'),
                //'start' => __('Start Time','nes'),
                //'end' => __('End Time','nes'),
                //'cleanup' => __('Cleanup End','nes'),
                //'date' => __('Publish Date','nes')
            );
            $columns = apply_filters('nes_event_columns', $columns);
            return $columns;
        }

        function nes_manage_event_columns($column, $post_id){

            switch($column){
                case 'status' :
                    $status = get_post_meta($post_id, 'nes_event_status', true);
                    if($status){
                        echo ucfirst($status);
                    }else{
                        _e('&mdash;','nes');
                    }
                    break;
                case 'venue' :
                    if(get_post_meta($post_id, 'nes_event_type', true) == 'onsite'){
                        echo '<a href="/wp-admin/post.php?post='.get_post_meta($post_id,'nes_venue_id',true).'&action=edit">'.get_the_title(get_post_meta($post_id,'nes_venue_id',true)).'</a>';
                    }else{
                        echo get_post_meta($post_id,'nes_offsite_venue_name',true);
                    }
                    break;
                case 'location':
                    if(get_post_meta($post_id, 'nes_event_type', true) == 'onsite'){
                        $location_ids = get_post_meta($post_id, 'nes_location_id', true);
                        $first = true;
                        if($location_ids && is_array($location_ids)){
                            foreach ($location_ids as $location_id){
                                if($first){
                                    $first = false;
                                    echo '<a href="/wp-admin/post.php?post='.$location_id.'&action=edit">'.get_the_title($location_id).'</a>';
                                }else{
                                    echo ', <a href="/wp-admin/post.php?post='.$location_id.'&action=edit">'.get_the_title($location_id).'</a>';
                                }
                            }
                        }elseif($location_ids){
                            // has id, but not array
                            echo '<a href="/wp-admin/post.php?post='.$location_ids.'&action=edit">'.get_the_title($location_ids).'</a>';
                        }else{
                            echo '&mdash;';
                        }
                    }else{
                        _e('offsite','nes');
                    }
                    break;
                case 'category' : 
                    $cats = get_the_terms($post_id, 'event_category');
                    if($cats && !is_wp_error($cats)){
                        $delimeter = '';
                        foreach($cats as $cat){
                            echo $delimeter . '<a href="/wp-admin/edit.php?post_type=nes_event&event_category='.$cat->slug.'">'.$cat->name.'</a>';
                            $delimeter = ', ';
                        }
                    }else{
                        echo '&mdash;';
                    }
                    break;
                case 'attendees':
                    if(get_post_meta($post_id,'nes_ticketed_event',true) == 'yes'){
                        $attendees = get_post_meta($post_id, 'nes_attendees', true);
                        $attendee_count = 0;
                        if(is_array($attendees)){$attendee_count = count($attendees);}
                        echo '<a href="/wp-admin/admin.php?page=nes-attendee-lists&event_id='.$post_id.'">'.$attendee_count. ' of ' . get_post_meta($post_id,'nes_tickets_available',true).'</a>';
                    }else{
                        echo '&mdash;';
                    }
                    break;                
                case 'recurring':
                    if(get_post_meta($post_id,'nes_frequency_type',true) != 'one'){
                        if($series = get_post_meta($post_id,'nes_recurring_series',true)){
                            echo '<a href="'.admin_url().'edit.php?post_type=nes_event&nes_event_ids[]='.implode('&nes_event_ids[]=', array_keys($series)).'">';
                            echo (array_search($post_id,array_keys($series)) +1) .' of '. count($series);
                            echo '</a>';
                        }else{
                            echo '&mdash;';
                        }
                    }else{
                        echo '&mdash;';
                    }
                    break;
                case 'time':
                    echo date('g:i a', strtotime(get_post_meta($post_id, 'nes_start_time', true))) . '<br/>' . date('g:i a', strtotime(get_post_meta($post_id, 'nes_end_time', true)));
                    break;
                case 'event-date':
                    echo date('m/d/y', strtotime(get_post_meta($post_id, 'nes_event_date', true)));
                    break;
                case 'setup':
                    echo date('g:i a', strtotime(get_post_meta($post_id, 'nes_start_setup_time', true)));
                    break;  
                case 'start':
                    echo date('g:i a', strtotime(get_post_meta($post_id, 'nes_start_time', true)));
                    break;    
                case 'end':
                    echo date('g:i a', strtotime(get_post_meta($post_id, 'nes_end_time', true)));
                    break;
                case 'cleanup':
                    echo date('g:i a', strtotime(get_post_meta($post_id, 'nes_end_cleanup_time', true)));
                    break;
            }
        }

        function nes_event_sortable_columns_filter($columns){
            $columns['status'] = 'status';
            $columns['event-date'] = 'event_date';
            // $columns['setup'] = 'setup';
            // $columns['start'] = 'start';
            // $columns['end'] = 'end';
            // $columns['cleanup'] = 'cleanup';
            return $columns;
        }

        function nes_column_orderby($query){
            global $current_screen;

            if(isset($current_screen)){
                if(!is_admin() || (get_current_screen()->post_type != 'nes_event') || (get_current_screen()->base != 'edit')){return;}
                
                $orderby = $query->get('orderby');
                if(empty($orderby)){
                    $query->set('meta_key','nes_event_date');
                    $query->set('orderby','meta_value');
                    if(isset($_GET['post_status']) && $_GET['post_status'] == 'past_events'){
                        $query->set('order','desc');
                    }else{
                         $query->set('order','asc');
                    }
                }else{
                    
                    switch($orderby){
                        case 'status':
                            $query->set('meta_key','nes_event_status');
                            $query->set('orderby','meta_value');
                            break;
                        case 'event_date':
                            $query->set('meta_key','nes_event_date');
                            $query->set('orderby','meta_value');
                            break;
                        case 'title':
                            $query->set('orderby','title');
                            break;
                        default:
                            break;
                    }
                }
            }
            return $query;
        }

        // allow for custom query to only show events in the selected recurring series
        function nes_event_list_filter($query){
            if(is_admin() && $query->query['post_type'] == 'nes_event'){
                $qv = &$query->query_vars;
                if(!empty($_GET['nes_event_ids'])){
                    $qv['post__in'] = $_GET['nes_event_ids'];
                }
            }
        }


        // setup custom "past event" post status so we dont have to clog up the upcoming events
        function nes_append_post_status_list(){
             global $post;
             $complete = '';
             $label = '';
             if($post->post_type == 'nes_event'){
                  if($post->post_status == 'past_events'){
                       $complete = ' selected=\"selected\"';
                       $label = '<span id=\"post-status-display\">Past Events</span>';
                  }
                  echo '
                  <script>
                  jQuery(document).ready(function($){
                       $("select#post_status").append("<option value=\"past_events\" '.$complete.'>Past Events</option>");
                       $(".misc-pub-section label").append("'.$label.'");
                  });
                  </script>
                  ';
             }
        }

        function nes_append_post_status_bulk_edit() {
            echo '
            <script>
                jQuery(document).ready(function($){
                    $(".inline-edit-status select ").append("<option value=\"past_events\">Past Events</option>");
                });
            </script>
            ';
        }

        ////// CURRENTLY NOT USED ///////////////
        // ajax request - event ticket form fields
        function nes_ajax_get_gravity_form_fields(){
            $form_id = $_POST['form_id'];
            $post_id = $_POST['post_id'];
            ob_start();  ?>
            <!--<p class="nes-gf-first-name">
                <label class="nes-styled"><?php _e('First Name','nes'); ?></label>
                <?php echo $this->nes_get_gf_form_fields_select($form_id, 'nes_gf_first_name', get_post_meta($post_id, 'nes_gf_first_name', true)); ?>
            </p>    
            <p class="nes-gf-last-name">
                <label class="nes-styled"><?php _e('Last Name','nes'); ?></label>
                <?php echo $this->nes_get_gf_form_fields_select($form_id, 'nes_gf_last_name', get_post_meta($post_id, 'nes_gf_last_name', true)); ?>
            </p>               
            <p class="nes-gf-quantity">
                <label class="nes-styled"><?php _e('Ticket Quantity (the "quantity" field for the product)','nes'); ?></label>
                <?php echo $this->nes_get_gf_form_fields_select($form_id, 'nes_gf_quantity', get_post_meta($post_id, 'nes_gf_quantity', true)); ?>
            </p>-->                
            <p class="nes-gf-attendees">
                <label class="nes-styled"><?php _e('Attendees (should be a "list" field)','nes'); ?></label>
                <?php echo $this->nes_get_gf_form_fields_select($form_id, 'nes_gf_attendees', get_post_meta($post_id, 'nes_gf_attendees', true)); ?>
            </p>            
            <?php echo ob_get_clean();
            die();
        }         

        /////// CURRENTLY NOT USED (from ajax function above) /////////
        function nes_get_gf_form_fields_select($form_id, $select_id, $meta_value){
            // check for gravity forms
            if(!class_exists(RGFormsModel)){return 'Gravity Forms is not installed.';}

            $form = RGFormsModel::get_form_meta($form_id);
            $fields = array();

            // get the form fields
            if(is_array($form["fields"])){
                foreach($form["fields"] as $field){
                    if(isset($field["inputs"]) && is_array($field["inputs"])){

                        foreach($field["inputs"] as $input)
                            $fields[] =  array($input["id"], GFCommon::get_label($field, $input["id"]));
                    }
                    else if(!rgar($field, 'displayOnly')){
                        $fields[] =  array($field["id"], GFCommon::get_label($field));
                    }
                }
            }

            // make the select box
            $select = 'No fields found for this form';
            if($fields){
                $select = '<select name="'.$select_id.'">';
                foreach($fields as $field){
                    $select .= '<option value="'.$field[0].'" '; if($field[0] == $meta_value){$select .= 'selected="selected"';} $select .= '>'.$field[1].'</option>';
                }
                $select .= '</select>';
            }

            return $select;
        }
        
        function nes_get_venues(){
            global $wpdb;
            $query_string = "
                SELECT DISTINCT p.*
                FROM $wpdb->posts p
                WHERE p.post_type = 'nes_venue'
                AND p.post_status = 'publish'
            ";
            $venues = $wpdb->get_results($query_string, OBJECT);
            return $venues;
        }   

        function nes_get_venue_name($venue_id){
            $venue = get_post($venue_id);
            return $venue->post_title;
        }

        function nes_get_venue_address($post_id){
            $venue_id = get_post_meta($post_id, 'nes_venue_id', true); 
            $street = get_post_meta($venue_id, 'nes_address', true);    
            $city = get_post_meta($venue_id, 'nes_city', true); 
            $state = get_post_meta($venue_id, 'nes_state', true);
            $zip = get_post_meta($venue_id, 'nes_zipcode', true);   
            $country = get_post_meta($venue_id, 'nes_country', true);   
            $address = $street . ' ' . $city . ' ' . $state . ' ' . $zip . ' ' . $country;
            return $address;
        }

        // ajax request
        function nes_ajax_get_venue_locations(){
            $venue_id = sanitize_text_field($_POST['venue_id']);

            global $wpdb;
            $query_string = "
                SELECT DISTINCT p.ID
                FROM $wpdb->posts p
                LEFT JOIN $wpdb->postmeta m1 ON p.ID = m1.post_id
                WHERE p.post_type = 'nes_location'
                AND p.post_status = 'publish'
                AND m1.meta_key = 'nes_venue_id'
                AND m1.meta_value = '$venue_id'
            ";
            $locations = $wpdb->get_results($query_string, OBJECT);

            if($locations){
                $html = '<p><label class="nes-styled">'.$this->nes_settings['location_plural'].': </label>';
                $html .= '<select id="nes-location-id" name="nes_location_id" multiple>';
                $html .=  '<option></option>';
                foreach($locations as $location){
                    $html .= '<option value="'.$location->ID.'" '; 
                    if(get_post_meta($event->ID, 'nes_location_id', true) == $location->ID){$html.= '"selected"';}
                    $html.= '>'.get_the_title($location->ID).'</option>';
                }
                $html .='</select></p>';
            }else{
                 $html = '<p><label class="nes-styled">'.$this->nes_settings['location_plural'].': </label><span>Sorry, no '.$this->nes_settings['location_plural'].' where found for the '.$this->nes_settings['venue_single']. ' selected.</span></p>';
            }

            echo $html;
            die();
        }    

        function nes_get_venue_locations($venue_id){
            global $wpdb;
            $query_string = "
                SELECT DISTINCT p.*
                FROM $wpdb->posts p
                LEFT JOIN $wpdb->postmeta m1 ON p.ID = m1.post_id
                WHERE p.post_type = 'nes_location'
                AND p.post_status = 'publish'
                AND m1.meta_key = 'nes_venue_id'
                AND m1.meta_value = $venue_id
            ";
            $locations = $wpdb->get_results($query_string, OBJECT);
            return $locations;
        }        

        function nes_get_location_names($location_ids){
            global $wpdb;
            $location_id_str = implode("','", $location_ids);
            $query_string = "
                SELECT DISTINCT p.post_title
                FROM $wpdb->posts p
                WHERE p.post_type = 'nes_location'
                AND p.post_status = 'publish'
                AND p.ID IN('$location_id_str')
            ";
            $locations = $wpdb->get_results($query_string, OBJECT);

            $location_names = array();
            if($locations){
                foreach($locations as $location){
                    $location_names[] = $location->post_title;
                }
            }
            return $location_names;
        }

        function nes_get_events(){
            global $wpdb;
            $query_string = "
                SELECT DISTINCT p.*
                FROM $wpdb->posts p
                WHERE p.post_type = 'nes_event'
                AND p.post_status = 'publish'
            ";
            $events = $wpdb->get_results($query_string, OBJECT);
            return $events;
        }

        function nes_venue_meta_box(){
            add_meta_box('nes-venue-meta', sprintf(__('%1$s Details','nes'),$this->nes_settings['venue_single']), array($this, 'nes_venue_meta_box_html'), 'nes_venue', 'normal', 'high');
        }

        function nes_venue_meta_box_html(){
            include_once('views/admin/meta/nes-venue-meta.php');
        }    

        function nes_location_meta_box(){
            add_meta_box('nes-location-meta', sprintf(__('%1$s Details','nes'),$this->nes_settings['location_single']), array($this, 'nes_location_meta_box_html'), 'nes_location', 'normal', 'high');
        }

        function nes_location_meta_box_html(){
            include_once('views/admin/meta/nes-location-meta.php');
        }   

        function nes_event_meta_box(){
            add_meta_box('nes-event-meta', sprintf(__('%1$s Details','nes'),$this->nes_settings['event_single']), array($this, 'nes_event_meta_box_html'), 'nes_event', 'normal', 'high');
        }

        function nes_event_meta_box_html(){
            include_once('views/admin/meta/nes-event-meta.php');
        }

        function nes_save_venue_meta($post_id){
            // Check if our nonce is set.
            if(!isset($_POST['venue_meta_nonce'])){return;}
            // Verify that the nonce is valid.
            if(!wp_verify_nonce($_POST['venue_meta_nonce'], 'save_venue_meta')){return;}
            // If this is an autosave, our form has not been submitted, so we don't want to do anything.
            if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){return;}
            // Check the user's permissions.
            if(isset($_POST['post_type']) && 'nes_venue' == $_POST['post_type']){
                if(!current_user_can('edit_page', $post_id)){return;}
                if(!current_user_can('edit_post', $post_id)){return;}
            }
            // sanitize data
            $address = sanitize_text_field($_POST['nes_address']);
            $city = sanitize_text_field($_POST['nes_city']);
            $state = sanitize_text_field($_POST['nes_state']);
            $zipcode = sanitize_text_field($_POST['nes_zipcode']);
            $country = sanitize_text_field($_POST['nes_country']);
            $email = sanitize_text_field($_POST['nes_contact_email']);
            $phone = sanitize_text_field($_POST['nes_phone']);
            $website = sanitize_text_field($_POST['nes_website']);
            $mon_start = sanitize_text_field($_POST['nes_venue_monday_start']);
            $mon_end = sanitize_text_field($_POST['nes_venue_monday_end']); 
            $tues_start = sanitize_text_field($_POST['nes_venue_tuesday_start']);
            $tues_end = sanitize_text_field($_POST['nes_venue_tuesday_end']);            
            $wed_start = sanitize_text_field($_POST['nes_venue_wednesday_start']);
            $wed_end = sanitize_text_field($_POST['nes_venue_wednesday_end']);            
            $thurs_start = sanitize_text_field($_POST['nes_venue_thursday_start']);
            $thurs_end = sanitize_text_field($_POST['nes_venue_thursday_end']);            
            $fri_start = sanitize_text_field($_POST['nes_venue_friday_start']);
            $fri_end = sanitize_text_field($_POST['nes_venue_friday_end']);            
            $sat_start = sanitize_text_field($_POST['nes_venue_saturday_start']);
            $sat_end = sanitize_text_field($_POST['nes_venue_saturday_end']);            
            $sun_start = sanitize_text_field($_POST['nes_venue_sunday_start']);
            $sun_end = sanitize_text_field($_POST['nes_venue_sunday_end']);
            $google_map = sanitize_text_field($_POST['nes_google_map']);
            $venue_color = sanitize_text_field($_POST['nes_venue_color']);
            $venue_text_color = sanitize_text_field($_POST['nes_venue_text_color']);

            // blackouts
            $blackout_locations = $_POST['nes_blackout_locations'];
            $blackout_start_date = $_POST['nes_blackout_start_date'];
            $blackout_end_date = $_POST['nes_blackout_end_date'];
            $blackout_start_time = $_POST['nes_blackout_start_time'];
            $blackout_end_time = $_POST['nes_blackout_end_time'];
            update_post_meta($post_id, 'nes_blackout_locations', $blackout_locations);
            update_post_meta($post_id, 'nes_blackout_start_date', $blackout_start_date);
            update_post_meta($post_id, 'nes_blackout_end_date', $blackout_end_date);
            update_post_meta($post_id, 'nes_blackout_start_time', $blackout_start_time);
            update_post_meta($post_id, 'nes_blackout_end_time', $blackout_end_time);


            // Update the meta field in the database.
            update_post_meta($post_id, 'nes_address', $address);
            update_post_meta($post_id, 'nes_city', $city);
            update_post_meta($post_id, 'nes_state', $state);
            update_post_meta($post_id, 'nes_zipcode', $zipcode);
            update_post_meta($post_id, 'nes_country', $country);
            update_post_meta($post_id, 'nes_contact_email', $email);
            update_post_meta($post_id, 'nes_phone', $phone);
            update_post_meta($post_id, 'nes_website', $website);
            update_post_meta($post_id, 'nes_venue_monday_start', $mon_start);
            update_post_meta($post_id, 'nes_venue_monday_end', $mon_end);
            update_post_meta($post_id, 'nes_venue_tuesday_start', $tues_start);
            update_post_meta($post_id, 'nes_venue_tuesday_end', $tues_end);
            update_post_meta($post_id, 'nes_venue_wednesday_start', $wed_start);
            update_post_meta($post_id, 'nes_venue_wednesday_end', $wed_end);
            update_post_meta($post_id, 'nes_venue_thursday_start', $thurs_start);
            update_post_meta($post_id, 'nes_venue_thursday_end', $thurs_end);
            update_post_meta($post_id, 'nes_venue_friday_start', $fri_start);
            update_post_meta($post_id, 'nes_venue_friday_end', $fri_end);
            update_post_meta($post_id, 'nes_venue_saturday_start', $sat_start);
            update_post_meta($post_id, 'nes_venue_saturday_end', $sat_end);
            update_post_meta($post_id, 'nes_venue_sunday_start', $sun_start);
            update_post_meta($post_id, 'nes_venue_sunday_end', $sun_end);
            update_post_meta($post_id, 'nes_google_map', $google_map);
            update_post_meta($post_id, 'nes_venue_color', $venue_color);
            update_post_meta($post_id, 'nes_venue_text_color', $venue_text_color);

            // check if default venue has been set and update if not
            $default_venue = get_option('nes_default_venue');
            if(!$default_venue){
                update_option('nes_default_venue', $post_id);
            }
        }

        function nes_save_location_meta($post_id){
            // Check if our nonce is set.
            if(!isset($_POST['location_meta_nonce'])){return;}
            // Verify that the nonce is valid.
            if(!wp_verify_nonce($_POST['location_meta_nonce'], 'save_location_meta')){return;}
            // If this is an autosave, our form has not been submitted, so we don't want to do anything.
            if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){return;}
            // Check the user's permissions.
            if(isset($_POST['post_type']) && 'nes_location' == $_POST['post_type']){
                if(!current_user_can('edit_page', $post_id)){return;}
                if(!current_user_can('edit_post', $post_id)){return;}
            }
            // sanitize data
            $venue_id = sanitize_text_field($_POST['nes_venue_id']);
            $avail = sanitize_text_field($_POST['nes_venue_availability']);
            $mon_start = sanitize_text_field($_POST['nes_location_monday_start']);
            $mon_end = sanitize_text_field($_POST['nes_location_monday_end']); 
            $tues_start = sanitize_text_field($_POST['nes_location_tuesday_start']);
            $tues_end = sanitize_text_field($_POST['nes_location_tuesday_end']);            
            $wed_start = sanitize_text_field($_POST['nes_location_wednesday_start']);
            $wed_end = sanitize_text_field($_POST['nes_location_wednesday_end']);            
            $thurs_start = sanitize_text_field($_POST['nes_location_thursday_start']);
            $thurs_end = sanitize_text_field($_POST['nes_location_thursday_end']);            
            $fri_start = sanitize_text_field($_POST['nes_location_friday_start']);
            $fri_end = sanitize_text_field($_POST['nes_location_friday_end']);            
            $sat_start = sanitize_text_field($_POST['nes_location_saturday_start']);
            $sat_end = sanitize_text_field($_POST['nes_location_saturday_end']);            
            $sun_start = sanitize_text_field($_POST['nes_location_sunday_start']);
            $sun_end = sanitize_text_field($_POST['nes_location_sunday_end']);

            // Update the meta field in the database.
            update_post_meta($post_id, 'nes_venue_id', $venue_id);
            update_post_meta($post_id, 'nes_venue_availability', $avail);
            update_post_meta($post_id, 'nes_location_monday_start', $mon_start);
            update_post_meta($post_id, 'nes_location_monday_end', $mon_end);
            update_post_meta($post_id, 'nes_location_tuesday_start', $tues_start);
            update_post_meta($post_id, 'nes_location_tuesday_end', $tues_end);
            update_post_meta($post_id, 'nes_location_wednesday_start', $wed_start);
            update_post_meta($post_id, 'nes_location_wednesday_end', $wed_end);
            update_post_meta($post_id, 'nes_location_thursday_start', $thurs_start);
            update_post_meta($post_id, 'nes_location_thursday_end', $thurs_end);
            update_post_meta($post_id, 'nes_location_friday_start', $fri_start);
            update_post_meta($post_id, 'nes_location_friday_end', $fri_end);
            update_post_meta($post_id, 'nes_location_saturday_start', $sat_start);
            update_post_meta($post_id, 'nes_location_saturday_end', $sat_end);
            update_post_meta($post_id, 'nes_location_sunday_start', $sun_start);
            update_post_meta($post_id, 'nes_location_sunday_end', $sun_end);
        }

        function nes_save_event_meta($post_id, $post){

            // Check if our nonce is set.
            if(!isset($_POST['event_meta_nonce'])){return;}
            // Verify that the nonce is valid.
            if(!wp_verify_nonce($_POST['event_meta_nonce'], 'save_event_meta')){return;}
            // If this is an autosave, our form has not been submitted, so we don't want to do anything.
            if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){return;}
            // Check the user's permissions.
            if(isset($_POST['post_type']) && 'nes_event' == $_POST['post_type']){
                if(!current_user_can('edit_page', $post_id)){return;}
                if(!current_user_can('edit_post', $post_id)){return;}
            }       

            // get post ID from $_POST data to account for new posts and incorrect "revision" id
            $post_id = $_POST['post_ID'];

            // remove this function from action to prevent infinite loop saving
            remove_action('save_post', array($this,'nes_save_event_meta'), 11);   

            // get current status
            $old_status = get_post_meta($post_id, 'nes_event_status', true);

            // get current event venue/location/date/time/frequency
            $old_venue_id = get_post_meta($post_id, 'nes_venue_id', true);
            $old_location_ids = get_post_meta($post_id, 'nes_location_id', true);
            $old_event_date = get_post_meta($post_id, 'nes_event_date', true);
            $old_start_time = get_post_meta($post_id, 'nes_start_time', true);
            $old_start_setup_time = get_post_meta($post_id, 'nes_start_setup_time', true);
            $old_end_time = get_post_meta($post_id, 'nes_end_time', true);
            $old_end_cleanup_time = get_post_meta($post_id, 'nes_end_cleanup_time', true);
            $old_frequency_end_date = get_post_meta($post_id, 'nes_frequency_end_date', true);
            $old_frequency_type = get_post_meta($post_id,'nes_frequency_type', true);
            $old_frequency_monthly = get_post_meta($post_id,'nes_frequency_monthly', true);
            $old_frequency_weekly = get_post_meta($post_id,'nes_frequency_weekly', true);
            $old_series_start_date = get_post_meta($post_id,'nes_series_start_date', true);

            // sanitize data
            $status = sanitize_text_field($_POST['nes_event_status']);
            $venue_id = sanitize_text_field($_POST['nes_venue_id']);
            $location_ids = array();

            // force array for location ids
            if(isset($_POST['nes_location_id'])){
                $location_ids = $_POST['nes_location_id'];
            }

            $event_date = sanitize_text_field($_POST['nes_event_date']);
            $custom_dates = sanitize_text_field($_POST['nes_custom_date']);

            // remove event date from custom dates
            if($custom_dates){
                $custom_dates_arr = explode(', ', $custom_dates);

                if(is_array($custom_dates_arr)){
                    // remove the start date if included
                    foreach($custom_dates_arr as $k => $custom_date){
                        if($custom_date == date('m/d/Y', strtotime($event_date))){
                            unset($custom_dates_arr[$k]);
                            $custom_dates = implode(', ', $custom_dates_arr); 
                            break;
                        }
                    }
                }
            }         

            $conflict_checking = sanitize_text_field($_POST['nes_conflict_checking']);
            $start_setup_time = sanitize_text_field($_POST['nes_start_setup_time']);
            $end_cleanup_time = sanitize_text_field($_POST['nes_end_cleanup_time']);
            $start_time = sanitize_text_field($_POST['nes_start_time']);
            $end_time = sanitize_text_field($_POST['nes_end_time']);
            $name = sanitize_text_field($_POST['nes_event_name']);
            $phone = sanitize_text_field($_POST['nes_event_phone']);
            $email = sanitize_text_field($_POST['nes_event_email']);
            $website = sanitize_text_field($_POST['nes_event_website']);
            $comments = sanitize_text_field($_POST['nes_event_comments']);
            $setup_needs = sanitize_text_field($_POST['nes_event_setup']);
            $av_needs = sanitize_text_field($_POST['nes_event_av']);
            $ticketed_event = sanitize_text_field($_POST['nes_ticketed_event']);
            $ticket_login = sanitize_text_field($_POST['nes_ticket_login']);
            $ticket_per_purchase = sanitize_text_field($_POST['nes_ticket_per_purchase']);
            $event_type = sanitize_text_field($_POST['nes_event_type']);
            $nes_private_public = sanitize_text_field($_POST['nes_private_public']);
            if($event_type == 'offsite'){$nes_private_public = 'public';}
            $nes_featured_event = sanitize_text_field($_POST['nes_featured_event']);
            $offsite_venue_name = sanitize_text_field($_POST['nes_offsite_venue_name']);
            $offsite_venue_address = sanitize_text_field($_POST['nes_offsite_venue_address']);
            $series_start_date = $event_date;


            // Update the meta field in the database.
            update_post_meta($post_id, 'nes_event_status', $status);
            update_post_meta($post_id, 'nes_venue_id', $venue_id);
            update_post_meta($post_id, 'nes_location_id', $location_ids);
            update_post_meta($post_id, 'nes_event_date', $event_date);
            update_post_meta($post_id, 'nes_start_time', $start_time);
            update_post_meta($post_id, 'nes_end_time', $end_time);
            update_post_meta($post_id, 'nes_event_name', $name);
            update_post_meta($post_id, 'nes_event_phone', $phone);
            update_post_meta($post_id, 'nes_event_email', $email);
            update_post_meta($post_id, 'nes_event_website', $website);
            update_post_meta($post_id, 'nes_event_comments', $comments);
            update_post_meta($post_id, 'nes_event_setup', $setup_needs);
            update_post_meta($post_id, 'nes_event_av', $av_needs);
            update_post_meta($post_id, 'nes_ticketed_event', $ticketed_event);
            update_post_meta($post_id, 'nes_ticket_login', $ticket_login);
            update_post_meta($post_id, 'nes_ticket_per_purchase', $ticket_per_purchase);
            update_post_meta($post_id, 'nes_event_type', $event_type);
            update_post_meta($post_id, 'nes_private_public', $nes_private_public);
            update_post_meta($post_id, 'nes_featured_event', $nes_featured_event);
            update_post_meta($post_id, 'nes_offsite_venue_name', $offsite_venue_name);
            update_post_meta($post_id, 'nes_offsite_venue_address', $offsite_venue_address);
            update_post_meta($post_id, 'nes_series_start_date', $series_start_date);

            if(!empty($start_setup_time)){
                update_post_meta($post_id, 'nes_start_setup_time', $start_setup_time);
            }else{
                update_post_meta($post_id, 'nes_start_setup_time', $start_time);
            }
          
            if(!empty($end_cleanup_time)){
                update_post_meta($post_id, 'nes_end_cleanup_time', $end_cleanup_time);
            }else{
                update_post_meta($post_id, 'nes_end_cleanup_time', $end_time);
            }

            // Recurring info
            $frequency_type = sanitize_text_field($_POST['nes_frequency_type']);
            $frequency_monthly = sanitize_text_field($_POST['nes_frequency_monthly']);
            $frequency_weekly = '';

            // set weekly to event date day if none submitted
            if(empty($_POST['nes_frequency_weekly'])){
                $frequency_weekly = array(strtolower(date('l', strtotime($event_date))));
            }else{
                $frequency_weekly = $_POST['nes_frequency_weekly'];
            }

            $frequency_end_date = sanitize_text_field($_POST['nes_frequency_end_date']);
            update_post_meta($post_id,'nes_frequency_type',$frequency_type);
            update_post_meta($post_id,'nes_frequency_monthly',$frequency_monthly);
            update_post_meta($post_id,'nes_frequency_weekly',$frequency_weekly);
            update_post_meta($post_id,'nes_frequency_end_date',$frequency_end_date);

            // Ticket info
            $nes_tickets_available = sanitize_text_field($_POST['nes_tickets_available']);
            update_post_meta($post_id,'nes_tickets_available', $nes_tickets_available);            

            $nes_registration_link = sanitize_text_field($_POST['nes_registration_link']);
            update_post_meta($post_id,'nes_registration_link', $nes_registration_link);

            // Gravity form ticket fields
            if(isset($_POST['nes_gravity_form'])){
                $gf = sanitize_text_field($_POST['nes_gravity_form']);
                update_post_meta($post_id,'nes_gravity_form',$gf);
            }

            if(isset($_POST['nes_gf_attendees'])){
                $gf_attendees = sanitize_text_field($_POST['nes_gf_attendees']);
                update_post_meta($post_id,'nes_gf_attendees',$gf_attendees);
            }

            // $gf_first_name = sanitize_text_field($_POST['nes_gf_first_name']);
            // $gf_last_name = sanitize_text_field($_POST['nes_gf_last_name']);
            // $gf_quantity = sanitize_text_field($_POST['nes_gf_quantity']);
            // update_post_meta($post_id,'nes_gf_first_name',$gf_first_name);
            // update_post_meta($post_id,'nes_gf_last_name',$gf_last_name);
            // update_post_meta($post_id,'nes_gf_quantity',$gf_quantity);

            if(isset($_POST['nes_ticket_type'])){
                $ticket_type = $_POST['nes_ticket_type']; // repeater array
                update_post_meta($post_id, 'nes_ticket_type', $ticket_type);            
            }

            if(isset($_POST['nes_ticket_description'])){
                $ticket_description = $_POST['nes_ticket_description']; // repeater array
                update_post_meta($post_id, 'nes_ticket_description', $ticket_description);
            }

            if(isset($_POST['nes_ticket_price'])){
                $ticket_price = $_POST['nes_ticket_price']; // repeater array
                update_post_meta($post_id, 'nes_ticket_price', $ticket_price); 
            }           

            if(isset($_POST['nes_ticket_suggested'])){
                $ticket_suggested = $_POST['nes_ticket_suggested']; // repeater array
                update_post_meta($post_id, 'nes_ticket_suggested', $ticket_suggested);
            }

            // save "uncategorized" if no categories where chosen
            $this->nes_maybe_save_uncategorized($post_id);

            // get edit type
            $edit_type = '';
            if(isset($_POST['nes_edit_type'])){
               $edit_type = sanitize_text_field($_POST['nes_edit_type']); 
            }
            
            // check if this is a new event that will start a recurring series
            if($frequency_type != 'one'){

                // create/update recurring events if necessary
                if($edit_type != 'one'){

                    // check if already in a series
                    if($series_ids = get_post_meta($post_id,'nes_recurring_series',true)){

                        // check if upcoming only
                        if($edit_type == 'upcoming'){

                            // only create new for all upcoming series ids
                            $existing_series = array();
                            $upcoming_series = array();
                            foreach($series_ids as $series_id => $date){
                                if($date < $event_date){
                                    $existing_series[$series_id] = $date;
                                }else{
                                    $upcoming_series[$series_id] = $date;
                                }
                            }

                            // update existing series
                            if($existing_series){
                                foreach($existing_series as $series_id => $date){

                                    // update the series
                                    update_post_meta($series_id,'nes_recurring_series',$existing_series);

                                    // update the series end date to one day before this event
                                    update_post_meta($series_id,'nes_frequency_end_date',date('Y-m-d',(strtotime('-1 day',strtotime($event_date)))));
                                }
                            }

                            // update the "series start date" for this post and the new series
                            $series_start_date = $event_date;
                            update_post_meta($post_id,'nes_series_start_date',$series_start_date);

                            // create new upcoming series
                            $available_dates = $this->nes_get_available_dates($post_id, $venue_id, $location_ids, $series_start_date, $event_type, $frequency_type, $frequency_end_date, $frequency_weekly, $frequency_monthly, $custom_dates, $conflict_checking);
                            if($available_dates){

                                // delete previous "upcoming events"
                                if($upcoming_series){
                                    foreach($upcoming_series as $series_id => $date){
                                        // ignore current post
                                        if($series_id != $post_id){
                                            wp_delete_post($series_id, true);  
                                        }
                                    }
                                }

                                // create new event series
                                $this->nes_create_new_event_series($post_id, $post, $available_dates, $series_start_date);
                            }

                        }else{
                            // update all in series, set empty date array
                            $available_dates = array();

                            // check if we are updating venue/location/date/time/frequency or not             
                            if(($series_start_date != $old_series_start_date) || ($venue_id != $old_venue_id) || ($location_ids != $old_location_ids) || ($event_date != $old_event_date) || ($start_setup_time != $old_start_setup_time) || ($start_time != $old_start_time) || ($end_time != $old_end_time) || ($end_cleanup_time != $old_end_cleanup_time) || ($frequency_end_date != $old_frequency_end_date) || ($frequency_type != $old_frequency_type) || ($frequency_monthly != $old_frequency_monthly) || ($frequency_weekly != $old_frequency_weekly)){
                                
                                // get dates
                                $available_dates = $this->nes_get_available_dates($post_id, $venue_id, $location_ids, $series_start_date, $event_type, $frequency_type, $frequency_end_date, $frequency_weekly, $frequency_monthly, $custom_dates, $conflict_checking);
                                
                                if($available_dates){
                                    foreach($series_ids as $series_id => $date){
                                        // ignore current post
                                        if($series_id != $post_id){
                                            wp_delete_post($series_id, true);  
                                        }
                                    }

                                    // create new event series
                                    $this->nes_create_new_event_series($post_id, $post, $available_dates, $series_start_date);
                                }
                            }else{
                                // update the series, ignore date/recurring meta
                                foreach($series_ids as $series_id => $event_date){
                                    // ignore this event
                                    if($series_id != $post_id){

                                        // update post data
                                        $event = array(
                                            'ID'           => $series_id,
                                            'post_title'   => $post->post_title,
                                            'post_content' => $post->post_content,
                                        );
                                        wp_update_post($event);

                                        // update featured image

                                        // update meta values
                                        $post_meta_keys = get_post_custom_keys($post_id);
                                        if($post_meta_keys){
                                            $ignore_keys = array('nes_recurring_series','nes_venue_id','nes_location_id','nes_start_setup_time','nes_start_time','nes_end_time','nes_end_cleanup_time','nes_event_date','nes_frequency_end_date','nes_frequency_weekly','nes_frequency_monthly','nes_frequency_type');
                                            foreach($post_meta_keys as $meta_key){
                                                // ignore event date/recurring
                                                if(!in_array($meta_key, $ignore_keys)){
                                                    $meta_values = get_post_custom_values($meta_key, $post_id);
                                                    foreach($meta_values as $meta_value){
                                                        $meta_value = maybe_unserialize($meta_value);
                                                        update_post_meta($series_id, $meta_key, $meta_value);
                                                    }
                                                }
                                            }
                                        }

                                        // update all taxonomies
                                        $post_taxonomies = get_object_taxonomies('nes_event');
                                        if($post_taxonomies){
                                            foreach($post_taxonomies as $taxonomy){
                                                $post_terms = wp_get_object_terms($post_id, $taxonomy, array('orderby' => 'term_order'));
                                                $terms = array();
                                                for($i=0; $i<count($post_terms); $i++){
                                                    $terms[] = $post_terms[$i]->slug;
                                                }
                                                wp_set_object_terms($series_id, $terms, $taxonomy);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }else{

                        // get dates
                        $available_dates = $this->nes_get_available_dates($post_id, $venue_id, $location_ids, $event_date, $event_type, $frequency_type, $frequency_end_date, $frequency_weekly, $frequency_monthly, $custom_dates, $conflict_checking);

                        // create new series
                        $this->nes_create_new_event_series($post_id, $post, $available_dates, $series_start_date);
                        
                    }
                }else{
                    // remove from series
                    $this->nes_trash_event_meta($post_id);
                }
            }

            // check for status change to send notification
            if($this->nes_settings['user_approved_notification'] == 'yes'){
                if(($old_status != $status) && ($status != 'pending')){
                    if(empty($this->nes_settings['user_subject_line_approved'])){
                        $subject = sprintf(__('Your %1$s has been %2$s','nes'),$this->nes_settings['event_single'],ucfirst($status));
                    }else{
                        $subject = $this->nes_settings['user_subject_line_approved'];
                    }
                    $content = '<p>'.sprintf(__('The following %1$s has been %2$s','nes'),$this->nes_settings['event_single'],ucfirst($status)).'.</p>';
                    $content .= '<ul><li><strong>'.__('Title','nes').': </strong> '.get_the_title($post_id).'</li>';
                    
                    // check for offsite
                    if($event_type == 'offsite'){
                        $content .= '<li><strong>Venue: </strong> '.$offsite_venue_name.'</li>';
                        $content .= '<li><strong>Address: </strong> '.$offsite_venue_address.'</li>';

                    }else{
                        $content .= '<li><strong>'.$this->nes_settings['venue_single'].': </strong> '.get_the_title($venue_id).'</li>';
            
                        // get location names if any
                        if($location_ids){
                            $location_names = $this-> nes_get_location_names($location_ids); 
                            $content .= '<li><strong>'.$this->nes_settings['location_plural'].': </strong> '.implode(', ', $location_names).'</li>';
                        }   
                    }

                    // get dates
                    $series_ids = get_post_meta($post_id, 'nes_recurring_series', true);
                    $event_date = get_post_meta($post_id, 'nes_event_date', true);

                    $dates = array();
                    if($series_ids){
                        if($edit_type == 'all'){
                            foreach($series_ids as $series_id => $series_date){
                                $dates[] = $series_date;
                            }
                        }else if($edit_type == 'upcoming'){
                            foreach($series_ids as $series_id => $series_date){
                                if(strtotime($series_date) >= strtotime($event_date)){
                                    $dates[] = $series_date;
                                }
                            }
                        }else{
                            $dates[] = $event_date;
                        }
                    }else{
                        $dates[] = $event_date;
                    }

                    if($dates){
                        $content .= '<li><strong>'.__('Date(s)','nes').': </strong><ul>';
                        foreach($dates as $date){
                            $content .= '<li>' . date('F jS, Y', strtotime($date)) . '</li>';
                        }
                        $content .= '</ul></li>';
                    }

                    if($start_setup_time){
                        $content .= '<li><strong>'.__('Start Setup Time','nes').': </strong> '.date('g:i a', strtotime($start_setup_time)).'</li>';
                    }
                    $content .= '<li><strong>'.__('Start Time','nes').': </strong> '.date('g:i a', strtotime($start_time)).'</li>';
                    $content .= '<li><strong>'.__('End Time','nes').': </strong> '.date('g:i a', strtotime($end_time)).'</li>';
                    if($end_cleanup_time){
                        $content .= '<li><strong>'.__('End Cleanup Time','nes').': </strong> '.date('g:i a', strtotime($end_cleanup_time)).'</li></ul>';
                    }
                    if($comments){    
                        $content .= '<p><strong>'.__('Comments','nes').':</strong></p>';
                        $content .= '<ul><li>'. $comments .'</li></ul>';
                    }
                    $content .= '<p>'.nl2br(get_option('nes_notification_footer')).'</p>';
                    $this->nes_send_notification($email, $subject, $content);
                }
            }
        }

        function nes_maybe_save_uncategorized($post_id){

            // save "uncategorized" if no categories where chosen
            if(!wp_get_object_terms($post_id, 'event_category')){

                $exists = term_exists('Uncategorized', 'event_category');
                if($exists !== 0 && $exists !== null){
                    $term = wp_insert_term('Uncategorized', 'event_category', array('slug' => 'uncategorized'));
                }

                wp_set_object_terms($post_id, 'uncategorized', 'event_category');
            }
        }

        // remove event from others in this recurring series
        function nes_trash_event_meta($post_id){

            // get all events in this series
            if($series_ids = get_post_meta($post_id,'nes_recurring_series',true)){
                foreach($series_ids as $series_id => $event_date){
                    // remove this event id from the meta of each event in the series
                    if($series = get_post_meta($series_id,'nes_recurring_series',true)){
                        if(array_key_exists($post_id, $series)){
                            unset($series[$post_id]);
                            asort($series);
                            update_post_meta($series_id,'nes_recurring_series',$series);
                        }
                    }
                }
                // fix its own series meta
                update_post_meta($post_id,'nes_recurring_series','');
                update_post_meta($post_id,'nes_frequency_type','one');
                update_post_meta($post_id,'nes_frequency_weekly','');
                update_post_meta($post_id,'nes_frequency_monthly','');                
                update_post_meta($post_id,'nes_series_start_date','');
                update_post_meta($post_id,'nes_frequency_end_date','');
            }

        }

        // get available dates for recurring event save
        function nes_get_available_dates($post_id, $venue_id, $location_ids, $start_date, $event_type, $frequency_type, $frequency_end_date, $frequency_weekly, $frequency_monthly, $custom_dates = null, $conflict_checking = true){

            // first check if the times submitted are possible
            $start_time = get_post_meta($post_id,'nes_start_time',true);
            $start_setup_time = get_post_meta($post_id,'nes_start_setup_time',true);
            $end_time = get_post_meta($post_id,'nes_end_time',true);
            $end_cleanup_time = get_post_meta($post_id,'nes_end_cleanup_time',true);

            // set start and end time based on setup and/or cleanup being submitted
            if(!empty($start_setup_time) && $start_setup_time < $start_time){
                $event_start = $start_setup_time;
            }else{
                $event_start = $start_time;
            }            
            if(!empty($end_cleanup_time) && $end_cleanup_time > $end_time){
                $event_end = $end_cleanup_time;
            }else{
                $event_end = $end_time;
            }

            // initialize dates array to return
            $available_dates = array();

            // check if start time is before end time and if date is in the future
            if(($event_start < $event_end) && (strtotime($start_date) >= strtotime(date('Y-m-d')))){                  

                // start with empty series dates
                $date_arr = array();

                // check if custom dates
                if($frequency_type == 'custom'){
                    if($custom_dates_arr = explode(', ', $custom_dates)){
                        foreach($custom_dates_arr as $custom_date){
                            $date_arr[] = date('Y-m-d', strtotime($custom_date));
                        }
                    }

                }else{

                    // set weekly to event date day if none submitted
                    if(empty($frequency_weekly)){
                        $frequency_weekly = array(strtolower(date('l', strtotime($start_date))));
                    }  

                    // get all possible dates matching criteria submitted
                    $next_date = $start_date;
                    if($frequency_type == 'daily'){
                        while(strtotime($next_date) <= strtotime($frequency_end_date)){
                            $next_date = date('Y-m-d',strtotime($next_date . " +1 days"));

                            // check if date is before end
                            if(strtotime($next_date) <= strtotime($frequency_end_date)){
                                $date_arr[] = $next_date;
                            }
                        }
                    }elseif($frequency_type == 'weekly'){                        
                        while(strtotime($next_date) <= strtotime($frequency_end_date)){
                            $next_date = date('Y-m-d',strtotime($next_date . " +1 days"));

                            // check if date is before end
                            if(strtotime($next_date) <= strtotime($frequency_end_date)){

                                // check if date is in the weekly frequency
                                if(is_array($frequency_weekly) && in_array(strtolower(date('l', strtotime($next_date))), $frequency_weekly)){
                                   $date_arr[] = $next_date;  
                                } 
                            }
                        }
                    }elseif($frequency_type == 'monthly'){
                        if(!empty($frequency_monthly) && !empty($frequency_weekly)){
                            while(strtotime($next_date) <= strtotime($frequency_end_date)){
                                $next_date = date('Y-m-d',strtotime($next_date . " +1 days"));
                                
                                // check if date is before end
                                if(strtotime($next_date) <= strtotime($frequency_end_date)){
                                        
                                    // check if date is in the weekly frequency
                                    if(is_array($frequency_weekly) && in_array(strtolower(date('l', strtotime($next_date))), $frequency_weekly)){
                                    
                                        // loop through the selected week days
                                        foreach($frequency_weekly as $day){

                                            // check if date matches monthly criteria
                                            if(strtotime($next_date) == strtotime($frequency_monthly . ' ' . $day . ' of ' . date('F Y', strtotime($next_date)))){
                                                $date_arr[] = $next_date;
                                            }
                                        }
                                    } 
                                }
                            }
                        }else{
                            while(strtotime($next_date) <= strtotime($frequency_end_date)){

                                // no monthly frequency set so just go to the next month
                                $next_date = date('Y-m-d',strtotime($next_date . " +1 months"));

                                // check if date is before end
                                if(strtotime($next_date) <= strtotime($frequency_end_date)){
                                    $date_arr[] = $next_date;
                                }
                            }
                        }
                    }
                }

                // start with empty conflict array
                foreach($date_arr as $date){

                    // fix date format
                    $date = date('Y-m-d',strtotime($date));

                    // if offsite or "non conflict" just add all the dates
                    if($event_type == 'offsite' || !$conflict_checking){
                        $available_dates[] = $date;
                    }else{

                        // check if location is unavailable for time selected
                        $conflict = false;
                        if($location_ids){
                            foreach($location_ids as $location_id){                

                                // get availability
                                $location_availability = get_post_meta($location_id, 'nes_venue_availability', true);
                                if($location_availability == 'custom'){
                                    $location_start = get_post_meta($location_id, 'nes_location_'.strtolower(date('l', strtotime($date))).'_start', true);
                                    $location_end = get_post_meta($location_id, 'nes_location_'.strtolower(date('l', strtotime($date))).'_end', true);
                                }
                                else{
                                    $venue_id = get_post_meta($location_id, 'nes_venue_id', true);
                                    $location_start = get_post_meta($venue_id, 'nes_venue_'.strtolower(date('l', strtotime($date))).'_start', true);
                                    $location_end = get_post_meta($venue_id, 'nes_venue_'.strtolower(date('l', strtotime($date))).'_end', true);
                                }

                                // first check availability conflict
                                if($location_start && $location_end && ($event_start >= $location_start) && ($event_end <= $location_end)){
                                    
                                    global $wpdb;

                                    // then check for existing event conflicts
                                    if($series_ids = get_post_meta($post_id,'nes_recurring_series',true)){

                                        $series_id_str = implode("','", $series_ids);
                                        $query_string = "
                                            SELECT DISTINCT p.ID
                                            FROM $wpdb->posts p
                                            LEFT JOIN $wpdb->postmeta m1 ON (p.ID = m1.post_id AND m1.meta_key = 'nes_event_date')
                                            LEFT JOIN $wpdb->postmeta m2 ON (p.ID = m2.post_id AND m2.meta_key = 'nes_start_setup_time')
                                            LEFT JOIN $wpdb->postmeta m3 ON (p.ID = m3.post_id AND m3.meta_key = 'nes_end_cleanup_time')
                                            WHERE p.post_type = 'nes_event'
                                            AND p.ID NOT IN ('$series_id_str')
                                            AND p.post_status IN ('publish','past_events')
                                            AND m1.meta_value = '$date'
                                            AND m2.meta_value < '$event_end'
                                            AND m3.meta_value > '$event_start'
                                        ";
                                        
                                    }else{

                                        $query_string = "
                                            SELECT DISTINCT p.ID
                                            FROM $wpdb->posts p
                                            LEFT JOIN $wpdb->postmeta m1 ON (p.ID = m1.post_id AND m1.meta_key = 'nes_event_date')
                                            LEFT JOIN $wpdb->postmeta m2 ON (p.ID = m2.post_id AND m2.meta_key = 'nes_start_setup_time')
                                            LEFT JOIN $wpdb->postmeta m3 ON (p.ID = m3.post_id AND m3.meta_key = 'nes_end_cleanup_time')
                                            WHERE p.post_type = 'nes_event'
                                            AND p.post_status IN ('publish','past_events')
                                            AND m1.meta_value = '$date'
                                            AND m2.meta_value < '$event_end'
                                            AND m3.meta_value > '$event_start'
                                        ";
                                    }

                                    $results = $wpdb->get_results($query_string, OBJECT);
                                    if($results){
                                        foreach($results as $result){
                                            $locations = get_post_meta($result->ID,'nes_location_id',true);
                                            if($locations){
                                                if(is_array($locations) && in_array($location_id, $locations)){
                                                    $conflict = true;
                                                }
                                            }
                                        }
                                    }

                                }else{
                                    // location time conflict
                                    $conflict = true;
                                }
                            }
                        }

                        // add date if no conflicting events
                        if(!$conflict){
                            $available_dates[] = $date;
                        }
                    }
                }
            }

            return $available_dates;
        }



        function nes_create_new_event_series($post_id, $post, $available_dates, $series_start_date){

            // make sure there is more than just one available date before making a series
            if(count($available_dates) >= 1){      

                // create the new event posts in this series
                $series_ids = array($post_id => $series_start_date);

                foreach($available_dates as $date){

                    // defer term and comment counting to improve insert performance
                    wp_defer_term_counting(true);
                    wp_defer_comment_counting(true);

                    // insert new event posts
                    $event_post = array(
                        'post_title' => $post->post_title,
                        'post_content' => $post->post_content,
                        'post_status' => 'publish',
                        'post_type' => 'nes_event'
                    );

                    // Insert the post into the database
                    $series_event_id = wp_insert_post($event_post);

                    // copy all meta values
                    $post_meta_keys = get_post_custom_keys($post_id);

                    if($post_meta_keys){
                        foreach($post_meta_keys as $meta_key){
                            $meta_value = get_post_meta($post_id, $meta_key, true);
                            update_post_meta($series_event_id, $meta_key, $meta_value);
                        }
                    }

                    // update event date to be different
                    update_post_meta($series_event_id,'nes_event_date',$date);

                    // copy all taxonomies
                    $post_taxonomies = get_object_taxonomies('nes_event');
                    if($post_taxonomies){
                        foreach($post_taxonomies as $taxonomy){
                            $post_terms = wp_get_object_terms($post_id, $taxonomy, array('orderby' => 'term_order'));
                            $terms = array();
                            for ($i=0; $i<count($post_terms); $i++) {
                                $terms[] = $post_terms[$i]->slug;
                            }
                            wp_set_object_terms($series_event_id, $terms, $taxonomy);
                        }
                    }

                    // add this event id to go back in and update meta when all the new events have been inserted
                    $series_ids[$series_event_id] = $date;
                }

                // sort the series and update this post
                asort($series_ids);
                update_post_meta($post_id,'nes_recurring_series',$series_ids);
                update_post_meta($post_id,'nes_event_date',$series_start_date);

                // update all the new events with the series
                if($series_ids){
                    foreach($series_ids as $series_id => $date){
                        update_post_meta($series_id,'nes_recurring_series',$series_ids);
                    }
                }
            }
        }

        // ajax call to check for venue/location/timeslot availability
        function nes_ajax_check_availability(){

            $message = '<div class="nes-availability-result">';

            // get venue id
            $venue_id = sanitize_text_field($_POST['venue']);

            // get location ids
            $location_ids = $_POST['locations'];

            // get date
            $date = sanitize_text_field($_POST['date']);

            // save for error checks on the backend
            $original_date = $date;

            // get custom dates (if any)
            $custom_dates = sanitize_text_field($_POST['custom_dates']);

            // remove event date from custom dates
            if($custom_dates){
                $custom_dates_arr = explode(', ', $custom_dates);

                if(is_array($custom_dates_arr)){
                    // remove the start date if included
                    foreach($custom_dates_arr as $k => $custom_date){
                        if($custom_date == date('m/d/Y', strtotime($date))){
                            unset($custom_dates_arr[$k]);
                            $custom_dates = implode(', ', $custom_dates_arr); 
                            break;
                        }
                    }
                }
            }  

            // get start and end times
            if(isset($_POST['setup_time']) && (isset($_POST['need_setup_time']) &&  $_POST['need_setup_time'] == 'yes')){
                $start_setup_time = sanitize_text_field($_POST['setup_time']);
            }else{
                $start_setup_time = false;
            }
            if(isset($_POST['cleanup_time']) && (isset($_POST['need_cleanup_time']) && $_POST['need_cleanup_time'] == 'yes')){
                $end_cleanup_time = sanitize_text_field($_POST['cleanup_time']);
            }else{
                $end_cleanup_time = false;
            }
            $start_time = sanitize_text_field($_POST['start_time']);
            $end_time = sanitize_text_field($_POST['end_time']);

            // set start and end time based on setup and/or cleanup being submitted
            if($start_setup_time){
                $event_start = $start_setup_time;
            }else{
                $event_start = $start_time;
            }            
            if($end_cleanup_time){
                $event_end = $end_cleanup_time;
            }else{
                $event_end = $end_time;
            }

            // maybe already a single event


            // maybe get existing series ids
            $series_ids = array();
            $post_id = false;
            if(isset($_POST['post_id'])){
                $post_id = sanitize_text_field($_POST['post_id']);
                $series_ids = get_post_meta($post_id,'nes_recurring_series',true); // array([post->ID] => event_date) 
                if(!empty($series_ids)){
                    $message .= '<p><em>NOTE: this check is excluding '. $this->nes_settings['event_plural'] . ' in this series (since they can\'t conflict with themselves)</em></p><hr/>';
                }else{
                    $message .= '<p><em>NOTE: this check exludes this '. $this->nes_settings['event_single'] . ' (since it can\'t conflict with itself)</em></p><hr/>';
                }
            }

            // check for recurring dates
            $frequency_end_date = sanitize_text_field($_POST['frequency_end_date']);
            $frequency_type = sanitize_text_field($_POST['frequency_type']);
            $frequency_weekly = '';

            // set weekly to event date day if none submitted
            if(empty($_POST['frequency_weekly'])){
                $frequency_weekly = array(strtolower(date('l', strtotime($date))));
            }else{
                $frequency_weekly = $_POST['frequency_weekly'];
            }

            $frequency_monthly = sanitize_text_field($_POST['frequency_monthly']);

            // check if custom dates
            if($frequency_type == 'custom'){
                $date_arr = array($date);
                if(!empty($custom_dates)){
                    $custom_dates_arr = explode(', ', $custom_dates);
                    if($custom_dates_arr){
                        foreach($custom_dates_arr as $custom_date){
                            $date_arr[] = date('Y-m-d', strtotime($custom_date));
                        }
                    }
                }
            }else{
                $date_arr = array($date);
                $next_date = $date;
                if($frequency_type == 'daily'){
                    while(strtotime($next_date) <= strtotime($frequency_end_date)){
                        $next_date = date('Y-m-d',strtotime($next_date . " +1 days"));

                        // check if date is before end
                        if(strtotime($next_date) <= strtotime($frequency_end_date)){
                            $date_arr[] = $next_date;
                        }
                    }
                }elseif($frequency_type == 'weekly'){                        
                    while(strtotime($next_date) <= strtotime($frequency_end_date)){
                        $next_date = date('Y-m-d',strtotime($next_date . " +1 days"));

                        // check if date is before end
                        if(strtotime($next_date) <= strtotime($frequency_end_date)){

                            // check if date is in the weekly frequency
                            if(is_array($frequency_weekly) && in_array(strtolower(date('l', strtotime($next_date))), $frequency_weekly)){
                               $date_arr[] = $next_date;  
                            } 
                        }
                    }
                }elseif($frequency_type == 'monthly'){
                    if(!empty($frequency_monthly) && !empty($frequency_weekly)){
                        while(strtotime($next_date) <= strtotime($frequency_end_date)){
                            $next_date = date('Y-m-d',strtotime($next_date . " +1 days"));
                            
                            // check if date is before end
                            if(strtotime($next_date) <= strtotime($frequency_end_date)){
                                    
                                // check if date is in the weekly frequency
                                if(is_array($frequency_weekly) && in_array(strtolower(date('l', strtotime($next_date))), $frequency_weekly)){
                                
                                    // loop through the selected week days
                                    foreach($frequency_weekly as $day){

                                        // check if date matches monthly criteria
                                        if(strtotime($next_date) == strtotime($frequency_monthly . ' ' . $day . ' of ' . date('F Y', strtotime($next_date)))){
                                            $date_arr[] = $next_date;
                                        }
                                    }
                                } 
                            }
                        }
                    }else{
                        while(strtotime($next_date) <= strtotime($frequency_end_date)){

                            // no monthly frequency set so just go to the next month
                            $next_date = date('Y-m-d',strtotime($next_date . " +1 months"));

                            // check if date is before end
                            if(strtotime($next_date) <= strtotime($frequency_end_date)){
                                $date_arr[] = $next_date;
                            }
                        }
                    }
                }
            }
            

            // check if start time is before end time
            if($event_start >= $event_end){echo '<div class="nes-availability-result"><strong>ERROR: Start time submitted is equal to or after End time</strong></div>'; die();}
            if(date('Y-m-d',strtotime($date)) < date('Y-m-d',strtotime('now'))){echo '<div class="nes-availability-result"><strong>ERROR: Start date is in the past</strong></div>'; die();}

            // start with empty conflict array
            $conflicts = array();
            $successes = array();

            foreach($date_arr as $date){

                // fix date format
                $date = date('Y-m-d',strtotime($date));

                // check if location is unavailable for time selected
                if($location_ids){
                    foreach($location_ids as $location_id){                

                        // conflict flag for this location
                        $conflict = false;

                        $location_id = sanitize_text_field($location_id);

                        // get availability
                        $location_availability = get_post_meta($location_id, 'nes_venue_availability', true);
                        if($location_availability == 'custom'){
                            $location_start = get_post_meta($location_id, 'nes_location_'.strtolower(date('l', strtotime($date))).'_start', true);
                            $location_end = get_post_meta($location_id, 'nes_location_'.strtolower(date('l', strtotime($date))).'_end', true);
                        }
                        else{
                            $venue_id = get_post_meta($location_id, 'nes_venue_id', true);
                            $location_start = get_post_meta($venue_id, 'nes_venue_'.strtolower(date('l', strtotime($date))).'_start', true);
                            $location_end = get_post_meta($venue_id, 'nes_venue_'.strtolower(date('l', strtotime($date))).'_end', true);
                        }

                        // first check availability conflict
                        if(!$location_start && !$location_end){
                            $conflicts[] = array('date' => $date, 'reason' => sprintf(__('(%1$s) %2$s are not available %3$s all day %4$s on %5$s','nes'),get_the_title($location_id),$this->nes_settings['event_plural'],'<strong>','</strong>',date('l', strtotime($date))));
                            $conflict = true;
                        }else if($event_start < $location_start){
                            $conflicts[] = array('date' => $date, 'reason' => sprintf(__('(%1$s) %2$s are not available %3$s before %4$s %5$s on %6$s','nes'),get_the_title($location_id),$this->nes_settings['event_plural'],'<strong>','</strong>',date('g:i a', strtotime($location_start)),date('l', strtotime($date))));
                            $conflict = true;
                        }else if($event_end > $location_end){
                            $conflicts[] = array('date' => $date, 'reason' => sprintf(__('(%1$s) %2$s are not available %3$s after %4$s %5$s on %6$s','nes'),get_the_title($location_id),$this->nes_settings['event_plural'],'<strong>','</strong>',date('g:i a', strtotime($location_end)),date('l', strtotime($date))));
                            $conflict = true;
                        }else{
                            // then check for existing event conflicts
                            global $wpdb;

                            $series_id_str = '';
                            if(!empty($series_ids)){
                                $series_id_str = implode("','", array_keys($series_ids));
                            }elseif($post_id){
                                $series_id_str = $post_id;
                            }
                            
                            $query_string = "
                                SELECT DISTINCT p.ID
                                FROM $wpdb->posts p
                                LEFT JOIN $wpdb->postmeta m1 ON (p.ID = m1.post_id AND m1.meta_key = 'nes_event_date')
                                LEFT JOIN $wpdb->postmeta m2 ON (p.ID = m2.post_id AND m2.meta_key = 'nes_start_setup_time')
                                LEFT JOIN $wpdb->postmeta m3 ON (p.ID = m3.post_id AND m3.meta_key = 'nes_end_cleanup_time')
                                LEFT JOIN $wpdb->postmeta m4 ON (p.ID = m4.post_id AND m4.meta_key = 'nes_event_status')
                                WHERE p.post_type = 'nes_event'
                                AND p.ID NOT IN ('$series_id_str')
                                AND p.post_status IN ('publish','past_events')
                                AND m1.meta_value = '$date'
                                AND m2.meta_value < '$event_end'
                                AND m3.meta_value > '$event_start'
                                AND m4.meta_value IN ('pending','approved')
                            ";
                            $results = $wpdb->get_results($query_string, OBJECT);

                            if($results){
                                foreach($results as $result){
                                    $locations = get_post_meta($result->ID,'nes_location_id',true);
                                    if($locations){
                                        if(is_array($locations) && in_array($location_id, $locations)){
                                            $conflicts[] = array('date' => $date, 'reason' => sprintf(__('Existing %1$s - (%2$s)','nes'),$this->nes_settings['event_single'],get_the_title($location_id)));
                                            $conflict = true;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if(!$conflict){
                        $successes[] = $date;
                    }
                }else{
                    $conflict = true;
                    $message .= '<p><strong>ERROR: No '.$this->nes_settings['location_plural'].' found. Please select at least 1 '.$this->nes_settings['location_single'].'</strong></p>';
                }

            }

            // notify about successes
            if(!empty($successes)){
                $message .= '<p><strong>SUCCESS: These dates are available:</strong></p>';
                $message .= '<ul>';
                foreach($successes as $success){
                    $message .= '<li>' . date('F jS, Y',strtotime($success)) . ' @ '. date('g:ia',strtotime($event_start)) . ' - ' . date('g:ia',strtotime($event_end)) .'</li>';
                }
                $message .= '</ul>';
            }

            // notify about conflicts
            if(!empty($conflicts)){
                $message .= '<p><strong>ERROR: These dates have conflicts</strong></p>';
                $message .= '<ul>';
                foreach($conflicts as $conflict_arr){
                    $maybe_stop = '';
                    if(date('Y-m-d',strtotime($conflict_arr['date'])) == date('Y-m-d',strtotime($original_date))){
                        $maybe_stop = ' class="nes-stop-new"';
                    }
                    $message .= '<li'.$maybe_stop.'>' . date('F jS, Y',strtotime($conflict_arr['date'])) . ' - ' . $conflict_arr['reason'] .' </li>';
                }
                $message .= '</ul>';
            }

            $message .= '</div>';
            echo $message;
            die();
        }    

        // add single event content page template
        function nes_add_content_template(){
            if(is_singular('nes_event')){
                include_once('views/frontend/nes-single-event-content.php');
            }
        }

        // [nessie] shortcode
        function nes_shortcode_view($atts){

            // check if view has been manually submitted
            $current_view = false;
            if(isset($_GET['view'])){
                // get posted view if any
                switch($_GET['view']){
                    case 'list' : $current_view = 'nes-list-view'; break;
                    case 'reservations' : $current_view = 'nes-reservation'; break;
                    case 'month' : $current_view = 'nes-month-view'; break;
                    default : break;
                }
            }else{
                if(isset($atts['view'])){
                    // get default view if any
                    switch($atts['view']){
                        case 'list' : $current_view = 'nes-list-view'; break;
                        case 'reservations' : $current_view = 'nes-reservation'; break;
                        case 'month' : $current_view = 'nes-month-view'; break;
                        default : break;
                    }
                }else{
                    $atts['view'] = 'month';
                    $current_view = 'nes-month-view';
                }
            }

            // check for options expanded or not
            if(isset($atts['options']) && $atts['options'] == 'open'){
                $expanded_options = true;
            }else{
                $expanded_options = false;
            }

            // check for category ids
            $cat_id_arr = array();
            if(isset($atts['categories']) && !empty($atts['categories'])){
                $cat_id_arr = explode(',', str_replace(' ', '', $atts['categories']));
            }            

            // check for venue ids
            $venue_id_arr = array();
            if(isset($atts['venues']) && !empty($atts['venues'])){
                $venue_id_arr = explode(',', str_replace(' ', '', $atts['venues']));
            }

            global $post;
            $can_submit = false;
            if($this->nes_settings['require_login'] == "yes"){
                if(is_user_logged_in()){
                    $can_submit = true;
                }else{
                    $login_message = sprintf(__('You must be logged in to make a %1$s','nes'),$this->nes_settings['event_single']).'. ';
                    $login_message .= sprintf(__('Please %1$s login here %2$s','nes'),'<a href="/wp-login.php?redirect_to='. get_permalink($post->ID).'">','</a>.');
                    return $login_message;
                }
            }else{
                $can_submit = true;
            }

            if($can_submit){
                ob_start();
                ?>
                <div id="nes" class="nes-content-wrap">
                    
                <?php // check if reservation view or not ?>
                <?php if($current_view == 'nes-reservation') : ?>

                    <?php if(!empty($_SESSION['nes']['message'])) : ?>
                        <div class="nes-event-success-message">
                            <?php echo $_SESSION['nes']['message']; ?>
                        </div>
                    <?php endif; ?>

                    <h3 class="nes-view-date">
                        <span class="nes-month-year"><?php echo date_i18n('l - F jS, Y', strtotime('now')); ?></span>
                        <button class="nes-prev-month" data-date="<?php echo date('Y-m-d', strtotime('now -1 day')); ?>"><i class="far fa-angle-double-left"></i> <span><?php echo date_i18n('F jS, Y', strtotime('now -1 day')); ?></span></button>
                        <button class="nes-next-month" data-date="<?php echo date('Y-m-d', strtotime('now +1 day')); ?>"><span><?php echo date_i18n('F jS, Y', strtotime('now +1 day')); ?></span> <i class="far fa-angle-double-right"></i></button>
                        <span class="nes-clearer"></span>
                    </h3>

                    <div class="nes-filter-wrap">
                            <h4 class="nes-filter-toggle">DISPLAY OPTIONS<i class="far fa-angle-down"></i><span class="nes-clearer"></span></h4>
                            <form id="nes-day-form" method="post" action="" <?php if($current_view != 'nes-reservation'){echo 'style="display:none;"';} ?>>

                            <div class="nes-datepicker">
                                <h6 class="nes-filter-title"><?php _e('Date','nes'); ?>:</h6>
                                <input id="nes-datepicker" type="text" value=""/>
                                <input id="nes-date" type="hidden" name="nes_date" value=""/>
                            </div>

                            <?php // check for offsite only  ?>
                            <?php if(in_array('offsite', $venue_id_arr)) : ?>
                                <div class="nes-venue-filter" style="display:none;">
                                    <select name="nes_venue_ids[]">
                                        <option value="offsite">Offsite</option>
                                    </select>
                                </div>
                            <?php else : ?>
                                <div class="nes-venue-filter">
                                    <h6 class="nes-filter-title"><?php echo $this->nes_settings['venue_single']; ?>:</h6>
                           
                                    <?php // get all venues to list ?>
                                    <?php $venues = $this->nes_get_venues(); ?>

                                    <select name="nes_venue_ids[]">
                                    <?php if($venues): ?>
                                        <?php foreach($venues as $venue) : ?>
                                            <option value="<?php echo $venue->ID; ?>"
                                                <?php 
                                                    if(get_option('nes_default_venue') == $venue->ID){
                                                        echo 'selected="selected"';
                                                    }
                                                ?>
                                            ><?php echo $venue->post_title; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif;?> 
                                        <option value="offsite">Offsite</option>
                                    </select>
                                </div>
                            <?php endif; ?>

                            <?php // submit an empty array of event cats because it doesnt apply to reservations ?>
                            <input type="hidden" name="nes_event_cats[]" value="" />

                            <div class="nes-views">
                                <h6 class="nes-filter-title"><?php _e('Go To','nes'); ?>:</h6>
                                <?php // event calendar page setting ?>
                                <?php if($events_page_id = $this->nes_settings['event_calendar_page']) : ?>
                                    <a class="nes-view-link nes-month-list" href="<?php echo get_permalink($events_page_id); ?>?view=month"><i class="fas fa-calendar"></i><span>Month View</span></a>
                                    <a class="nes-view-link nes-month-list" href="<?php echo get_permalink($events_page_id); ?>?view=list"><i class="fas fa-list-ul"></i><span>List View</span></a>
                                <?php endif; ?>
                                <input type="hidden" name="nes_active_view" value="<?php echo $current_view; ?>">
                                <span class="nes-clearer"></span>
                            </div>
                            <span class="nes-clearer"></span>
                        </form> 
                    </div>

                    <span class="clearer"></span>
                    <div id="nes-shortcode-view"></div>

                    <script type="text/javascript">

                        // define vars
                        var view,date,venues,categories;

                        jQuery(document).ready(function($){

                            // jquery datepicker
                            $('#nes-datepicker').datepicker({
                                dateFormat: "mm/dd/yy",
                                altField: "#nes-date",
                                altFormat: "yy-mm-dd",
                                inline: true,
                                showOtherMonths: true,
                                showOn: "both", 
                                buttonText: "<i class='far fa-calendar'></i>",
                                onSelect: function(selectedDate){
                                    $('#nes-date').trigger('change');
                                }
                            });                            

                            // set today for default if blank
                            if(!($('#nes-date').val().length > 0)){
                                $('#nes-datepicker').datepicker('setDate', new Date());
                            }

                            // initial pageload view
                            nes_change_view();     

                            // toggle display options
                            $('.nes-filter-toggle').on('click',function(){
                                $('i',this).toggleClass('fa-angle-up fa-angle-down');
                                $('#nes-day-form').slideToggle('fast');
                            });

                            $('input[name="nes_date"]').on('change', function(){
                                // get current view date and add/subtract month
                                var date = $(this).val();
                                var date_arr = date.split('-');

                                // update month/year title and prev/next buttons
                                var next_day = moment([date_arr[0], (date_arr[1] - 1), date_arr[2]]).add(1, 'days').format("YYYY-MM-DD");
                                $('.nes-next-month').attr('data-date',next_day);

                                var next_day_formatted = moment([date_arr[0], (date_arr[1] - 1), date_arr[2]]).add(1, 'days').format("MMMM Do, YYYY");
                                $('.nes-next-month span').text(next_day_formatted);

                                var prev_day = moment([date_arr[0], (date_arr[1] - 1), date_arr[2]]).subtract(1, 'days').format("YYYY-MM-DD");
                                $('.nes-prev-month').attr('data-date',prev_day);

                                var prev_day_formatted = moment([date_arr[0], (date_arr[1] - 1), date_arr[2]]).subtract(1, 'days').format("MMMM Do, YYYY");
                                $('.nes-prev-month span').text(prev_day_formatted);

                                // update the view title
                                $('.nes-month-year').text(moment([date_arr[0], (date_arr[1] - 1), date_arr[2]]).format("dddd - MMMM Do, YYYY"));
                                
                                // change the view
                                nes_change_view();
                            });    

                            $('select[name="nes_venue_ids[]"]').on('change',function(){
                                // change the view
                                nes_change_view();
                            }); 

                            // prev next buttons
                            $(document).on('click', '.nes-prev-month, .nes-next-month', function(e){

                                e.preventDefault();
       
                                // get current view date and add/subtract month
                                var date = $(this).attr('data-date');
                                var date_arr = date.split('-');

                                // update date
                                $('#nes-datepicker').datepicker('setDate', moment(date).format("MM/DD/YYYY"));
                                $('#nes-date').val(moment(date).format("YYYY-MM-DD"));

                                // update month/year title and prev/next buttons
                                var next_month = moment([date_arr[0], (date_arr[1] - 1), date_arr[2]]).add(1, 'days').format("YYYY-MM-DD");
                                $('.nes-next-month').attr('data-date',next_month);
                                
                                var next_month_formatted = moment([date_arr[0], (date_arr[1] - 1), date_arr[2]]).add(1, 'days').format("MMMM Do, YYYY");
                                $('.nes-next-month span').text(next_month_formatted);

                                var prev_month = moment([date_arr[0], (date_arr[1] - 1), date_arr[2]]).subtract(1, 'days').format("YYYY-MM-DD");
                                $('.nes-prev-month').attr('data-date',prev_month);

                                var prev_month_formatted = moment([date_arr[0], (date_arr[1] - 1), date_arr[2]]).subtract(1, 'days').format("MMMM Do, YYYY");
                                $('.nes-prev-month span').text(prev_month_formatted);

                                // update the view title
                                $('.nes-month-year').text(moment([date_arr[0], (date_arr[1] - 1), date_arr[2]]).format("dddd - MMMM Do, YYYY"));

                                // change the view
                                nes_change_view();
                            });   
                        });

                        function nes_change_view(){

                            // update vars
                            view = jQuery('input[name="nes_active_view"]').val();
                            date = jQuery('input[name="nes_date"]').val();
                            venues = jQuery(".nes-venue-filter select").map(function(){
                              return jQuery(this).val();
                            }).get();                    
                            categories = jQuery(".nes-category-filter input").map(function(){
                              return jQuery(this).val();
                            }).get();

                            jQuery('#nes-shortcode-view').html('<div id="nes-loading"><p>Loading <i class="fas fa-spinner fa-pulse"></i></p></div>');
                            var data = {
                                'action': 'nes_ajax_view_change',
                                'date': date,
                                'view': view,
                                'venues': venues,
                                'categories': categories
                            };
                            jQuery.post("<?php echo NES_AJAX_URL; ?>", data, function(response){
                                jQuery('#nes-shortcode-view').html(response);
                            }); 
                        }   
                        
                    </script>

                <?php else : ?>
                    <?php // view is either month or list ?>
                    <?php 
                        // check if date was submitted
                        $the_month = date('m');
                        $the_year = date('Y');
                        if(isset($_GET['mo'])){$the_month = htmlentities($_GET['mo']);}
                        if(isset($_GET['yr'])){$the_year = htmlentities($_GET['yr']);}
                        $the_date = $the_year . '-' . $the_month;
                    ?>

                    <h3 class="nes-view-date">
                        <span class="nes-month-year"><?php echo date_i18n('F Y', strtotime($the_date)); ?></span>
                        <button class="nes-prev-month" data-date="<?php echo date('Y-m', strtotime($the_date . ' -1 month')); ?>"><i class="far fa-angle-double-left"></i> <span><?php echo date_i18n('F Y', strtotime($the_date . ' -1 month')); ?></span></button>
                        <button class="nes-next-month" data-date="<?php echo date('Y-m', strtotime($the_date . ' +1 month')); ?>"><span><?php echo date_i18n('F Y', strtotime($the_date . ' +1 month')); ?></span> <i class="far fa-angle-double-right"></i></button>
                        <span class="nes-clearer"></span>
                    </h3>

                    <div class="nes-filter-wrap" <?php if(isset($atts['options']) && $atts['options'] == 'hide'){echo' style="display:none;"';}?>>
                        <?php $visible_filter_venues = apply_filters('nes_visible_filter_venues', 2); ?>
                        <?php if($expanded_options) : ?>
                            <h4 class="nes-filter-toggle">DISPLAY OPTIONS<i class="far fa-angle-up"></i><span class="nes-clearer"></span></h4>
                            <form id="nes-day-form" method="post" action="">
                        <?php else : ?>
                            <h4 class="nes-filter-toggle">DISPLAY OPTIONS<i class="far fa-angle-down"></i><span class="nes-clearer"></span></h4>
                            <form id="nes-day-form" method="post" action="" <?php if(!isset($_GET['view'])){echo 'style="display:none;"';} ?>>
                        <?php endif; ?>  
                                <div class="nes-venue-filter">
                                <?php $venues = $this->nes_get_venues(); ?>
                                <?php $venue_count = count($venues) + 1 // add 1 for offsite; ?>
                                <h6 class="nes-filter-title"><?php echo $this->nes_settings['venue_single']; ?>:</h6>
                                <?php $count = 0; ?>
                                <?php if($venues) : ?>
                                    <?php foreach($venues as $venue) : ?>
                                            <label>
                                                <?php if(!empty($venue_id_arr)) : ?>
                                                    <?php if(in_array($venue->ID, $venue_id_arr)) : ?>
                                                        <span class="nes-venue-color checked" style="background:#<?php echo get_post_meta($venue->ID,'nes_venue_color',true); ?>;color:#<?php echo get_post_meta($venue->ID,'nes_venue_text_color',true); ?>;"><i class="fas fa-check"></i></span>
                                                        <input type="checkbox" name="nes_venue_ids[]" value="<?php echo $venue->ID; ?>" style="display:none;" checked="checked" /> 
                                                    <?php else: ?>
                                                        <span class="nes-venue-color" style="background:#<?php echo get_post_meta($venue->ID,'nes_venue_color',true); ?>;color:#<?php echo get_post_meta($venue->ID,'nes_venue_text_color',true); ?>;">&nbsp;</span>
                                                        <input type="checkbox" name="nes_venue_ids[]" value="<?php echo $venue->ID; ?>" style="display:none;" /> 
                                                    <?php endif; ?>
                                                <?php else : ?>
                                                    <span class="nes-venue-color checked" style="background:#<?php echo get_post_meta($venue->ID,'nes_venue_color',true); ?>;color:#<?php echo get_post_meta($venue->ID,'nes_venue_text_color',true); ?>;"><i class="fas fa-check"></i></span>
                                                    <input type="checkbox" name="nes_venue_ids[]" value="<?php echo $venue->ID; ?>" style="display:none;" checked="checked" /> 
                                                <?php endif; ?>
                                                <?php echo $venue->post_title; ?>
                                            </label>
                                        <?php $count++; ?>                                        
                                        <?php if($count == $visible_filter_venues) : ?>
                                            <div class="nes-more-wrap" style="display:none;">
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                <?php endif;?>
                                    <label>
                                        <span class="nes-venue-color checked" style="color:#<?php echo $this->nes_settings['offsite_venue_text']; ?>;background:#<?php echo $this->nes_settings['offsite_venue_bg']; ?>;"><i class="fas fa-check"></i></span>
                                        <input type="checkbox" name="nes_venue_ids[]" style="display:none;" value="offsite" checked="checked" /> 
                                        Offsite
                                    </label>                                        
                                <?php if($count >= $visible_filter_venues) : ?>
                                    </div>
                                    <div class="nes-more-trigger">view <?php echo $venue_count - $visible_filter_venues; ?> <span>more</span> <i class="far fa-angle-double-down"></i></div>
                                <?php endif; ?>
                            </div>

                            <div class="nes-category-filter">
                                <?php $visible_filter_categories = apply_filters('nes_visible_filter_categories', 2); ?>
                                <h6 class="nes-filter-title">Categories:</h6>
                                <?php $event_cats = get_terms('event_category', array('hide_empty' => false)); ?>
                                <?php $cat_count = count($event_cats); ?>
                                <?php if($event_cats): ?>
                                    <?php $count = 0; ?>
                                    <?php foreach($event_cats as $cat) : ?>
                                            <?php if($count == $visible_filter_categories) : ?>
                                                <div class="nes-more-wrap" style="display:none;">
                                            <?php endif; ?>
                                                    <label for="cat-<?php echo $cat->term_id; ?>">
                                                        <?php if(!empty($cat_id_arr)) : ?>
                                                            <?php if(in_array($cat->term_id, $cat_id_arr)) : ?>
                                                                <input id="cat-<?php echo $cat->term_id; ?>" type="checkbox" name="nes_event_cats[]" value="<?php echo $cat->term_id; ?>" checked="checked"/> <?php echo $cat->name; ?>
                                                            <?php else : ?>
                                                                <input id="cat-<?php echo $cat->term_id; ?>" type="checkbox" name="nes_event_cats[]" value="<?php echo $cat->term_id; ?>" /> <?php echo $cat->name; ?>
                                                            <?php endif; ?>
                                                        <?php else : ?>
                                                            <input id="cat-<?php echo $cat->term_id; ?>" type="checkbox" name="nes_event_cats[]" value="<?php echo $cat->term_id; ?>" checked="checked"/> <?php echo $cat->name; ?>
                                                        <?php endif; ?>
                                                    </label>
                                        <?php $count++; ?>
                                    <?php endforeach; ?>
                                            <?php if($count > $visible_filter_categories) : ?>
                                                </div>
                                                <div class="nes-more-trigger">view <?php echo $cat_count - $visible_filter_categories; ?> <span>more</span> <i class="far fa-angle-double-down"></i></div>
                                            <?php endif; ?>
                                <?php endif;?>
                            </div>

                            <div class="nes-datepicker">
                                <h6 class="nes-filter-title"><?php _e('Date','nes'); ?>:</h6>
                                <select name="nes_month">
                                <?php $months = array('01' => 'January', '02' => 'February', '03' => 'March', '04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December'); ?>
                                <?php foreach($months as $k => $month) : ?>
                                    <option value="<?php echo $k; ?>" <?php if($k == date('m', strtotime($the_date))){echo 'selected="selected"';} ?>><?php echo $month; ?></option>
                                <?php endforeach; ?>
                                </select>

                                <select name="nes_year">
                                <?php for($year = (date('Y', strtotime($the_date . ' -10 years'))); $year <= (date('Y', strtotime($the_date . ' +10 years'))); $year++) : ?>
                                    <option value="<?php echo $year; ?>" <?php if($year == date('Y', strtotime($the_date))){echo 'selected="selected"';} ?>><?php echo $year; ?></option>
                                <?php endfor; ?>
                                </select>
                            </div>

                            <div class="nes-views">
                                <h6 class="nes-filter-title"><?php _e('Go To','nes'); ?>:</h6>

                                <?php // reservation page setting ?>
                                <?php if($event_calendar_page_id = $this->nes_settings['event_calendar_page']) : ?>

                                    <?php // only show view for page user is not on ?>
                                    <?php if(isset($_GET['view'])) : ?>
                                        <?php if($_GET['view'] == 'list') : ?>
                                            <a class="nes-view-link nes-month-list" href="<?php echo get_permalink($event_calendar_page_id); ?>?view=month"><i class="fas fa-calendar"></i><span>Month View</span></a>
                                        <?php elseif($_GET['view'] == 'month') : ?>
                                            <a class="nes-view-link nes-month-list" href="<?php echo get_permalink($event_calendar_page_id); ?>?view=list"><i class="fas fa-list-ul"></i><span>List View</span></a>
                                        <?php endif; ?>
                                    <?php elseif(isset($atts['view'])) : ?>
                                        <?php if($atts['view'] == 'list') : ?>
                                            <a class="nes-view-link nes-month-list" href="<?php echo get_permalink($event_calendar_page_id); ?>?view=month"><i class="fas fa-calendar"></i><span>Month View</span></a>
                                        <?php elseif($atts['view'] == 'month') : ?>
                                            <a class="nes-view-link nes-month-list" href="<?php echo get_permalink($event_calendar_page_id); ?>?view=list"><i class="fas fa-list-ul"></i><span>List View</span></a>
                                        <?php endif; ?>
                                    <?php endif; ?>

                                <?php endif; ?>

                                <?php // reservation page setting ?>
                                <?php if(($reservation_page_id = $this->nes_settings['event_reservation_page']) && ($this->nes_settings['show_reservation_link'] == 'yes')) : ?>
                                    <a class="nes-view-link" href="<?php echo get_permalink($reservation_page_id); ?>"><i class="fas fa-edit"></i><span>Make Reservation</span></a>
                                <?php endif; ?>

                                <input type="hidden" name="nes_active_view" value="<?php echo $current_view; ?>">
                                <span class="nes-clearer"></span>
                            </div>

                            <span class="nes-clearer"></span>
                        </form> 
                    </div>

                    <span class="clearer"></span>
                    <div id="nes-shortcode-view"></div>

                    <script type="text/javascript">

                        // define vars
                        var view,date,venues,categories;

                        jQuery(document).ready(function($){

                            // toogle "more" filter options
                            $('.nes-more-trigger').on('click',function(){
                                $('i', this).toggleClass('fa-angle-double-down fa-angle-double-up');
                                $(this).siblings('.nes-more-wrap').slideToggle('fast');
                                if($('span', this).text() == 'more'){
                                    $('span', this).text('less');
                                }else{
                                    $('span', this).text('more');
                                }
                            });

                            // toggle display options
                            $('.nes-filter-toggle').on('click',function(){
                                $('i',this).toggleClass('fa-angle-up fa-angle-down');
                                $('#nes-day-form').slideToggle('fast');
                            });

                            // initial pageload view
                            nes_change_view();                        

                            // refresh view on venue change
                            $('.nes-venue-filter label').on('click', function(e){
                                e.preventDefault();

                                // toggle checkbox
                                if($('span', this).hasClass("checked")){
                                    $('span', this).html('&nbsp;');
                                }else{
                                    $('span', this).html('<i class="fas fa-check"></i>');
                                }
                                $('span', this).toggleClass("checked");

                                // manually toggle checkbox
                                $('input', this).prop("checked", !$('input', this).prop("checked"));

                                // change the view
                                nes_change_view();
                            });                         

                             // refresh view on category change
                            // $('.nes-category-filter label').on('click', function(e){
                            $('.nes-category-filter label input').on('click', function(e){
                                // e.preventDefault();

                                // manually toggle checkbox
                                // $('input', this).prop("checked", !$('input', this).prop("checked"));

                                // change the view
                                nes_change_view();
                            });       

                            // refresh view on date change
                            $('.nes-datepicker select').on('change', function(){

                                // get current view date and add/subtract month
                                var month = $('select[name="nes_month"]').val();
                                var year = $('select[name="nes_year"]').val();
                                var date = year + '-' + month;

                                // update month/year title and prev/next buttons
                                var next_month = moment(date).add(1, 'months').format("YYYY-MM");
                                $('.nes-next-month').attr('data-date',next_month);

                                var next_month_formatted = moment(date).add(1, 'months').format("MMMM YYYY");
                                $('.nes-next-month span').text(next_month_formatted);

                                var prev_month= moment(date).subtract(1, 'months').format("YYYY-MM");
                                $('.nes-prev-month').attr('data-date',prev_month);

                                var prev_month_formatted = moment(date).subtract(1, 'months').format("MMMM YYYY");
                                $('.nes-prev-month span').text(prev_month_formatted);

                                // update the view title
                                $('.nes-month-year').text(moment(date).format("MMMM YYYY"));

                                // change the view
                                nes_change_view();
                            });     

                            // prev next buttons
                            $(document).on('click', '.nes-prev-month, .nes-next-month', function(e){
                                e.preventDefault();

                                // get current view date and add/subtract month
                                var date = $(this).attr('data-date');
                                var date_arr = date.split('-');

                                // update filter month/year
                                $('select[name="nes_year"]').val(date_arr[0]);
                                $('select[name="nes_month"]').val(date_arr[1]);

                                // update month/year title and prev/next buttons
                                var next_month = moment([date_arr[0], (date_arr[1] - 1)]).add(1, 'months').format("YYYY-MM");
                                $('.nes-next-month').attr('data-date',next_month);

                                var next_month_formatted = moment([date_arr[0], (date_arr[1] - 1)]).add(1, 'months').format("MMMM YYYY");
                                $('.nes-next-month span').text(next_month_formatted);

                                var prev_month = moment([date_arr[0], (date_arr[1] - 1)]).subtract(1, 'months').format("YYYY-MM");
                                $('.nes-prev-month').attr('data-date',prev_month);

                                var prev_month_formatted = moment([date_arr[0], (date_arr[1] - 1)]).subtract(1, 'months').format("MMMM YYYY");
                                $('.nes-prev-month span').text(prev_month_formatted);

                                // update the view title
                                $('.nes-month-year').text(moment([date_arr[0], (date_arr[1] - 1)]).format("MMMM YYYY"));

                                // change the view
                                nes_change_view();
                            });     

                            // for tooltips, check window width 200ms after resize or load
                            var TO = false;
                            $(window).resize(function(){
                                if(TO !== false){clearTimeout(TO);}
                                TO = setTimeout('nes_check_width()', 200);
                            }).resize();  

                            // prevent linking to event on mobile when dot is clicked
                            $(document).on('click', '.nes-event .nes-event-link', function(e){
                                if($(window).width() < 750){
                                    e.preventDefault();
                                }
                            }); 
                        });

                        function nes_change_view(){

                            // update vars
                            view = jQuery('input[name="nes_active_view"]').val();
                            date = jQuery('select[name="nes_year"]').val() + '-' + jQuery('select[name="nes_month"]').val();

                            // update view link
                            jQuery('.nes-month-list').attr('href', nes_update_param(jQuery('.nes-month-list').attr('href'), 'mo', jQuery('select[name="nes_month"]').val()));
                            jQuery('.nes-month-list').attr('href', nes_update_param(jQuery('.nes-month-list').attr('href'), 'yr', jQuery('select[name="nes_year"]').val()));

                            // update venues and categories
                            venues = jQuery(".nes-venue-filter input:checkbox:checked").map(function(){
                              return jQuery(this).val();
                            }).get();       

                            categories = jQuery(".nes-category-filter input:checkbox:checked").map(function(){
                              return jQuery(this).val();
                            }).get();

                            jQuery('#nes-shortcode-view').append('<div id="nes-loading"><p>Loading <i class="fas fa-spinner fa-pulse"></i></p></div>');
                            var data = {
                                'action': 'nes_ajax_view_change',
                                'date': date,
                                'view': view,
                                'venues': venues,
                                'categories': categories
                            };
                            jQuery.post("<?php echo NES_AJAX_URL; ?>", data, function(response){
                                jQuery('#nes-shortcode-view').html(response);

                                // for tooltips, check window width 200ms after resize or load
                                setTimeout('nes_check_width()', 200);

                                // set desktop class or not
                                if(jQuery(window).width() < 750){
                                    jQuery('.nes-event-tooltip').addClass('mobile');
                                }else{
                                    jQuery('.nes-event-tooltip').removeClass('mobile');
                                } 
                            }); 
                        }   

                        function nes_update_param(uri, key, value){
                            if(key){
                                var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
                                if(re){
                                    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
                                    if (uri.match(re)) {
                                        return uri.replace(re, '$1' + key + "=" + value + '$2');
                                    }
                                    else {
                                        return uri + separator + key + "=" + value;
                                    }
                                }
                            }
                        }

                        function nes_check_width(){
                            
                            // check nav level two
                            jQuery('.nes-event').each(function(){
                                if(jQuery(this).offset().left + jQuery(this).width() + jQuery('.nes-event-tooltip',this).width() + 40 > jQuery(window).width()){
                                    jQuery('.nes-event-tooltip',this).addClass('nes-tooltip-shift');
                                }else{
                                    jQuery('.nes-event-tooltip',this).removeClass('nes-tooltip-shift');
                                }
                            });
                        }    
                    </script>
                <?php endif; ?>
                </div>
                <?php return ob_get_clean();
            }
        }

        // frontend display - ajax request
        function nes_ajax_view_change(){
            ob_start();
            $view = $_POST['view'];
            $date = $_POST['date'];
            $venues = $_POST['venues'];

            $categories = array();
            if(isset($_POST['categories'])){
                $categories = $_POST['categories'];
            }
            ?>
            <?php if(empty($view) || ($view == 'nes-month-view')) : ?>

                <?php // draw table ?>
                <table cellpadding="0" cellspacing="0" id="nes-calendar-month" class="nes-calendar">
                    <thead>
                        <?php //table headings ?>
                        <?php $headings = array(__('Sun','nes'),__('Mon','nes'),__('Tue','nes'),__('Wed','nes'),__('Thu','nes'),__('Fri','nes'),__('Sat','nes')); ?>
                        <tr class="calendar-row">
                            <th class="calendar-day-head">
                                <?php echo implode('</th><th class="calendar-day-head">',$headings); ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            // days and weeks vars now ...
                            $month = date('m', strtotime($date));
                            $year = date('Y', strtotime($date));
                            $running_day = date('w', mktime(0,0,0,$month,1,$year));
                            $days_in_month = date('t', mktime(0,0,0,$month,1,$year));
                            $days_in_this_week = 1;
                            $day_counter = 0;
                            $today = strtotime('today');
                            $dates_array = array();
                        ?>
                        <?php 
                            // get events
                            $events = $this->nes_get_view_events(date('Y-m-d', strtotime("$year-$month")), $venues, $categories);
                        ?>
                        <?php // row for week one ?>
                        <tr class="calendar-row">
                        <?php // print "blank" days until the first of the current week ?>
                        <?php for($x = 0; $x < $running_day; $x++): ?>
                            <td class="calendar-day-np"></td>
                            <?php $days_in_this_week++; ?>
                        <?php endfor; ?>
                        <?php // keep going with days.... ?>
                        <?php for($list_day = 1; $list_day <= $days_in_month; $list_day++): ?>
                            <td class="calendar-day<?php if($today == mktime(0,0,0,$month,$list_day,$year)){echo ' today';}?>" data-date="<?php echo date('Y-m-d', strtotime("$year-$month-$list_day")); ?>">
                                <?php // add in the day number ?>
                                <div class="day-number"><?php echo $list_day; ?></div>

                                <?php if($events) : ?>
                                <div class="nes-event-wrap">
                                    <?php // use counter for "more" on a day ?>
                                    <?php $count = 0; ?>
                                    <?php $max_events = $this->nes_settings['max_events_per_day'] + 1; ?>

                                    <?php foreach($events as $event) : ?>
                                        <?php $post_id = $event->ID; ?>

                                        <?php // check if this event is on this day ?>
                                        <?php if(date('Y-m-d', strtotime(get_post_meta($event->ID, 'nes_event_date', true))) == date('Y-m-d', strtotime("$year-$month-$list_day"))) : ?>


                                            <?php if((get_post_meta($post_id,'nes_private_public',true) != 'private') && (get_post_meta($post_id,'nes_event_status',true) == 'approved')) : ?>
                                                     
                                                <?php // view more if necessary ?>   
                                                <?php $count++; ?>                              
                                                <?php if($count == $max_events) : ?>
                                                    <div class="nes-more-wrap" style="display:none;">
                                                <?php endif; ?>
                        
                                                <div class="nes-event">
                                                    <?php 
                                                        // get event/venue colors
                                                        $event_type = get_post_meta($post_id,'nes_event_type',true);
                                                        $venue_id = get_post_meta($post_id,'nes_venue_id',true);
                                                        if($event_type == 'offsite'){
                                                            $bg = $this->nes_settings['offsite_venue_bg'];
                                                            $tooltip_text = $bg;
                                                            $text = $this->nes_settings['offsite_venue_text'];
                                                            
                                                            // check for white bg
                                                            if($tooltip_text == 'ffffff'){
                                                                $tooltip_text = '111111';
                                                            }
                                                        }else{
                                                            $bg = get_post_meta($venue_id,'nes_venue_color',true);
                                                            $tooltip_text = $bg;

                                                            // check for white bg
                                                            if($tooltip_text == 'ffffff'){
                                                                $tooltip_text = '111111';
                                                            }
                                                            $text = get_post_meta($venue_id,'nes_venue_text_color',true); 
                                                            
                                                        }  
                                                    ?>
                                                    <a class="nes-event-link" href="<?php echo get_the_permalink($post_id); ?>" style="background:#<?php echo $bg; ?>;color:#<?php echo $text; ?> !important;"><?php echo get_the_title($post_id); ?></a>
                                                    <div class="nes-event-tooltip">
                                                        <ul>
                                                            <li><h5 style="color:#<?php echo $tooltip_text; ?>;"><?php if(get_post_meta($post_id,'nes_ticketed_event',true) == 'yes'){echo '<i class="fas fa-tags"></i>';}?><?php echo get_the_title($post_id); ?></h5></li>
                                                            <li><strong><?php echo date('F jS, Y', strtotime(get_post_meta($post_id, 'nes_event_date', true))); ?> @ <?php echo date('g:ia',strtotime(get_post_meta($post_id, 'nes_start_time', true))); ?> - <?php echo date('g:ia',strtotime(get_post_meta($post_id, 'nes_end_time', true))); ?></strong></li>
                                                            <li><?php echo $this->nes_custom_excerpt(150, $post_id); ?></li>
                                                            <li class="nes-event-venue">
                                                                <?php if($event_type != 'offsite') : ?>
                                                                    <?php 
                                                                        $venue_name = $this->nes_get_venue_name($venue_id); 
                                                                        echo $venue_name;
                                                                        $location_ids = get_post_meta($post_id,'nes_location_id',true);
                                                                        $location_names = $this->nes_get_location_names($location_ids);
                                                                        if($location_names && !in_array($venue_name, $location_names)){
                                                                            echo '<br/>' . implode(', ',$location_names);
                                                                        } 
                                                                    ?>
                                                                <?php else : ?>
                                                                    <?php echo get_post_meta($post_id,'nes_offsite_venue_name',true); ?><br/>
                                                                    <?php echo get_post_meta($post_id,'nes_offsite_venue_address',true); ?>
                                                                <?php endif; ?>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <?php if($count >= $max_events) : ?>
                                        </div>
                                        <div class="nes-more-trigger"><span>view <?php echo $count - ($max_events - 1); ?> more</span> <i class="far fa-angle-double-down"></i></div>
                                    <?php endif; ?>
                                </div>
                                <?php endif; ?>
                            </td>
                            <?php if($running_day == 6) : ?>
                                </tr>
                                <?php if(($day_counter+1) != $days_in_month) : ?>
                                    <tr class="calendar-row">
                                <?php endif; ?>
                                <?php $running_day = -1; ?>
                                <?php $days_in_this_week = 0; ?>
                            <?php endif; ?>
                            <?php $days_in_this_week++; $running_day++; $day_counter++; ?>
                        <?php endfor; ?>
                        <?php // finish the rest of the days in the week ?>

                        <?php if($days_in_this_week > 1 && $days_in_this_week < 8) : ?>
                            <?php for($x = 1; $x <= (8 - $days_in_this_week); $x++) : ?>
                                <td class="calendar-day-np"></td>
                            <?php endfor; ?>
                        <?php endif; ?>

                        </tr>
                    </tbody>
                </table>

                <script type="text/javascript">
                    jQuery(document).ready(function($){
                        $('.nes-calendar .nes-more-trigger').on('click', function(){
                            $('i', this).toggleClass('fa-angle-double-up fa-angle-double-down');
                            $(this).siblings('.nes-more-wrap').slideToggle('fast');
                            if($('span', this).text() == 'more'){
                                $('span', this).text('less');
                            }else{
                                $('span', this).text('more');
                            }
                        });
                    });
                </script>

                <div class="nes-prev-next-month-wrap">
                    <span class="nes-month-year"><?php echo date_i18n('F Y', strtotime($date)); ?></span>
                    <button class="nes-prev-month" data-date="<?php echo date('Y-m', strtotime($date . ' -1 month')); ?>"><i class="far fa-angle-double-left"></i> <span><?php echo date_i18n('F Y', strtotime($date . ' -1 month')); ?></span></button>
                    <button class="nes-next-month" data-date="<?php echo date('Y-m', strtotime($date . ' +1 month')); ?>"><span><?php echo date_i18n('F Y', strtotime($date . ' +1 month')); ?></span> <i class="far fa-angle-double-right"></i></button>
                    <span class="nes-clearer"></span>
                </div>

                <div class="nes-ical-buttons">
                    <a target="_blank" class="nes-button" href="<?php echo $this->nes_settings['dir'];?>views/frontend/nes-ical-feed.php"><i class="fas fa-plus"></i> EXPORT UPCOMING EVENTS</a>
                    <a target="_blank" class="nes-button" href="<?php echo $this->nes_settings['dir'];?>views/frontend/nes-ical-feed.php?type=month&amp;date=<?php echo $date; ?>"><i class="fas fa-plus"></i> EXPORT MONTH'S EVENTS</a>
                    <span class="nes-clearer"></span>
                </div>

            <?php elseif($view == 'nes-list-view') : ?>
                <?php // get featured events ?>
                <?php $featured_events = $this->nes_get_featured_events($date.'-01', $venues, $categories); ?> 
                <?php if($featured_events) : ?>
                    <h3 class="nes-featured-events-title">Featured <?php echo $this->nes_settings['event_plural']; ?></h3>
                    <ul class="nes-featured-event-list">
                        <?php foreach($featured_events as $event) : ?>
                            <?php $post_id = $event->ID; ?>
                            <?php // get venue color
                                $event_type = get_post_meta($post_id,'nes_event_type',true);
                                $venue_id = get_post_meta($post_id,'nes_venue_id',true);
                                if($event_type == 'offsite'){
                                    $bg_hex = '#' . $this->nes_settings['offsite_venue_bg'];
                                    $text = '#' . $this->nes_settings['offsite_venue_text'];
                                }else{
                                    $bg_hex = '#' . get_post_meta($venue_id,'nes_venue_color',true);
                                    $text = '#' . get_post_meta($venue_id,'nes_venue_text_color',true); 
                                } 
                                list($r, $g, $b) = sscanf($bg_hex, "#%02x%02x%02x");

                                // get the image
                                $image = get_the_post_thumbnail_url($post_id) ? get_the_post_thumbnail_url($post_id) : apply_filters('nes_default_image', '');
                            ?>
                            <li class="nes-featured-event" style="background:url('<?php echo $image; ?>') center no-repeat;background-size:cover;">
                                <a href="<?php echo get_the_permalink($post_id); ?>" style="background:rgba(<?php echo $r.','.$g.','.$b; ?>,0.8);color:<?php echo $text; ?> !important;">
                                    <span class="nes-featured-event-title">
                                        <strong><?php echo get_the_title($post_id); ?> <?php if(get_post_meta($post_id,'nes_ticketed_event',true) == 'yes'){echo '<i class="fas fa-tags"></i>';}?></strong>
                                        <span class="nes-featured-event-date">
                                            <?php $event_date = get_post_meta($post_id, 'nes_event_date', true); ?>
                                            <?php echo date('F', strtotime($event_date)); ?>
                                            <?php echo date('jS', strtotime($event_date)); ?>, 
                                            <?php echo date('Y', strtotime($event_date)); ?>
                                        </span> 
                                    </span>
                                    <span class="nes-featured-event-when">
                                        <strong><?php echo date('g:ia',strtotime(get_post_meta($post_id, 'nes_start_time', true))); ?> - <?php echo date('g:ia',strtotime(get_post_meta($post_id, 'nes_end_time', true))); ?></strong>
                                    </span>
                                   
                                    <address class="nes-featured-event-where">  
                                        <?php $event_type = get_post_meta($post_id,'nes_event_type',true); ?>
                                        <?php if($event_type != 'offsite') : ?>
                                            <?php 
                                                $venue_name = $this->nes_get_venue_name($venue_id); 
                                                echo $venue_name;
                                                $location_ids = get_post_meta($post_id,'nes_location_id',true);
                                                $location_names = $this->nes_get_location_names($location_ids);
                                                if($location_names && !in_array($venue_name, $location_names)){
                                                    echo '<br/>' . implode(', ',$location_names);
                                                } 
                                            ?>
                                        <?php else : ?>
                                            <?php echo get_post_meta($post_id,'nes_offsite_venue_name',true); ?><br/>
                                            <?php echo get_post_meta($post_id,'nes_offsite_venue_address',true); ?>
                                        <?php endif; ?>
                                    </address>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
                <?php 
                    // get events
                    $events = $this->nes_get_view_events($date.'-01', $venues, $categories);
                ?>
                <?php if($events) : ?>
                    <ul class="nes-event-list">
                        <?php foreach($events as $event) : ?>
                        <li class="nes-event-list-item">
                            <?php $post_id = $event->ID; ?>
                            <?php // get venue color
                                $event_type = get_post_meta($post_id,'nes_event_type',true);
                                $venue_id = get_post_meta($post_id,'nes_venue_id',true);
                                if($event_type == 'offsite'){
                                    $bg = $this->nes_settings['offsite_venue_bg'];
                                    $text = $this->nes_settings['offsite_venue_text'];
                                }else{
                                    $bg = get_post_meta($venue_id,'nes_venue_color',true);
                                    $tooltip_text = $bg;
                                    $text = get_post_meta($venue_id,'nes_venue_text_color',true); 
                                }  
                            ?>                
                            <div class="nes-list-item-date-info" style="background:#<?php echo $bg; ?>;color:#<?php echo $text; ?> !important;">
                                <?php $event_date = get_post_meta($post_id, 'nes_event_date', true); ?>
                                <div class="nes-list-item-month"> <?php echo date('F', strtotime($event_date)); ?></div>
                                <div class="nes-list-item-day"> <?php echo date('jS', strtotime($event_date)); ?></div>
                                <div class="nes-list-item-year"> <?php echo date('Y', strtotime($event_date)); ?></div>
                            </div>
                            <div class="nes-list-item-info">
                                <h3 class="nes-list-item-title"><a href="<?php echo get_the_permalink($post_id); ?>"><?php echo get_the_title($post_id); ?></a><?php if(get_post_meta($post_id,'nes_ticketed_event',true) == 'yes'){echo '<i class="fas fa-tags"></i>';}?></h3>
                                <p class="nes-when"><strong><?php echo date('g:ia',strtotime(get_post_meta($post_id, 'nes_start_time', true))); ?> - <?php echo date('g:ia',strtotime(get_post_meta($post_id, 'nes_end_time', true))); ?></strong></p>
                                <address class="nes-where">
                                    <?php $event_type = get_post_meta($post_id,'nes_event_type',true); ?>
                                    <?php if($event_type != 'offsite') : ?>
                                        <?php 
                                            $venue_name = $this->nes_get_venue_name($venue_id); 
                                            echo $venue_name;
                                            $location_ids = get_post_meta($post_id,'nes_location_id',true);
                                            $location_names = $this->nes_get_location_names($location_ids);
                                            if($location_names && !in_array($venue_name, $location_names)){
                                                echo '<br/>' . implode(', ',$location_names);
                                            } 
                                        ?>
                                    <?php else : ?>
                                        <?php echo get_post_meta($post_id,'nes_offsite_venue_name',true); ?><br/>
                                        <?php echo get_post_meta($post_id,'nes_offsite_venue_address',true); ?>
                                    <?php endif; ?>
                                </address>
                                <div class="nes-excerpt">
                                    <?php echo $this->nes_custom_excerpt(300, $post_id); ?>
                                </div>
                                <p><a href="<?php echo get_the_permalink($post_id); ?>"></a></p>
                            </div>
                            <span class="nes-clearer"></span>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                <?php else : ?>
                    <ul class="nes-event-list">
                        <li class="nes-event-list-item"><h3 class="nes-event-list-notice">Sorry, no events were found using the display options above.</h3></li>
                    <ul>
                <?php endif; ?>

                <div class="nes-prev-next-month-wrap">
                    <button class="nes-prev-month" data-date="<?php echo date('Y-m', strtotime($date . ' -1 month')); ?>"><i class="far fa-angle-double-left"></i> <?php echo date_i18n('F Y', strtotime($date . ' -1 month')); ?></button>
                    <button class="nes-next-month" data-date="<?php echo date('Y-m', strtotime($date . ' +1 month')); ?>"><?php echo date_i18n('F Y', strtotime($date . ' +1 month')); ?> <i class="far fa-angle-double-right"></i></button>
                    <span class="nes-clearer"></span>
                </div>

                <div class="nes-ical-buttons">
                    <a target="_blank" class="nes-button" href="<?php echo $this->nes_settings['dir'];?>views/frontend/nes-ical-feed.php"><i class="fas fa-plus"></i> EXPORT UPCOMING EVENTS</a>
                    <a target="_blank" class="nes-button" href="<?php echo $this->nes_settings['dir'];?>views/frontend/nes-ical-feed.php?type=month&amp;date=<?php echo $date; ?>"><i class="fas fa-plus"></i> EXPORT MONTH'S EVENTS</a>
                    <span class="nes-clearer"></span>
                </div>

            <?php elseif($view == 'nes-reservation') : ?>

                <?php // assign single venue id to a variable ?>
                <?php $venue_id = $venues[0]; ?>

                <?php // check if "tabbed layout" was chosen ?>
                <?php if($this->nes_settings['use_tabbed_layout'] == "yes") : ?>
                    
                    <div id="nes-table-wrap">
                        <?php if($venue_id == 'offsite') : ?>
                            <?php // offsite venue just shows the form right away ?>
                            <?php echo $this->nes_make_offsite_event_form($date); ?>
                        <?php else : ?>
                            <?php // get venue meta ?>
                            <?php $venue_start = get_post_meta($venue_id, 'nes_venue_'.strtolower(date('l',strtotime($date))).'_start', true); ?>
                            <?php $venue_end = get_post_meta($venue_id, 'nes_venue_'.strtolower(date('l',strtotime($date))).'_end', true); ?>
                            <?php $venue_blackouts = $this->nes_get_venue_blackouts($venue_id); ?>

                            <?php // get location venues ?>
                            <?php $locations = $this->nes_get_venue_locations($venue_id); ?>
                            <?php if($locations) : ?>
                            <div class="nes-location-tabs">
                                <p><?php echo $this->nes_settings['location_plural']; ?></p>
                                <ul>
                                <?php $first = true; ?>
                                <?php foreach($locations as $location) : ?>
                                    <li <?php if($first){echo 'class="nes-active-tab"';} $first = false; ?> data-id="<?php echo $location->ID; ?>" ><?php echo $location->post_title; ?></li>
                                <?php endforeach; ?>
                                </ul>
                            </div>
                               
                            <div class="nes-tabbed-view">
                                <?php $first = true; ?>

                                <?php foreach($locations as $location) : ?>
                                <table cellpadding="0" cellspacing="0" class="nes-day-view" data-id="<?php echo $location->ID; ?>" <?php if(!$first){echo 'style="display:none;"';} $first = false;?>>
                                    <col style="width:20%;">
                                    <col style="width:80%;">
                                    <thead>
                                        <tr class="nes-header-row" <?php echo apply_filters('nes_table_header_background', ''); ?>>
                                            <th class="nes-times">&nbsp;</th>
                                            <th><?php echo $location->post_title; ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                        global $wpdb;
                                        $query_string = "
                                            SELECT DISTINCT p.ID
                                            FROM $wpdb->posts p
                                            LEFT JOIN $wpdb->postmeta m1 ON p.ID = m1.post_id
                                            LEFT JOIN $wpdb->postmeta m2 ON p.ID = m2.post_id
                                            WHERE p.post_type = 'nes_event'
                                            AND (p.post_status = 'publish' OR p.post_status = 'past_events')
                                            AND (m1.meta_key = 'nes_venue_id' AND m1.meta_value = '$venue_id')
                                            AND (m2.meta_key = 'nes_event_date' AND m2.meta_value = '$date')
                                        ";
                                        $events = $wpdb->get_results($query_string, OBJECT);
                                    ?>
                                    <?php if($this->nes_settings['match_minmax_interval'] == 'yes') : ?>
                                        <?php $interval = $this->nes_settings['end_time_minmax_interval']; ?>
                                    <?php else : ?>
                                        <?php $interval = 0.25; ?>
                                    <?php endif; ?>
                                    <?php $times = $this->nes_get_times($this->nes_settings['day_start_time'],$this->nes_settings['day_end_time'],$interval); ?>

                                    <?php if($times) : ?>
                                        <?php $event_displayed = ''; ?>
                                        <?php foreach($times as $time) : ?>
                                            <?php $time_string = strtotime($time); ?>
                                            <tr>
                                                <td class="time"><?php echo date('g:i a',strtotime($time)); ?></td>
                                            <?php //foreach($locations->posts as $location) : ?>
                                                <td id="<?php echo $venue_id . '-' . $location->ID . '-' . $date . '-' . str_replace(':','',$time); ?>"
                                                    <?php 
                                                        $time_status = 'not-available';

                                                        // check for "blackout dates/times"
                                                        if(!$this->nes_is_blacked_out($venue_blackouts, $location->ID, $date, $time)){

                                                            // check for custom availability 
                                                            if(get_post_meta($location->ID, 'nes_venue_availability', true) == 'custom'){ 
                                                                $location_start = get_post_meta($location->ID, 'nes_location_'.strtolower(date('l',strtotime($date))).'_start', true);
                                                                $location_end = get_post_meta($location->ID, 'nes_location_'.strtolower(date('l',strtotime($date))).'_end', true);
                                                                
                                                                if((strtotime($location_start) <= $time_string) && ($time_string <= strtotime($location_end))){
                                                                    $time_status = 'available';

                                                                }
                                                            }elseif((strtotime($venue_start) <= $time_string) && ($time_string <= strtotime($venue_end))){  
                                                                //check location availability
                                                                $time_status = 'available';

                                                            }
                                                        }

                                                    ?>
                                                    class="<?php echo $time_status; ?>" <?php echo apply_filters('nes_unavailable_background', '', $time_status); ?>>
                                              
                                                    <?php if($events) : ?>
                                                        <?php foreach($events as $event) : ?>
                                                            <?php $status = get_post_meta($event->ID, 'nes_event_status', true); ?>
                                                            <?php $found = false; ?>
                                                            <?php if(is_array(get_post_meta($event->ID, 'nes_location_id', true))) : ?>
                                                                <?php if(in_array($location->ID, get_post_meta($event->ID, 'nes_location_id', true)) && $time >= get_post_meta($event->ID, 'nes_start_setup_time',true) && $time < get_post_meta($event->ID, 'nes_end_cleanup_time',true)) : ?>
                                                                    <?php $found = true; ?>
                                                                <?php endif; ?>
                                                            <?php elseif(get_post_meta($event->ID, 'nes_location_id', true) == $location->ID && $time >= get_post_meta($event->ID, 'nes_start_setup_time',true) && $time < get_post_meta($event->ID, 'nes_end_cleanup_time',true)) : ?>
                                                                <?php $found = true; ?> 
                                                            <?php endif; ?>
                                                            <?php if($found) : ?>

                                                                <?php $start = get_post_meta($event->ID, 'nes_start_setup_time', true); ?>
                                                                <?php $end = get_post_meta($event->ID, 'nes_end_cleanup_time', true); ?>
                                                                <?php $diff = (strtotime($end) - strtotime($start))/60; ?>
                                                                <?php $int_minutes = $this->nes_decimal_to_minutes($interval);?>
                                                                <?php $height = ($diff/$int_minutes) * 30;  ?>
                                                                
                                                                <?php if($status != 'denied' && $status != 'private') : ?>

                                                                    <?php if($start <= $time && $end >= $time && ($event_displayed != $event->ID)) : ?>
                                                                        <?php $event_displayed = $event->ID; ?>
                                                                        <div class="nes-event <?php echo $status; ?>" style="height:<?php echo $height; ?>px;<?php echo apply_filters('nes_event_background', '', $status); ?>">
                                                                        
                                                                            <?php if($this->nes_settings['show_event_details'] == 'yes') : ?>
                                                                                <?php echo get_the_title($event->ID) . ' (' . date('g:i a', strtotime($start)) . ' - ' . date('g:i a', strtotime($end)) . ')'; ?>
                                                                            <?php else : ?>
                                                                                <?php echo date('g:i a', strtotime($start)) . ' - ' . date('g:i a', strtotime($end)); ?>
                                                                            <?php endif; ?>
                                                                        
                                                                        </div>
                                                                    <?php else : ?>
                                                                        <div></div>
                                                                    <?php endif; ?>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </td>
                                            <?php //endforeach; ?>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    </tbody>
                                </table>                            

                                <?php endforeach; ?>
                            </div>
                            <?php else : ?>
                                <p>Sorry, no <?php echo $this->nes_settings['location_plural']; ?> were found.</p>
                            <?php endif; ?>
                
                            <div id="nes-event-form-wrap" style="display:none;"></div>


                            <script type="text/javascript">
                                jQuery(document).ready(function($){

                                    // toggle tabbed tables
                                    $('.nes-location-tabs li').on('click',function(){
                                        $('.nes-location-tabs li').removeClass('nes-active-tab');
                                        $(this).addClass('nes-active-tab');
                                        $('.nes-day-view').hide();
                                        var data_id = $(this).attr('data-id');
                                        $('[data-id="'+data_id+'"]').fadeIn('fast');
                                    });

                                    //floating table headers
                                    // if($('#wpadminbar').length > 0){ 
                                    //     $(".nes-tabbed-view table").floatThead({
                                    //         //useAbsolutePositioning: true,
                                    //         scrollingTop: 32,
                                    //         autoReflow:true
                                    //     });
                                       
                                    // }else{
                                    //    $(".nes-tabbed-view table").floatThead({
                                    //         //useAbsolutePositioning: false
                                    //         autoReflow:true
                                    //     });
                                    // }
                                    
                                    $(".nes-day-view td").each(function(){
                                        if($(this).children('div').length > 0){
                                            $(this).removeClass('available').addClass('not-available');
                                        }
                                    });

                                    // prev next buttons
                                    $('.nes-prev-day, .nes-next-day').on('click', function(){
                                        $('#nes-date').val($(this).attr('data-date'));
                                        $('#nes-prev-next').val('1');
                                        $('#nes-day-form').submit();
                                    });

                                    // toggle form handling
                                    var clicked = false;
                                    $('.nes-day-view .available').on('click', function(){ 
                                        $('.nes-editing').removeClass('nes-editing').html('');
                                        $(this).addClass('nes-editing').html('Selected <i class="far fa-check"></i>');
                                        var $clicked = $(this);
                                        var data = {
                                            'action' : 'nes_ajax_make_event_form',
                                            'selected' : $(this).attr('id'),
                                            'date' : "<?php echo $date; ?>"
                                        };
                                        if(!clicked){
                                            $.post("<?php echo NES_AJAX_URL; ?>", data, function(response){
                                                //$('#nes-event-form-wrap').html(response);

                                                $('#nes-added-tr').remove();

                                                // count table columns
                                                var cols = 0;
                                                $('.nes-day-view tr:nth-child(1) td').each(function () {
                                                    cols++;
                                                });
                                                var res_var = "<?php echo $this->nes_settings['event_single']; ?>";
                                                $clicked.parents('tr:first').after('<tr id="nes-added-tr"><td colspan="'+cols+'" style="padding:0;"><div id="nes-clicked-form" style="display:none;"><h6>STEP 1: Check Availability <span>close <i class="fas fa-times-circle"></i></h6>'+response+'<span class="nes-clearer"></span></div></td></tr>');
                                                $('#nes-clicked-form').slideToggle('fast');
                                                //$('#nes-thickbox-link').trigger('click');
                                                clicked = false;
                                            });
                                        }
                                        clicked = true; 
                                    });

                                    // remove form when closed is clicked
                                    $(document).on('click', '.nes-day-view #nes-clicked-form h6 span', function(){
                                        $('.nes-day-view #nes-added-tr').remove();
                                        $('.nes-editing').removeClass('nes-editing').html('');
                                    });

                                });
                            </script>

                            <?php // check if "tabbed layout follow" was chosen ?>
                            <?php if($this->nes_settings['use_tabbed_layout_follow'] == "yes") : ?>
                                <script type="text/javascript">
                                    jQuery(document).ready(function($){
                                        // scroll tabbed locations
                                        var element = $('.nes-location-tabs'),
                                            originalY = element.offset().top;

                                        // Space between element and top of screen (when scrolling)
                                        var topMargin = 60;

                                        // Should probably be set in CSS; but here just for emphasis
                                        element.css('position', 'relative');

                                        $(window).on('scroll', function(event) {
                                            var scrollTop = $(window).scrollTop();

                                            // only scroll if non mobile
                                            if($(window).width() > 750){
                                                element.stop(false, false).animate({
                                                    top: scrollTop < originalY
                                                            ? 0
                                                            : scrollTop - originalY + topMargin
                                                }, 300);
                                            } else {
                                                element.css('top', 0);
                                            }
                                        });
                                    });
                                </script>
                            <?php endif; ?>

                        <?php endif; ?>

                        <span class="nes-clearer"></span>
                        <div class="nes-prev-next-day-wrap">
                            <span class="nes-month-year"><?php echo date_i18n('l F jS, Y', strtotime($date)); ?></span>
                            <button class="nes-prev-month" data-date="<?php echo date('Y-m-d', strtotime($date . ' -1 day')); ?>"><i class="far fa-angle-double-left"></i> <?php echo date('F jS, Y', strtotime($date . ' -1 day')); ?></button>
                            <button class="nes-next-month" data-date="<?php echo date('Y-m-d', strtotime($date . ' +1 day')); ?>"><?php echo date('F jS, Y', strtotime($date . ' +1 day')); ?> <i class="far fa-angle-double-right"></i></button>
                            <span class="nes-clearer"></span>
                        </div>
                    </div>

                <?php else : ?>

                    <?php // standard table layout ?>
                    <div id="nes-table-wrap">
        
                        <?php if($venue_id == 'offsite') : ?>
                            <?php // offsite venue just shows the form right away ?>
                            <?php echo $this->nes_make_offsite_event_form($date); ?>
                        <?php else : ?>
                            <?php $venue_start = get_post_meta($venue_id, 'nes_venue_'.strtolower(date('l',strtotime($date))).'_start', true); ?>
                            <?php $venue_end = get_post_meta($venue_id, 'nes_venue_'.strtolower(date('l',strtotime($date))).'_end', true); ?>

                            <?php // build location ID array ?>
                            <?php $location_ids = array(); ?>

                            <?php // get venue locations ?>
                            <?php $locations = $this->nes_get_venue_locations($venue_id); ?>
                            <?php if($locations) : ?>
                                <table cellpadding="0" cellspacing="0" class="nes-day-view">
                                    <thead>
                                        <tr class="nes-header-row" <?php echo apply_filters('nes_table_header_background', ''); ?>>
                                            <th class="nes-times">&nbsp;</th>
                                        <?php foreach($locations as $location) : ?>
                                            <th><?php echo $location->post_title; ?></th>
                                            <?php $location_ids[$location->ID] = ''; ?>
                                        <?php endforeach; ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                        global $wpdb;
                                        $query_string = "
                                            SELECT DISTINCT p.ID
                                            FROM $wpdb->posts p
                                            LEFT JOIN $wpdb->postmeta m1 ON p.ID = m1.post_id
                                            LEFT JOIN $wpdb->postmeta m2 ON p.ID = m2.post_id
                                            WHERE p.post_type = 'nes_event'
                                            AND (p.post_status = 'publish' OR p.post_status = 'past_events')
                                            AND (m1.meta_key = 'nes_venue_id' AND m1.meta_value = '$venue_id')
                                            AND (m2.meta_key = 'nes_event_date' AND m2.meta_value = '$date')
                                        ";
                                        $events = $wpdb->get_results($query_string, OBJECT);
                                    ?>
                                    <?php if($this->nes_settings['match_minmax_interval'] == 'yes') : ?>
                                        <?php $interval = $this->nes_settings['end_time_minmax_interval']; ?>
                                    <?php else : ?>
                                        <?php $interval = 0.25; ?>
                                    <?php endif; ?>
                                    <?php $times = $this->nes_get_times($this->nes_settings['day_start_time'],$this->nes_settings['day_end_time'],$interval); ?>
                                    <?php if($times) : ?>
                                        <?php foreach($times as $time) : ?>
                                            <tr>
                                                <td class="time"><?php echo date('g:i a',strtotime($time)); ?></td>
                                            <?php foreach($locations as $location) : ?>
                                                
                                                <td id="<?php echo $venue_id . '-' . $location->ID . '-' . $date . '-' . str_replace(':',':',$time); ?>"
                                                    <?php 
                                                        $status = 'not-available';
            											
            											// check for custom availability 
                                                        if(get_post_meta($location->ID, 'nes_venue_availability', true) == 'custom'){ 
                                                            $location_start = get_post_meta($location->ID, 'nes_location_'.strtolower(date('l',strtotime($date))).'_start', true);
                                                            $location_end = get_post_meta($location->ID, 'nes_location_'.strtolower(date('l',strtotime($date))).'_end', true);
            												
                                                            if(($location_start <= date('H:i', strtotime($time))) && (date('H:i', strtotime($time)) <= $location_end)){
            													$status = 'available';
            												}
            											}
            											else{
            												//check location availability
            												if(($venue_start <= date('H:i', strtotime($time))) && (date('H:i', strtotime($time)) <= $venue_end)){
            													$status = 'available';
            												}
                                                        } 
            											
            										?>
            										class="<?php echo $status; ?>" <?php echo apply_filters('nes_unavailable_background', '', $status); ?>>
                                              
                                                    <?php if($events) : ?>
                                                        <?php foreach($events as $event) : ?>
                                                            <?php $found = false; ?>
                                                            <?php if(is_array(get_post_meta($event->ID, 'nes_location_id', true))) : ?>
                                                                <?php if(in_array($location->ID, get_post_meta($event->ID, 'nes_location_id', true)) && $time >= get_post_meta($event->ID, 'nes_start_setup_time',true) && $time < get_post_meta($event->ID, 'nes_end_cleanup_time',true)) : ?>
                                                                    <?php $found = true; ?>
                                                                <?php endif; ?>
                                                            <?php elseif(get_post_meta($event->ID, 'nes_location_id', true) == $location->ID && $time >= get_post_meta($event->ID, 'nes_start_setup_time',true) && $time < get_post_meta($event->ID, 'nes_end_cleanup_time',true)) : ?>
                                                                    <?php $found = true; ?>
                                                                <?php endif; ?>
                                                            <?php if($found) : ?>
                                                                <?php $start = get_post_meta($event->ID, 'nes_start_setup_time', true); ?>
                                                                <?php $end = get_post_meta($event->ID, 'nes_end_cleanup_time', true); ?>
                                                                <?php $diff = (strtotime($end) - strtotime($start))/60; ?>
                                                                <?php $int_minutes = $this->nes_decimal_to_minutes($interval); ?>
                                                                <?php $height = ($diff/$int_minutes) * 30;  ?>
                                                                <?php $status = get_post_meta($event->ID, 'nes_event_status', true); ?>
                                                                <?php if($status != 'denied' && $status != 'private') : ?>

                                                                    <?php if(($start <= $time) && ($end >= $time) && ($location_ids[$location->ID] != $event->ID)) : ?>
                                                                        <?php $location_ids[$location->ID] = $event->ID; ?>
                                                                        <div class="nes-event <?php echo $status; ?>" style="height:<?php echo $height; ?>px;<?php echo apply_filters('nes_event_background', '', $status); ?>">
                                                                            <?php if($this->nes_settings['show_event_details'] == 'yes') : ?>
                                                                                <?php echo get_the_title($event->ID) . ' (' . date('g:i a', strtotime($start)) . ' - ' . date('g:i a', strtotime($end)) . ')'; ?>
                                                                            <?php else : ?>
                                                                                <?php echo date('g:i a', strtotime($start)) . ' - ' . date('g:i a', strtotime($end)); ?>
                                                                            <?php endif; ?>
                                                                        </div>
                                                                    <?php else : ?>
                                                                        <div></div>
                                                                    <?php endif; ?>

                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </td>
                                                <?php $event_location = $location->ID; ?>
                                            <?php endforeach; ?>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    </tbody>
                                </table>
                            <?php else : ?>
                                <p><?php printf(__('Sorry, no %1$s were found'),$this->nes_settings['location_plural']); ?>.</p>
                            <?php endif; ?>
                            
                            <div id="nes-event-form-wrap" style="display:none;"></div>
                            <script type="text/javascript">
                                jQuery(document).ready(function($){

                                    // set even widths for the location columns
                                    $(".nes-header-row th:not(.nes-times)").css('width',(100/$(".nes-header-row th:not(.nes-times)").length) + '%');

                                    if($('#wpadminbar').length > 0){ 
                                        $(".nes-day-view").floatThead({
                                            //useAbsolutePositioning: true,
                                            scrollingTop: 32
                                        });
                                       
                                    }else{
                                       $(".nes-day-view").floatThead({
                                            // useAbsolutePositioning: false
                                        });
                                    }

                                    $(".nes-day-view td").each(function(){
                                        if($(this).children('div').length > 0){$(this).toggleClass('available not-available');}
                                    });

                                    var clicked = false;
                                    $('.nes-day-view .available').on('click', function(){ 
                                        $('.nes-editing').removeClass('nes-editing').html('');
                                        $(this).addClass('nes-editing').html('Selected <i class="far fa-check"></i>');
                                        var $clicked = $(this);
                                        var data = {
                                            'action' : 'nes_ajax_make_event_form',
                                            'selected' : $(this).attr('id'),
                                            'date' : "<?php echo $date; ?>"
                                        };
                                        if(!clicked){
                                            $.post("<?php echo NES_AJAX_URL; ?>", data, function(response){

                                                $('#nes-added-tr').remove();

                                                // count table columns
                                                var cols = 0;
                                                $('.nes-day-view tr:nth-child(1) td').each(function () {
                                                    cols++;
                                                });
                                                $clicked.parents('tr:first').after('<tr id="nes-added-tr"><td colspan="'+cols+'" style="padding:0;"><div id="nes-clicked-form" style="display:none;"><h6>STEP 1: Check Availability <span>'+"<?php echo __('close','nes');?>"+' <i class="fas fa-times-circle"></i></h6>'+response+'</div></td></tr>');
                                                $('#nes-clicked-form').slideToggle('fast');
     
                                                clicked = false;
                                            });
                                        }
                                        clicked = true; 
                                    });

                                    // remove form when closed is clicked
                                    $(document).on('click', '.nes-day-view #nes-clicked-form h6 span', function(){
                                        $('.nes-day-view #nes-added-tr').remove();
                                        $('.nes-editing').removeClass('nes-editing').html('');
                                    });

                                });
                            </script>
                        <?php endif; ?>
                        <span class="nes-clearer"></span>

                        <div class="nes-prev-next-day-wrap">
                            <span class="nes-month-year"><?php echo date_i18n('l F jS, Y', strtotime($date)); ?></span>
                            <button class="nes-prev-month" data-date="<?php echo date('Y-m-d', strtotime($date . ' -1 day')); ?>"><i class="far fa-angle-double-left"></i> <?php echo date('F jS, Y', strtotime($date . ' -1 day')); ?></button>
                            <button class="nes-next-month" data-date="<?php echo date('Y-m-d', strtotime($date . ' +1 day')); ?>"><?php echo date('F jS, Y', strtotime($date . ' +1 day')); ?> <i class="far fa-angle-double-right"></i></button>
                            <span class="nes-clearer"></span>
                        </div>
                    </div>
                <?php endif; ?>

            <?php endif; ?>

            <?php echo ob_get_clean(); ?>
            <?php die();
        }

        // get blackout dates
        function nes_get_venue_blackouts($venue_id){
            
            $venue_blackouts = array();

            $blackout_locations_arr = get_post_meta($venue_id, 'nes_blackout_locations', true);
            $blackout_locations = array();
            if($blackout_locations_arr){
                foreach($blackout_locations_arr as $blackout_location_arr){
                    $blackout_locations[] = $blackout_location_arr;
                }
            
                $blackout_start_date = get_post_meta($venue_id, 'nes_blackout_start_date', true);
                $blackout_end_date = get_post_meta($venue_id, 'nes_blackout_end_date', true);
                $blackout_start_time = get_post_meta($venue_id, 'nes_blackout_start_time', true);
                $blackout_end_time = get_post_meta($venue_id, 'nes_blackout_end_time', true);

                $venue_blackouts = array(
                    'locations' => $blackout_locations,
                    'start_date' => $blackout_start_date,
                    'end_date' => $blackout_end_date,
                    'start_time' => $blackout_start_time,
                    'end_time' => $blackout_end_time
                );
            }

            return $venue_blackouts;
        }

        // check if this location/date/time is blacked out
        function nes_is_blacked_out($venue_blackouts, $location_id, $date, $time){
            // must match all to be true
            $blacked_out = false;
            if($venue_blackouts){

                // check locations
                foreach($venue_blackouts['locations'] as $key => $locations_arr){

                    // has location?
                    if(in_array($location_id, $locations_arr)){

                        // then start_date
                        if(strtotime($venue_blackouts['start_date'][$key]) <= strtotime($date)){


                            // then end_date
                            if(strtotime($date) <= strtotime($venue_blackouts['end_date'][$key])){

                                // then start_time
                                if(strtotime($venue_blackouts['start_time'][$key]) <= strtotime($time)){

                                    // then end_time
                                    if(strtotime($time) <= strtotime($venue_blackouts['end_time'][$key])){
                                        $blacked_out = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return $blacked_out;
        }

        function nes_get_view_events($query_date, $venues = array(), $categories = array(), $featured = false){

            // set temp venue id variable
            $venue_ids = $venues;
            $offsite = false;
            $all_ids = array();
            global $wpdb;

            // remove offsite from venue array for later search
            if(is_array($venue_ids)){
                $index = array_search('offsite', $venue_ids);
                if($index !== false){
                    $offsite = true;
                    unset($venue_ids[$index]);
                }
            }

            // implode venues
            $venue_id_str = false;
            if($venue_ids){$venue_id_str = implode("','", $venue_ids);}            

            // implode cats
            $cat_str = false;
            if($categories){$cat_str = implode("','", $categories);}

            // make date range for list
            $query_end_date = date('Y-m-d',(strtotime('next month',strtotime($query_date))));

            // check if we need to include offsite
            if($offsite){
                $query_string = "
                    SELECT DISTINCT p.ID, m1.meta_value AS 'date', m5.meta_value AS 'time'
                    FROM $wpdb->posts p
                    INNER JOIN $wpdb->term_relationships tr ON (p.ID = tr.object_id)
                    INNER JOIN $wpdb->term_taxonomy tt ON (tr.term_taxonomy_id = tt.term_taxonomy_id)
                    INNER JOIN $wpdb->terms t ON (t.term_id = tt.term_id)
                    LEFT JOIN $wpdb->postmeta m1 ON p.ID = m1.post_id
                    LEFT JOIN $wpdb->postmeta m2 ON p.ID = m2.post_id
                    LEFT JOIN $wpdb->postmeta m3 ON p.ID = m3.post_id
                    LEFT JOIN $wpdb->postmeta m4 ON p.ID = m4.post_id
                    LEFT JOIN $wpdb->postmeta m5 ON (p.ID = m5.post_id AND m5.meta_key = 'nes_start_time')
                    WHERE p.post_type = 'nes_event'
                    AND p.post_status IN ('publish', 'past_events')
                    AND (tt.taxonomy = 'event_category' AND t.term_id IN ('$cat_str'))
                    AND (m1.meta_key = 'nes_event_date' AND (m1.meta_value >= '$query_date' AND m1.meta_value < '$query_end_date'))
                    AND (m2.meta_key = 'nes_event_type' AND m2.meta_value = 'offsite')
                    AND (m3.meta_key = 'nes_event_status' AND m3.meta_value = 'approved')
                    AND (m4.meta_key = 'nes_private_public' AND m4.meta_value = 'public')
                    UNION
                    SELECT DISTINCT p.ID, m1.meta_value AS 'date', m5.meta_value AS 'time'
                    FROM $wpdb->posts p
                    INNER JOIN $wpdb->term_relationships tr ON (p.ID = tr.object_id)
                    INNER JOIN $wpdb->term_taxonomy tt ON (tr.term_taxonomy_id = tt.term_taxonomy_id)
                    INNER JOIN $wpdb->terms t ON (t.term_id = tt.term_id)
                    LEFT JOIN $wpdb->postmeta m1 ON p.ID = m1.post_id
                    LEFT JOIN $wpdb->postmeta m2 ON p.ID = m2.post_id
                    LEFT JOIN $wpdb->postmeta m3 ON p.ID = m3.post_id
                    LEFT JOIN $wpdb->postmeta m4 ON p.ID = m4.post_id
                    LEFT JOIN $wpdb->postmeta m5 ON (p.ID = m5.post_id AND m5.meta_key = 'nes_start_time')
                    LEFT JOIN $wpdb->postmeta m6 ON p.ID = m6.post_id
                    WHERE p.post_type = 'nes_event'
                    AND p.post_status IN ('publish', 'past_events')
                    AND (tt.taxonomy = 'event_category' AND t.term_id IN ('$cat_str'))
                    AND (m1.meta_key = 'nes_event_date' AND (m1.meta_value >= '$query_date' AND m1.meta_value < '$query_end_date'))
                    AND (m2.meta_key = 'nes_event_type' AND m2.meta_value != 'offsite')
                    AND (m3.meta_key = 'nes_event_status' AND m3.meta_value = 'approved')
                    AND (m4.meta_key = 'nes_private_public' AND m4.meta_value = 'public')
                    AND (m6.meta_key = 'nes_venue_id' AND m6.meta_value IN ('$venue_id_str'))
                    ORDER BY date ASC, time ASC
                "; 

            } else {
                $query_string = "
                    SELECT DISTINCT p.ID, m1.meta_value AS 'date', m5.meta_value AS 'time'
                    FROM $wpdb->posts p
                    INNER JOIN $wpdb->term_relationships tr ON (p.ID = tr.object_id)
                    INNER JOIN $wpdb->term_taxonomy tt ON (tr.term_taxonomy_id = tt.term_taxonomy_id)
                    INNER JOIN $wpdb->terms t ON (t.term_id = tt.term_id)
                    LEFT JOIN $wpdb->postmeta m1 ON p.ID = m1.post_id
                    LEFT JOIN $wpdb->postmeta m2 ON p.ID = m2.post_id
                    LEFT JOIN $wpdb->postmeta m3 ON p.ID = m3.post_id
                    LEFT JOIN $wpdb->postmeta m4 ON p.ID = m4.post_id
                    LEFT JOIN $wpdb->postmeta m5 ON (p.ID = m5.post_id AND m5.meta_key = 'nes_start_time')
                    LEFT JOIN $wpdb->postmeta m6 ON p.ID = m6.post_id
                    WHERE p.post_type = 'nes_event'
                    AND p.post_status IN ('publish', 'past_events')
                    AND (tt.taxonomy = 'event_category' AND t.term_id IN ('$cat_str'))
                    AND (m1.meta_key = 'nes_event_date' AND (m1.meta_value >= '$query_date' AND m1.meta_value < '$query_end_date'))
                    AND (m2.meta_key = 'nes_event_type' AND m2.meta_value != 'offsite')
                    AND (m3.meta_key = 'nes_event_status' AND m3.meta_value = 'approved')
                    AND (m4.meta_key = 'nes_private_public' AND m4.meta_value = 'public')
                    AND (m6.meta_key = 'nes_venue_id' AND m6.meta_value IN ('$venue_id_str'))
                    ORDER BY date ASC, time ASC
                ";    

                // $args = array(
                //     'post_type' => 'nes_event',
                //     'post_status' => array('publish', 'past_events'),
                //     'posts_per_page' => -1,
                //     'tax_query' => array(
                //         array(
                //             'taxonomy' => 'event_category',
                //             'field' => 'term_id',
                //             'terms' => $categories
                //         )                    
                //     ),
                //     'meta_query' => array(
                //         'relation' => 'AND',
                //         array(
                //             'key' => 'nes_event_date',
                //             'value' => $query_date,
                //             'compare' => '>='
                //         ),
                //         array(
                //             'key' => 'nes_event_date',
                //             'value' => $query_end_date,
                //             'compare' => '<'
                //         ),
                //         array(
                //             'key' => 'nes_event_type',
                //             'value' => 'offsite',
                //             'compare' => '!='
                //         ),
                //         array(
                //             'key' => 'nes_event_status',
                //             'value' => 'approved',
                //             'compare' => '='
                //         ),
                //         array(
                //             'key' => 'nes_private_public',
                //             'value' => 'public',
                //             'compare' => '='
                //         ),
                //         array(
                //             'key' => 'nes_venue_id',
                //             'value' => $venue_ids,
                //             'compare' => 'IN'
                //         )
                //     )
                // );

                // $events = new WP_Query($args);
                // return $events;
            }           

            $event_ids = $wpdb->get_results($query_string, OBJECT);
            return $event_ids;
        }        

        function nes_get_featured_events($query_date, $venues = array(), $categories = array()){

            // set temp venue id variable
            $venue_ids = $venues;
            $offsite = false;
            $all_ids = array();
            global $wpdb;

            // remove offsite from venue array for later search
            if(is_array($venue_ids)){
                $index = array_search('offsite', $venue_ids);
                if($index !== false){
                    $offsite = true;
                    unset($venue_ids[$index]);
                }
            }

            // implode venues
            $venue_id_str = false;
            if($venue_ids){$venue_id_str = implode("','", $venue_ids);}            

            // implode cats
            $cat_str = false;
            if($categories){$cat_str = implode("','", $categories);}

            // make date range for list
            $query_end_date = date('Y-m-d',(strtotime('next month',strtotime($query_date))));
            $now = date('Y-m-d');

            $query_string = "";

            // check if we need to include offsite
            if($offsite){
                $query_string = "
                    SELECT DISTINCT p.ID, m1.meta_value AS 'date', m5.meta_value AS 'time'
                    FROM $wpdb->posts p
                    INNER JOIN $wpdb->term_relationships tr ON (p.ID = tr.object_id)
                    INNER JOIN $wpdb->term_taxonomy tt ON (tr.term_taxonomy_id = tt.term_taxonomy_id)
                    INNER JOIN $wpdb->terms t ON (t.term_id = tt.term_id)
                    LEFT JOIN $wpdb->postmeta m1 ON (p.ID = m1.post_id AND m1.meta_key = 'nes_event_date')
                    LEFT JOIN $wpdb->postmeta m2 ON (p.ID = m2.post_id AND m2.meta_key = 'nes_event_type')
                    LEFT JOIN $wpdb->postmeta m3 ON (p.ID = m3.post_id AND m3.meta_key = 'nes_event_status')
                    LEFT JOIN $wpdb->postmeta m4 ON (p.ID = m4.post_id AND m4.meta_key = 'nes_private_public')
                    LEFT JOIN $wpdb->postmeta m5 ON (p.ID = m5.post_id AND m5.meta_key = 'nes_start_time')
                    LEFT JOIN $wpdb->postmeta m7 ON (m7.post_id = p.ID AND m7.meta_key = 'nes_featured_event')
                    WHERE p.post_type = 'nes_event'
                    AND p.post_status = 'publish'
                    AND (tt.taxonomy = 'event_category' AND t.term_id IN ('$cat_str'))
                    AND m2.meta_value = 'offsite'
                    AND m3.meta_value = 'approved'
                    AND m4.meta_value = 'public'                    
                    AND (
                        (m7.meta_value = 'month' AND (m1.meta_value >= '$query_date' AND m1.meta_value < '$query_end_date'))
                        OR
                        (m7.meta_value = 'always' AND (m1.meta_value >= '$now'))
                    )
                    UNION
                ";
            }

            $query_string .= "
                SELECT DISTINCT p.ID, m1.meta_value AS 'date', m5.meta_value AS 'time'
                FROM $wpdb->posts p
                INNER JOIN $wpdb->term_relationships tr ON (p.ID = tr.object_id)
                INNER JOIN $wpdb->term_taxonomy tt ON (tr.term_taxonomy_id = tt.term_taxonomy_id)
                INNER JOIN $wpdb->terms t ON (t.term_id = tt.term_id)
                LEFT JOIN $wpdb->postmeta m1 ON (p.ID = m1.post_id AND m1.meta_key = 'nes_event_date')
                LEFT JOIN $wpdb->postmeta m2 ON (p.ID = m2.post_id AND m2.meta_key = 'nes_event_type')
                LEFT JOIN $wpdb->postmeta m3 ON (p.ID = m3.post_id AND m3.meta_key = 'nes_event_status')
                LEFT JOIN $wpdb->postmeta m4 ON (p.ID = m4.post_id AND m4.meta_key = 'nes_private_public')
                LEFT JOIN $wpdb->postmeta m5 ON (p.ID = m5.post_id AND m5.meta_key = 'nes_start_time')
                LEFT JOIN $wpdb->postmeta m6 ON (p.ID = m6.post_id AND m6.meta_key = 'nes_venue_id')
                LEFT JOIN $wpdb->postmeta m7 ON (m7.post_id = p.ID AND m7.meta_key = 'nes_featured_event')
                WHERE p.post_type = 'nes_event'
                AND p.post_status IN ('publish', 'past_events')
                AND (tt.taxonomy = 'event_category' AND t.term_id IN ('$cat_str'))
                AND m2.meta_value != 'offsite'
                AND m3.meta_value = 'approved'
                AND m4.meta_value = 'public'
                AND m6.meta_value IN ('$venue_id_str')                    
                AND (
                    (m7.meta_value = 'month' AND (m1.meta_value >= '$query_date' AND m1.meta_value < '$query_end_date'))
                    OR
                    (m7.meta_value = 'always' AND (m1.meta_value >= '$now'))
                )
                GROUP BY p.ID
                ORDER BY date ASC, time ASC
            ";    
        
            $event_ids = $wpdb->get_results($query_string, OBJECT);
            return $event_ids;
        }


        // ajax call from reservation table click
        function nes_ajax_make_event_form($venue_id = ''){
            ?>
            <form id="nes-event-form" method="post" action="<?php echo $this->nes_settings['dir'] . 'inc/nes-save-submitted-event.php';?>">
                <?php 
                    // venue_id - location_id - year - month - day - time
                    //array(4) { 
                    //     [0]=> string(23) "260-262-2014-10-31-1515" 
                    //     [1]=> string(23) "260-262-2014-10-31-1530" 
                    //     [2]=> string(23) "260-262-2014-10-31-1545" 
                    //     [3]=> string(23) "260-262-2014-10-31-1600" 
                    // }
        
                    $location_ids = array();
                    $start_time = 9999;
                    $end_time = 0;
                    $selected_time = '';
                    if(isset($_POST['selected'])){
                        $selected_time = sanitize_text_field($_POST['selected']);
                        $data = explode('-', $selected_time);
                        $venue_id = $data[0];
                        $location_id = $data[1];
                        $year = $data[2];
                        $month = $data[3];
                        $day = $data[4];
                        $date = $data[2] . '-' . $data[3] . '-' . $data[4];
                        $start_time = $data[5];
                    }

                    // get location latest event end time             
                    $availability_type = get_post_meta($location_id, 'nes_venue_availability', true);      
                    if($availability_type == 'venue'){
                        $location_end = get_post_meta($venue_id, 'nes_venue_'.strtolower(date('l',strtotime($date))).'_end', true);               
                    }else{
                        $location_end = get_post_meta($location_id, 'nes_location_'.strtolower(date('l',strtotime($date))).'_end', true);               
                    }          
                       
                ?>    

                <div class="nes-event-form-wrap">

                    <div class="nes-event-date-time-section">
                        <?php // this is onsite?>
                        <input type="hidden" name="nes_event_type" value="onsite">

                        <div class="nes-event-date">
                            <label><?php _e('Event Date','nes'); ?></label>
                            <input type="text" name="nes_event_date" value="<?php echo date('m/d/Y', strtotime($date)); ?>" readonly />
                            <input type="hidden" name="nes_event_date_formatted" value="<?php echo $date; ?>" />
                        </div>

                        <?php if(is_array($this->nes_settings['show_form_fields']) && in_array('venue', $this->nes_settings['show_form_fields'])) : ?>
                            <div class="nes-venue">
                                <label><?php echo $this->nes_settings['venue_single']; ?>:</label>
                                <input type="text" value="<?php echo get_the_title($venue_id); ?>" readonly />
                            </div>
                        <?php endif; ?>
                        <input type="hidden" name="nes_venue_id" value="<?php echo $venue_id; ?>" />
                        
                        <?php if(is_array($this->nes_settings['show_form_fields']) && in_array('location', $this->nes_settings['show_form_fields'])) : ?>
                            <div class="nes-locations">
                                <label><?php echo $this->nes_settings['location_plural']?>:</label> 

                                <?php $locations = $this->nes_get_venue_locations($venue_id); ?>
                                <?php if($locations) : ?>
                                    <select id="nes-location-id" name="nes_location_id[]" multiple >
                                    <option></option>
                                    <?php foreach($locations as $location) : ?>
                                        <option value="<?php echo $location->ID; ?>" 
                                            <?php if($location_id == $location->ID){echo 'selected';} ?>
                                        ><?php echo get_the_title($location->ID); ?></option>
                                    <?php endforeach; ?>
                                    </select>
                                <?php endif; ?>

                            </div>
                        <?php else : ?>
                            <input type="hidden" name="nes_location_id[]" value="<?php echo $location_id; ?>" />
                        <?php endif; ?>

                        <span class="nes-clearer"></span>

                        <?php $setup_cleanup = $this->nes_settings['setup_cleanup']; ?>
                        <div class="nes-start">
                            <label><strong><?php printf(__('Start %1$s at', 'nes'),$this->nes_settings['event_single']); ?>:</strong></label>
                            <input type="text" name="nes_start_time_formatted" value="<?php echo date('g:i a', strtotime($start_time)); ?>" readonly />
                            <input type="hidden" name="nes_start_time" value="<?php echo date('H:i', strtotime($start_time)); ?>" />
                            
                            <?php if(is_array($this->nes_settings['show_form_fields']) && in_array('setup_time', $this->nes_settings['show_form_fields'])) : ?>
                                <div class="nes-setup-time" <?php if($setup_cleanup == 'no'){echo 'style="display:none;"';}?>>
                                    <label><?php printf(__('Do you need setup time before your %1$s begins','nes'),$this->nes_settings['event_single']); ?>?</label> 
                                    <select name="nes_need_setup_time">
                                        <option value="no"><?php _e('No','nes'); ?></option>
                                        <option value="yes"><?php _e('Yes','nes'); ?></option>
                                    </select>
                                    
                                    <div id="nes-setup-time" style="display:none;">
                                        <label><?php printf(__('Start Setup before %1$s at','nes'),$this->nes_settings['event_single']); ?>:</label> 
                                        <?php $start_setup = date('H:i', strtotime("-15 minutes", strtotime($start_time))); ?>
                                        <?php echo $this->nes_get_time_select('nes_start_setup_time',null,null,false,false,$start_setup,0.25,true); ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="nes-end">
                            <?php if($this->nes_settings['end_time_type'] == "standard") : ?>
                                <label><strong><?php printf(__('End %1$s at','nes'),$this->nes_settings['event_single']); ?>:</strong></label> 
                                <?php $end_start = date('H:i',strtotime("+ 15 minutes", strtotime($start_time))); ?>
                                <?php echo $this->nes_get_time_select('nes_end_time', $end_start, null, true, $end_start); ?>
                                
                            <?php elseif($this->nes_settings['end_time_type'] == "fixed") : ?>
                                <label><strong><?php printf(__('End %1$s at','nes'),$this->nes_settings['event_single']); ?>:</strong></label>
                                <?php $end_time_temp = strtotime("+".$this->nes_settings['end_time_length_hr']." hours", strtotime($start_time)); ?>
                                <?php $end_time = date('H:i',strtotime("+".$this->nes_settings['end_time_length_min']." minutes", $end_time_temp)); ?>
                                <?php echo $this->nes_get_time_select('nes_end_time', $end_time, null, true, $end_time, $end_time); ?>
                                
                            <?php else : // min/max ?>
                                <label><strong><?php printf(__('End %1$s at','nes'),$this->nes_settings['event_single']); ?>:</strong></label>
                                <?php 
                                    $start_time_temp = strtotime("+".$this->nes_settings['end_time_min_length_hr']." hours", strtotime($start_time));
                                    $start_time_min = date('H:i',strtotime("+".$this->nes_settings['end_time_min_length_min']." minutes", $start_time_temp));
                                                  
                                    $end_time_temp = strtotime("+".$this->nes_settings['end_time_max_length_hr']." hours", strtotime($start_time));
                                    
                                    // check if end time is past venue/location end
                                    if((date('H:i', $end_time_temp) <= $location_end) && (date('H:i', $end_time_temp) > $start_time_min)){
                                        $end_time_max = date('H:i',strtotime("+".$this->nes_settings['end_time_max_length_min']." minutes", $end_time_temp));      
                                    }else{
                                        $end_time_max = $location_end;
                                    }
                                    $interval = $this->nes_settings['end_time_minmax_interval'];
                                ?>
                                <?php echo $this->nes_get_time_select('nes_end_time', $start_time_min, null, true, $start_time_min, $end_time_max, $interval); ?>
                            <?php endif; ?>
                            <?php if(is_array($this->nes_settings['show_form_fields']) && in_array('cleanup_time', $this->nes_settings['show_form_fields'])) : ?>
                                <div class="nes-cleanup-time" <?php if($setup_cleanup == 'no'){echo 'style="display:none;"';}?>>
                                    <label><?php printf(__('Do you need cleanup time after your %1$s ends','nes'),$this->nes_settings['event_single']); ?>?</label>
                                    <select name="nes_need_cleanup_time">
                                        <option value="no"><?php _e('No','nes'); ?></option>
                                        <option value="yes"><?php _e('Yes','nes'); ?></option>
                                    </select>
                                    <div id="nes-cleanup-time" style="display:none;">
                                        <label><?php printf(__('End Cleanup after %1$s at','nes'),$this->nes_settings['event_single']); ?>:</label> 
                                        <?php $end_cleanup = date('H:i',strtotime("+ 30 minutes", strtotime($start_time))); ?>
                                        <?php echo $this->nes_get_time_select('nes_end_cleanup_time',$end_cleanup,null,false,$end_cleanup); ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                        
                        <span class="nes-clearer"></span>


                        <div class="nes-frequency-type">
                            <label><?php echo $this->nes_settings['event_single']; ?> frequency</label>
                            <label><input type="radio" name="nes_frequency_type" value="one" checked="checked"/> One Time&nbsp;&nbsp;</label> 
                            <label><input type="radio" name="nes_frequency_type" value="daily" /> Daily&nbsp;&nbsp;</label> 
                            <label><input type="radio" name="nes_frequency_type" value="weekly" /> Weekly&nbsp;&nbsp;</label> 
                            <label><input type="radio" name="nes_frequency_type" value="monthly" /> Monthly</label>
                            <label><input type="radio" name="nes_frequency_type" value="custom" /> Custom</label>
                        </div>

                        <div class="nes-series-start-end" style="display:none;">        
                            <div class="nes-frequency-end-date" style="display:none;">
                                <label>Series End Date</label> <input id="nes-frequency-end-datepicker" type="text" value="" />
                                <input id="nes-frequency-end-date" name="nes_frequency_end_date" type="hidden" value="" />
                            </div>
                            <div class="nes-custom-datepicker-wrap" style="display:none;">
                                <div id="nes-custom-datepicker"></div>
                                <div class="nes-custom-dates">
                                    <label>Series Dates</label> 
                                    <div class="nes-custom-date-display"></div>
                                    <input id="nes-custom-date" name="nes_custom_date" type="hidden" value="" />
                                </div>
                            </div>
                        </div>                                                   

                        <div class="nes-frequency" style="display:none;">  
                            <div class="nes-frequency-monthly">
                                <label>On the</label>
                                <select  name="nes_frequency_monthly">
                                    <?php $days = array('First','Second','Third','Fourth','Last'); ?>
                                    <option value="">&mdash;</option>
                                    <?php foreach($days as $day) : ?>
                                        <option value="<?php echo strtolower($day); ?>"><?php echo $day; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>  
                            <div class="nes-frequency-weekly">
                                <?php $days = array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'); ?>
                                <?php foreach($days as $day) : ?>
                                    <label><input type="checkbox" name="nes_frequency_weekly[]" value="<?php echo strtolower($day); ?>" /> <?php echo $day; ?>&nbsp;&nbsp;</label> 
                                <?php endforeach; ?>
                            </div>  
                        </div>

                        <span class="nes-clearer"></span>
                        <div class="nes-availability-wrap"></div>
                        <button class="nes-check-availability button">Check Availability <i style="display:none;" class="fas fa-spinner fa-pulse"></i></button>
                    </div>

                    <span class="nes-clearer"></span>

                    <div class="nes-event-info" style="display:none;">
                        <h6>STEP 2: <?php echo $this->nes_settings['event_single']; ?> Information</h6>

                        <?php if(is_array($this->nes_settings['show_form_fields']) && in_array('title', $this->nes_settings['show_form_fields'])) : ?>
                            <div class="nes-event-title">
                                <label><strong>
                                    <?php if(empty($this->nes_settings['title_label'])) : ?>
                                        <?php echo $this->nes_settings['event_single']; ?> Title:
                                    <?php else :  ?>
                                        <?php echo $this->nes_settings['title_label']; ?>
                                    <?php endif; ?>
                                </strong></label>
                                <input type="text" name="nes_event_title" value="<?php if(isset($_SESSION['nes']['event_title'])){echo stripslashes(sanitize_text_field($_SESSION['nes']['event_title']));} ?>"  />
                            </div>
                        <?php endif; ?>
                    
                        <div class="nes-your-name">
                            <label><strong>
                                <?php if(empty($this->nes_settings['name_label'])) : ?>
                                    <?php _e('Your Name','nes'); ?>:
                                <?php else :  ?>
                                    <?php echo $this->nes_settings['name_label']; ?>
                                <?php endif; ?>
                            </strong></label>
                            <input type="text" name="nes_event_name" value="<?php if(isset($_SESSION['nes']['event_name'])){echo stripslashes(sanitize_text_field($_SESSION['nes']['event_name']));} ?>"  />
                        </div>

                        <div class="nes-phone">
                            <label><strong>
                                <?php if(empty($this->nes_settings['phone_label'])) : ?>
                                    <?php _e('Phone','nes'); ?>:
                                <?php else :  ?>
                                    <?php echo $this->nes_settings['phone_label']; ?>
                                <?php endif; ?>
                            </strong></label>
                            <input type="tel" class="nes-tel-input" name="nes_event_phone" value="<?php if(isset($_SESSION['nes']['event_phone'])){echo stripslashes(sanitize_text_field($_SESSION['nes']['event_phone']));} ?>"  />
                        </div>

                        <div class="nes-email">
                            <label><strong>
                                <?php if(empty($this->nes_settings['email_label'])) : ?>
                                    <?php _e('Email','nes'); ?>:
                                <?php else :  ?>
                                    <?php echo $this->nes_settings['email_label']; ?>
                                <?php endif; ?>
                            </strong></label>
                            <input type="email" name="nes_event_email" value="<?php if(isset($_SESSION['nes']['event_email'])){echo stripslashes(sanitize_text_field($_SESSION['nes']['event_email']));} ?>"  />
                            <span class="nes-clearer"></span>
                        </div>

                        <?php if(is_array($this->nes_settings['show_form_fields']) && in_array('website', $this->nes_settings['show_form_fields'])) : ?>
                            <div class="nes-website">
                                <label><strong>
                                    <?php if(empty($this->nes_settings['website_label'])) : ?>
                                        <?php _e('Website','nes'); ?>:
                                    <?php else :  ?>
                                        <?php echo $this->nes_settings['website_label']; ?>
                                    <?php endif; ?>
                                </strong></label>
                                <input type="text" name="nes_event_website" value="<?php if(isset($_SESSION['nes']['event_website'])){echo stripslashes(sanitize_text_field($_SESSION['nes']['event_website']));} ?>" />
                            </div>
                        <?php endif; ?>

                        <div class="nes-private-public">
                            <label><?php echo $this->nes_settings['event_single']; ?> visibility:</label>
                            <label><input type="radio" name="nes_private_public" value="public" checked="checked" /> Public (show on calendar)</label>
                            <label><input type="radio" name="nes_private_public" value="private" /> Private</label>
                        </div>

                        <?php if(is_array($this->nes_settings['show_form_fields']) && in_array('type', $this->nes_settings['show_form_fields'])) : ?>
                            <div class="nes-send-to">
                                <label><strong>
                                    <?php if(empty($this->nes_settings['event_type_label'])) : ?>
                                         <?php printf(__('%1$s Type','nes'),$this->nes_settings['event_single']); ?>:
                                    <?php else :  ?>
                                        <?php echo $this->nes_settings['event_type_label']; ?>
                                    <?php endif; ?>
                                </strong></label>
                                <select class="event-send-to" name="nes_event_send_to" >
                                    <?php if($this->nes_settings['admin_email_two'] == '') : ?>
                                        <option value="one"><?php echo $this->nes_settings['admin_email_label_one']; ?></option>
                                    <?php else : ?>
                                        <option value="">- <?php _e('Choose One','nes'); ?> -</option>
                                        <option value="one"><?php echo $this->nes_settings['admin_email_label_one']; ?></option>
                                        <option value="two"><?php echo $this->nes_settings['admin_email_label_two']; ?></option>
                                    <?php endif; ?>
                                </select>
                            </div>
                        <?php else : ?>
                            <input class="event-send-to" type="hidden" name="nes_event_send_to" value="one" />
                        <?php endif; ?>

                        <span class="nes-clearer"></span>

                        <?php if(is_array($this->nes_settings['show_form_fields']) && in_array('description', $this->nes_settings['show_form_fields'])) : ?>
                            <div class="nes-event-description">
                                <label><strong>
                                    <?php if(empty($this->nes_settings['description_label'])) : ?>
                                        <?php printf(__('%1$s Description','nes'),$this->nes_settings['event_single']); ?>
                                    <?php else :  ?>
                                        <?php echo $this->nes_settings['description_label']; ?>
                                    <?php endif; ?>
                                </strong></label>
                                <textarea name="nes_event_content" style="width:100%;" rows="6"><?php if(isset($_SESSION['nes']['event_content'])){echo stripslashes(sanitize_text_field($_SESSION['nes']['event_content']));} ?></textarea>
                            </div>
                        <?php endif; ?>

    				    <span class="nes-clearer"></span>

                        <?php if(is_array($this->nes_settings['show_form_fields']) && in_array('setup_needs', $this->nes_settings['show_form_fields'])) : ?>
                            <div class="nes-setup-needs">
                                <label><strong>
                                    <?php if(empty($this->nes_settings['setup_needs_label'])) : ?>
                                        <?php _e('Setup Needs','nes'); ?>:
                                    <?php else :  ?>
                                        <?php echo $this->nes_settings['setup_needs_label']; ?>
                                    <?php endif; ?>
                                </strong></label>
                                <textarea name="nes_event_setup" style="width:100%;" rows="6"><?php if(isset($_SESSION['nes']['event_setup'])){echo stripslashes(sanitize_text_field($_SESSION['nes']['event_setup']));} ?></textarea>
                            </div>
                        <?php endif; ?>


                        <?php if(is_array($this->nes_settings['show_form_fields']) && in_array('av_needs', $this->nes_settings['show_form_fields'])) : ?>
                            <div class="nes-av-needs">
                                <label><strong>
                                    <?php if(empty($this->nes_settings['av_needs_label'])) : ?>
                                        <?php _e('A/V Tech Needs:','nes'); ?>
                                    <?php else :  ?>
                                        <?php echo $this->nes_settings['av_needs_label']; ?>
                                    <?php endif; ?>
                                </strong></label>
                                <textarea name="nes_event_av" style="width:100%;" rows="6"><?php if(isset($_SESSION['nes']['event_av'])){echo stripslashes(sanitize_text_field($_SESSION['nes']['event_av']));} ?></textarea>
                            </div>
                        <?php endif; ?>
    				
                        <span class="nes-clearer"></span> 
                        <div class="nes-sending-message" style="display:none;">Sending request... Please do not refresh this page <i class="fas fa-spinner fa-pulse"></i></div>
                        <div class="nes-event-submitted">
                            <input type="hidden" name="nes_date" value="<?php echo $date; ?>" />
                            <button id="nes-event-submitted" class="button" name="nes_event_submitted" value="1">Submit <?php echo $this->nes_settings['event_single']; ?> Request</button>
                        </div>
                        <span class="nes-clearer"></span>
                    </div>
                    <span class="nes-clearer"></span>
                </div>
                <span class="nes-clearer"></span>
            </form>

            <div class="nes-missing-fields nes-dialog" title="VALIDATION FAILED" style="display:none;">
                <h4>Please review the highlighted fields below:</h4>
                <ul class="nes-error-list"><li></li></ul>
            </div>

            <script type="text/javascript">
                jQuery(document).ready(function($){

                    // jquery datepickers       
                    $('#nes-frequency-end-datepicker').datepicker({
                        dateFormat: "mm/dd/yy",
                        altField: "#nes-frequency-end-date",
                        altFormat: "yy-mm-dd",
                        showOtherMonths: true,
                        minDate: 0,
                        showOn: "both", 
                        buttonText: "<i class='far fa-calendar'></i>",
                        onSelect: function(selectedDate){
                            $('#nes-frequency-end-date').trigger('change');
                        }
                    });                    

                    // multidatepicker
                    $('#nes-custom-datepicker').multiDatesPicker({
                        dateFormat: "mm/dd/yy",
                        altField: "#nes-custom-date",
                        altFormat: "yy-mm-dd",
                        showOtherMonths: true,
                        minDate: 0,
                        addDates: ["<?php echo date('m/d/Y', strtotime($date)); ?>"],
                        defaultDate: "<?php echo date('m/d/Y', strtotime($date)); ?>",
                        onSelect: function(selectedDate){
                            $('#nes-custom-date').trigger('change');
                            $('.nes-custom-date-display').html($('#nes-custom-date').val());
                        }
                    });
                    $('.nes-custom-date-display').html($('#nes-custom-date').val());


                    // locations
                    $('#nes-location-id').chosen({
                        placeholder_text_multiple: "<?php printf(__('Select some %1$s','nes'),$this->nes_settings['location_plural']); ?>",
                        width:"100%"
                    });

                    // toogle setup/cleanup times
                    $('select[name="nes_need_setup_time"]').on('change',function(){
                        $('#nes-setup-time').slideToggle('fast');
                    });

                    $('select[name="nes_need_cleanup_time"]').on('change',function(){
                        $('#nes-cleanup-time').slideToggle('fast');
                    });

                    // refresh cleanup time select on end time change
                    $('select[name="nes_end_time"]').on('change',function(){
                        var data = {
                            'action': 'nes_ajax_get_time_select',
                            'name': 'nes_end_cleanup_time',
                            'start': $(this).val()
                        };
                        // add spinner
                        $('#nes-cleanup-time select').replaceWith('<i class="fas fa-spinner fa-pulse"></i>');

                        $.post("<?php echo NES_AJAX_URL; ?>", data, function(response){
                            $('#nes-cleanup-time i').replaceWith(response);
                        }); 
                    });

                    // toggle frequency type
                    $('input[name="nes_frequency_type"]').on('change',function(){                        

                        if($(this).val() != 'one'){
                            $('.nes-frequency').fadeIn('fast');
                        }else{
                            $('.nes-frequency').hide();
                        }

                        if($(this).val() == 'one'){
                            $('.nes-series-start-end').hide();
                            $('.nes-frequency-weekly').hide();
                            $('.nes-frequency-monthly').hide();
                        }else if($(this).val() == 'daily'){
                            $('.nes-series-start-end').fadeIn('fast');
                            $('.nes-series-start-end').removeClass('nes-custom');
                            $('.nes-frequency-end-date').fadeIn('fast');
                            $('.nes-custom-datepicker-wrap').hide();
                            $('.nes-frequency-weekly').hide();
                            $('.nes-frequency-monthly').hide();
                        }else if($(this).val() == 'weekly'){
                            $('.nes-series-start-end').fadeIn('fast');
                            $('.nes-series-start-end').removeClass('nes-custom');
                            $('.nes-frequency-end-date').fadeIn('fast');
                            $('.nes-custom-datepicker-wrap').hide();
                            $('.nes-frequency-weekly').fadeIn('fast');
                            $('.nes-frequency-monthly').hide();
                        }else if($(this).val() == 'monthly'){
                            $('.nes-series-start-end').fadeIn('fast');
                            $('.nes-series-start-end').removeClass('nes-custom');
                            $('.nes-custom-datepicker-wrap').hide();
                            $('.nes-frequency-end-date').fadeIn('fast');
                            $('.nes-frequency-weekly').fadeIn('fast');
                            $('.nes-frequency-monthly').fadeIn('fast');

                            if($('select[name=nes_frequency_monthly]').val()){
                                $('.nes-frequency-weekly').fadeIn('fast');
                            }else{
                                $('.nes-frequency-weekly').hide();
                            }
                        }else if($(this).val() == 'custom'){
                            $('.nes-series-start-end').fadeIn('fast');
                            $('.nes-series-start-end').addClass('nes-custom');
                            $('.nes-frequency-end-date').hide();
                            $('.nes-custom-datepicker-wrap').fadeIn('fast');
                            $('.nes-frequency-weekly').hide();
                            $('.nes-frequency-monthly').hide();
                        }
                    });  

                    // toggle weekly if monthly is empty
                    $('select[name=nes_frequency_monthly]').on('change',function(){
                        if($(this).val()){
                            $('.nes-frequency-weekly').fadeIn('fast');
                        }else{
                            $('.nes-frequency-weekly').hide();
                        }
                    });

                    // toggle the form if any of the criteria has changed
                    $(document).on('change', 'select[name="nes_location_id[]"], input[name="nes_custom_date"], select[name="nes_end_time"], input[name="nes_frequency_type"], select[name="nes_need_setup_time"], select[name="nes_need_cleanup_time"], #nes-frequency-end-date, select[name="nes_frequency_monthly"], input[name="nes_frequency_weekly[]"]',function(){
                        $('.nes-event-info').hide();
                        $('.nes-check-availability i').hide();
                        $('.nes-check-availability').fadeIn('fast');
                    });

                    // ajax call to check availability
                    $('.nes-check-availability').on('click',function(e){
                        e.preventDefault();
                        $('i', this).show();
                        
                        // get weekly checkbox values into array
                        var weekly_vals = new Array();
                        $("input[name='nes_frequency_weekly[]']:checked").each(function(){
                            weekly_vals.push($(this).val());
                        });

                        // build postdata to send
                        var data = {
                            'action': 'nes_ajax_check_availability',
                            'venue': $('input[name=nes_venue_id]').val(),
                            'locations': $('#nes-location-id').val(),
                            'date': $('input[name=nes_event_date_formatted]').val(),
                            'custom_dates': $('input[name=nes_custom_date]').val(),
                            'need_setup_time': $('select[name=nes_need_setup_time]').val(),
                            'setup_time': $('select[name=nes_start_setup_time]').val(),
                            'start_time': $('input[name=nes_start_time]').val(),
                            'end_time': $('select[name=nes_end_time]').val(),
                            'need_cleanup_time': $('select[name=nes_need_cleanup_time]').val(),
                            'cleanup_time': $('select[name=nes_end_cleanup_time]').val(),
                            'frequency_type': $('input[name=nes_frequency_type]:checked').val(),
                            'frequency_end_date': $('input[name=nes_frequency_end_date]').val(),
                            'frequency_weekly': weekly_vals,
                            'frequency_monthly': $('select[name=nes_frequency_monthly]').val()
                        };            

                        $.post("<?php echo NES_AJAX_URL; ?>", data, function(response){
                           
                            // hide loading icon and clear availability wrap
                            $('.nes-check-availability i').hide();
                            $('.nes-availability-wrap').html('').removeClass('nes-checked');;

                            // only show form if there is something available
                            if(/error/i.test(response) && /success/i.test(response)){
                                $('.nes-availability-wrap').addClass('nes-checked');
                                $('.nes-event-info').fadeIn('fast');
                                $('.nes-check-availability').hide();
                            }else if(/error/i.test(response)){
                                $('.nes-availability-wrap').addClass('nes-checked');
                                $('.nes-event-info').fadeOut('fast');
                            }else{
                                $('.nes-availability-wrap').addClass('nes-checked');
                                $('.nes-event-info').fadeIn('fast');
                                $('.nes-check-availability').hide();
                            }

                            // populate response
                            $('.nes-availability-wrap').html(response);
                        }); 
                    });

                    // inititialize and include validation script
                    $('.nes-tel-input').intlTelInput({
                        utilsScript: "<?php echo $this->nes_settings['dir']; ?>/js/intl-tel-input/build/js/utils.js",
                        preferredCountries: ['us'],
                        initialCountry: 'us'
                    });

                    ////////// VALIDATION //////////////////
                    $(document).on('click', '#nes-event-submitted', function(e){

                        // clear previous errors
                        $('.nes-error-list').empty();
                        $('.nes-required-field').each(function(){
                            $(this).removeClass('nes-required-field');
                        });

                        // check for missing fields
                        var errors = new Array();

                        // event time
                        // if(!$('input[name=nes_start_time]').val()){
                        //     $('input[name=nes_start_time]').addClass('nes-required-field');
                        //     errors.push("<?php echo $this->nes_settings['event_single']; ?>" + ' Start Time');
                        // }
                        if(!$('select[name=nes_end_time]').val()){
                            $('select[name=nes_end_time]').addClass('nes-required-field');
                            errors.push("<?php echo $this->nes_settings['event_single']; ?>" + ' End Time');
                        }

                        var setup = $('select[name=nes_start_setup_time]').val();
                        var start = $('input[name=nes_start_time]').val();
                        var end = $('select[name=nes_end_time]').val();
                        var cleanup = $('select[name=nes_end_cleanup_time]').val();

                        if(setup.length && setup > start){
                            $('select[name=nes_start_setup_time]').addClass('nes-required-field');
                            errors.push('Conflicting Setup Time');
                        }
                        if(start >= end){
                            $('input[name=nes_start_time]').addClass('nes-required-field');
                            errors.push('Conflicting Start Time');
                        }            
                        if(end <= start){
                            $('select[name=nes_end_time]').addClass('nes-required-field');
                            errors.push('Conflicting End Time');
                        }                
                        if(cleanup.length && end > cleanup){
                            $('select[name=nes_end_cleanup_time]').addClass('nes-required-field');
                            errors.push('Conflicting Cleanup Time');
                        }            

                        // recurring event fields
                        if($('input[name=nes_frequency_type]:checked').val() != 'one' && $('input[name=nes_frequency_type]:checked').val() != 'custom'){           
                            if(!$('input[name=nes_frequency_end_date]').val()){
                                $('#nes-frequency-end-datepicker').addClass('nes-required-field');
                                errors.push('Series End Date');
                            }
                        }

                        // event information fields
                        if(!$('input[name=nes_event_title]').val()){
                            $('input[name=nes_event_title]').addClass('nes-required-field');
                            errors.push("<?php echo $this->nes_settings['event_single']; ?>" + ' Title');
                        }                        
                        if(!$('input[name=nes_event_name]').val()){
                            $('input[name=nes_event_name]').addClass('nes-required-field');
                            errors.push('Your Name');
                        }                        
                        if(!$('input[name=nes_event_email]').val() || !nes_validate_email($('input[name=nes_event_email]').val())){
                            $('input[name=nes_event_email]').addClass('nes-required-field');
                            errors.push('Email Address');
                        }     

                        // tel field validate
                        if($.trim($('.nes-tel-input').val())){
                            if(!$('.nes-tel-input').intlTelInput('isValidNumber')){
                                $('.nes-tel-input').addClass("nes-required-field");
                                errors.push('Phone Number');
                            }
                        }      

                        if(!$('.event-send-to').val()){
                            $('.event-send-to').addClass('nes-required-field');
                            errors.push('Event Type');
                        }

                        // generate validation error popup
                        if(errors.length > 0){
                            e.preventDefault();
                            $.each(errors, function(key, value){
                                $('.nes-error-list').append('<li>'+ value + '</li>');
                            });
                            $('.nes-missing-fields').dialog({
                                width:400,
                                modal:true,
                                buttons: [{
                                    text: "OK",
                                    click: function(){
                                        $(this).dialog("close");
                                    }
                                }]
                            });
                        }else{
                            $(this).hide();
                            $('.nes-sending-message').show();
                        }
                    }); 

                    function nes_validate_email(email){
                        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                        return re.test(email);
                    } 

                });
            </script>
            <?php
            die();
        }

        // make offsite event form
        function nes_make_offsite_event_form($date){
            ?>
            <form id="nes-event-form" class="nes-offsite-form" method="post" action="<?php echo $this->nes_settings['dir'] . 'inc/nes-save-submitted-event.php';?>">
                <div class="nes-event-form-wrap">

                    <div class="nes-event-date-time-section">
                        
                        <?php // date selected for submission ?>
                        <input type="hidden" name="nes_event_date" value="<?php echo date('m/d/Y', strtotime($date)); ?>" />
                        <input type="hidden" name="nes_event_date_formatted" value="<?php echo $date; ?>" />

                        <?php // get offsite venue name ?>
                        <div class="nes-venue">
                            <label><?php echo $this->nes_settings['venue_single']; ?> Name:</label>
                            <input type="text" name="nes_offsite_venue_name" value="<?php if(isset($_SESSION['nes']['offsite_venue_name'])){echo stripslashes(sanitize_text_field($_SESSION['nes']['offsite_venue_name']));} ?>" />
                            <input type="hidden" name="nes_venue_id" value="" />
                        </div>
                        
                        <?php // get offsite venue address ?>
                        <div class="nes-locations">
                            <label><?php echo $this->nes_settings['venue_single']?> Address:</label> 
                            <input type="text" name="nes_offsite_venue_address" value="<?php if(isset($_SESSION['nes']['offsite_venue_address'])){echo stripslashes(sanitize_text_field($_SESSION['nes']['offsite_venue_address']));} ?>" />
                            <input type="hidden" name="nes_location_id[]" value="" />
                        </div>

                        <?php // this is offsite ?>
                        <input type="hidden" name="nes_event_type" value="offsite">

                        <span class="nes-clearer"></span>

                        <?php // setup not necessary ?>
                        <div class="nes-start">
                            <label><strong><?php printf(__('Start %1$s at', 'nes'),$this->nes_settings['event_single']); ?>:</strong></label>
                            <?php echo $this->nes_get_time_select('nes_start_time', '12:00'); ?>
                            <input type="hidden" name="nes_start_setup_time" value="" />
                        </div>

                        <?php // end intervals and cleanup time not necessary ?>
                        <div class="nes-end">
                            <label><strong><?php printf(__('End %1$s at','nes'),$this->nes_settings['event_single']); ?>:</strong></label> 
                            <?php echo $this->nes_get_time_select('nes_end_time', '12:00'); ?>
                            <input type="hidden" name="nes_end_cleanup_time" value="" />
                        </div>
                        
                        <span class="nes-clearer"></span>

                        <div class="nes-frequency-type">
                            <label><?php echo $this->nes_settings['event_single']; ?> Frequency</label>
                            <label><input type="radio" name="nes_frequency_type" value="one" checked="checked"/> One Time&nbsp;&nbsp;</label> 
                            <label><input type="radio" name="nes_frequency_type" value="daily" /> Daily&nbsp;&nbsp;</label> 
                            <label><input type="radio" name="nes_frequency_type" value="weekly" /> Weekly&nbsp;&nbsp;</label> 
                            <label><input type="radio" name="nes_frequency_type" value="monthly" /> Monthly</label>
                            <label><input type="radio" name="nes_frequency_type" value="custom" /> Custom</label>
                        </div>

                        <div class="nes-series-start-end" style="display:none;">        
                            <div class="nes-frequency-end-date" style="display:none;">
                                <label>Series End Date</label> <input id="nes-frequency-end-datepicker" type="text" value="" />
                                <input id="nes-frequency-end-date" name="nes_frequency_end_date" type="hidden" value="" />
                            </div>
                            <div class="nes-custom-datepicker-wrap" style="display:none;">
                                <div id="nes-custom-datepicker"></div>
                                <div class="nes-custom-dates">
                                    <label>Series Dates</label> 
                                    <div class="nes-custom-date-display"></div>
                                    <input id="nes-custom-date" name="nes_custom_date" type="hidden" value="" />
                                </div>
                            </div>
                        </div>                          

                        <div class="nes-frequency" style="display:none";>  
                            <div class="nes-frequency-monthly">
                                <label>On the</label>
                                <select  name="nes_frequency_monthly">
                                    <?php $days = array('First','Second','Third','Fourth','Last'); ?>
                                    <option value="">&mdash;</option>
                                    <?php foreach($days as $day) : ?>
                                        <option value="<?php echo strtolower($day); ?>"><?php echo $day; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>  
                            <div class="nes-frequency-weekly">
                                <?php $days = array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'); ?>
                                <?php foreach($days as $day) : ?>
                                    <label><input type="checkbox" name="nes_frequency_weekly[]" value="<?php echo strtolower($day); ?>" /> <?php echo $day; ?>&nbsp;&nbsp;</label> 
                                <?php endforeach; ?>
                            </div>  
                        </div>

                        <span class="nes-clearer"></span>
                    </div>

                    <span class="nes-clearer"></span>

                    <div class="nes-event-info">
                        <?php if(is_array($this->nes_settings['show_form_fields']) && in_array('title', $this->nes_settings['show_form_fields'])) : ?>
                            <div class="nes-event-title">
                                <label><strong>
                                    <?php if(empty($this->nes_settings['title_label'])) : ?>
                                        <?php echo $this->nes_settings['event_single']; ?> Title:
                                    <?php else :  ?>
                                        <?php echo $this->nes_settings['title_label']; ?>
                                    <?php endif; ?>
                                </strong></label>
                                <input type="text" name="nes_event_title" value="<?php if(isset($_SESSION['nes']['event_title'])){echo stripslashes(sanitize_text_field($_SESSION['nes']['event_title']));} ?>" />
                            </div>
                        <?php endif; ?>
                    
                        <div class="nes-your-name">
                            <label><strong>
                                <?php if(empty($this->nes_settings['name_label'])) : ?>
                                    <?php _e('Your Name','nes'); ?>:
                                <?php else :  ?>
                                    <?php echo $this->nes_settings['name_label']; ?>
                                <?php endif; ?>
                            </strong></label>
                            <input type="text" name="nes_event_name" value="<?php if(isset($_SESSION['nes']['event_name'])){echo stripslashes(sanitize_text_field($_SESSION['nes']['event_name']));} ?>" />
                        </div>

                        <div class="nes-phone">
                            <label><strong>
                                <?php if(empty($this->nes_settings['phone_label'])) : ?>
                                    <?php _e('Phone','nes'); ?>:
                                <?php else :  ?>
                                    <?php echo $this->nes_settings['phone_label']; ?>
                                <?php endif; ?>
                            </strong></label>
                            <input type="tel" class="nes-tel-input" name="nes_event_phone" value="<?php if(isset($_SESSION['nes']['event_phone'])){echo stripslashes(sanitize_text_field($_SESSION['nes']['event_phone']));} ?>"  />
                        </div>

                        <div class="nes-email">
                            <label><strong>
                                <?php if(empty($this->nes_settings['email_label'])) : ?>
                                    <?php _e('Email','nes'); ?>:
                                <?php else :  ?>
                                    <?php echo $this->nes_settings['email_label']; ?>
                                <?php endif; ?>
                            </strong></label>
                            <input type="email" name="nes_event_email" value="<?php if(isset($_SESSION['nes']['event_email'])){echo stripslashes(sanitize_text_field($_SESSION['nes']['event_email']));} ?>"  />
                            <span class="nes-clearer"></span>
                        </div>

                        <?php if(is_array($this->nes_settings['show_form_fields']) && in_array('website', $this->nes_settings['show_form_fields'])) : ?>
                            <div class="nes-website">
                                <label><strong>
                                    <?php if(empty($this->nes_settings['website_label'])) : ?>
                                        <?php _e('Website','nes'); ?>:
                                    <?php else :  ?>
                                        <?php echo $this->nes_settings['website_label']; ?>
                                    <?php endif; ?>
                                </strong></label>
                                <input type="url" name="nes_event_website" value="<?php if(isset($_SESSION['nes']['event_website'])){echo stripslashes(sanitize_text_field($_SESSION['nes']['event_website']));} ?>" />
                            </div>
                        <?php endif; ?>

                        <?php if(is_array($this->nes_settings['show_form_fields']) && in_array('type', $this->nes_settings['show_form_fields'])) : ?>
                            <div class="nes-send-to">
                                <label><strong>
                                    <?php if(empty($this->nes_settings['event_type_label'])) : ?>
                                         <?php printf(__('%1$s Type','nes'),$this->nes_settings['event_single']); ?>:
                                    <?php else :  ?>
                                        <?php echo $this->nes_settings['event_type_label']; ?>
                                    <?php endif; ?>
                                </strong></label>
                                <select class="event-send-to" name="nes_event_send_to" >
                                    <?php if($this->nes_settings['admin_email_two'] == '') : ?>
                                        <option value="one"><?php echo $this->nes_settings['admin_email_label_one']; ?></option>
                                    <?php else : ?>
                                        <option value="">- <?php _e('Choose One','nes'); ?> -</option>
                                        <option value="one"><?php echo $this->nes_settings['admin_email_label_one']; ?></option>
                                        <option value="two"><?php echo $this->nes_settings['admin_email_label_two']; ?></option>
                                    <?php endif; ?>
                                </select>
                            </div>
                        <?php else : ?>
                            <input class="event-send-to" type="hidden" name="nes_event_send_to" value="one" />
                        <?php endif; ?>

                        <?php $event_categories = get_terms('event_category', array('hide_empty' => false)); ?>
                        <?php if($event_categories) : ?>
                        <div class="nes-event-categories">
                            <label><strong><?php echo $this->nes_settings['event_single']; ?> Category</strong></label>
                            <select name="nes_event_category">
                                <?php foreach($event_categories as $event_category) : ?>
                                    <?php 
                                        $selected = '';
                                        if(isset($_SESSION['nes']['event_category']) && $_SESSION['nes']['event_category'] == $event_category->term_id){
                                            $selected = ' selected="selected"';
                                        } 
                                    ?>
                                    <option value="<?php echo $event_category->term_id; ?>"<?php echo $selected; ?>><?php echo $event_category->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <?php endif; ?>

                        <span class="nes-clearer"></span>

                        <?php if(is_array($this->nes_settings['show_form_fields']) && in_array('description', $this->nes_settings['show_form_fields'])) : ?>
                            <div class="nes-event-description">
                                <label><strong>
                                    <?php if(empty($this->nes_settings['description_label'])) : ?>
                                        <?php printf(__('%1$s Description','nes'),$this->nes_settings['event_single']); ?>
                                    <?php else :  ?>
                                        <?php echo $this->nes_settings['description_label']; ?>
                                    <?php endif; ?>
                                </strong></label>
                                <textarea name="nes_event_content" style="width:100%;" rows="6"><?php if(isset($_SESSION['nes']['event_content'])){echo stripslashes(sanitize_text_field($_SESSION['nes']['event_content']));} ?></textarea>
                            </div>
                        <?php endif; ?>

                        <span class="nes-clearer"></span>

                        <?php if(is_array($this->nes_settings['show_form_fields']) && in_array('setup_needs', $this->nes_settings['show_form_fields'])) : ?>
                            <div class="nes-setup-needs">
                                <label><strong>
                                    <?php if(empty($this->nes_settings['setup_needs_label'])) : ?>
                                        <?php _e('Setup Needs','nes'); ?>:
                                    <?php else :  ?>
                                        <?php echo $this->nes_settings['setup_needs_label']; ?>
                                    <?php endif; ?>
                                </strong></label>
                                <textarea name="nes_event_setup" style="width:100%;" rows="6"><?php if(isset($_SESSION['nes']['event_setup'])){echo stripslashes(sanitize_text_field($_SESSION['nes']['event_setup']));} ?></textarea>
                            </div>
                        <?php endif; ?>

                        <?php if(is_array($this->nes_settings['show_form_fields']) && in_array('av_needs', $this->nes_settings['show_form_fields'])) : ?>
                            <div class="nes-av-needs">
                                <label><strong>
                                    <?php if(empty($this->nes_settings['av_needs_label'])) : ?>
                                        <?php _e('A/V Tech Needs:','nes'); ?>
                                    <?php else :  ?>
                                        <?php echo $this->nes_settings['av_needs_label']; ?>
                                    <?php endif; ?>
                                </strong></label>
                                <textarea name="nes_event_av" style="width:100%;" rows="6"><?php if(isset($_SESSION['nes']['event_av'])){echo stripslashes(sanitize_text_field($_SESSION['nes']['event_av']));} ?></textarea>
                            </div>
                        <?php endif; ?>
                    
                        <span class="nes-clearer"></span> 
                        <div class="nes-sending-message" style="display:none;">Sending request... Please do not refresh this page <i class="fas fa-spinner fa-pulse"></i></div>
                        <div class="nes-event-submitted">
                            <input type="hidden" name="nes_date" value="<?php echo $date; ?>" />
                            <button id="nes-event-submitted" class="button" name="nes_event_submitted" value="1">Submit <?php echo $this->nes_settings['event_single']; ?> Request</button>
                        </div>
                        <span class="nes-clearer"></span>
                    </div>
                    <span class="nes-clearer"></span>
                </div>
                <span class="nes-clearer"></span>
            </form>

            <div class="nes-missing-fields nes-dialog" title="VALIDATION FAILED" style="display:none;">
                <h4>Please review the highlighted fields below:</h4>
                <ul class="nes-error-list"><li></li></ul>
            </div>

            <script type="text/javascript">
                jQuery(document).ready(function($){

                    // jquery datepickers       
                    $('#nes-frequency-end-datepicker').datepicker({
                        dateFormat: "mm/dd/yy",
                        altField: "#nes-frequency-end-date",
                        altFormat: "yy-mm-dd",
                        showOtherMonths: true,
                        showOn: "both", 
                        buttonText: "<i class='far fa-calendar'></i>",
                        onSelect: function(selectedDate){
                            $('#nes-frequency-end-date').trigger('change');
                        }
                    });

                    // multidatepicker
                    $('#nes-custom-datepicker').multiDatesPicker({
                        dateFormat: "mm/dd/yy",
                        altField: "#nes-custom-date",
                        altFormat: "yy-mm-dd",
                        showOtherMonths: true,
                        minDate: 0,
                        addDates: ["<?php echo date('m/d/Y', strtotime($date)); ?>"],
                        defaultDate: "<?php echo date('m/d/Y', strtotime($date)); ?>",
                        onSelect: function(selectedDate){
                            $('#nes-custom-date').trigger('change');
                            $('.nes-custom-date-display').html($('#nes-custom-date').val());
                        }
                    });
                    $('.nes-custom-date-display').html($('#nes-custom-date').val());

                   // toggle frequency type
                    $('input[name="nes_frequency_type"]').on('change',function(){                        

                        if($(this).val() != 'one'){
                            $('.nes-frequency').fadeIn('fast');
                        }else{
                            $('.nes-frequency').hide();
                        }

                        if($(this).val() == 'one'){
                            $('.nes-series-start-end').hide();
                            $('.nes-frequency-weekly').hide();
                            $('.nes-frequency-monthly').hide();
                        }else if($(this).val() == 'daily'){
                            $('.nes-series-start-end').fadeIn('fast');
                            $('.nes-series-start-end').removeClass('nes-custom');
                            $('.nes-frequency-end-date').fadeIn('fast');
                            $('.nes-custom-datepicker-wrap').hide();
                            $('.nes-frequency-weekly').hide();
                            $('.nes-frequency-monthly').hide();
                        }else if($(this).val() == 'weekly'){
                            $('.nes-series-start-end').fadeIn('fast');
                            $('.nes-series-start-end').removeClass('nes-custom');
                            $('.nes-frequency-end-date').fadeIn('fast');
                            $('.nes-custom-datepicker-wrap').hide();
                            $('.nes-frequency-weekly').fadeIn('fast');
                            $('.nes-frequency-monthly').hide();
                        }else if($(this).val() == 'monthly'){
                            $('.nes-series-start-end').fadeIn('fast');
                            $('.nes-series-start-end').removeClass('nes-custom');
                            $('.nes-custom-datepicker-wrap').hide();
                            $('.nes-frequency-end-date').fadeIn('fast');
                            $('.nes-frequency-weekly').fadeIn('fast');
                            $('.nes-frequency-monthly').fadeIn('fast');

                            if($('select[name=nes_frequency_monthly]').val()){
                                $('.nes-frequency-weekly').fadeIn('fast');
                            }else{
                                $('.nes-frequency-weekly').hide();
                            }
                        }else if($(this).val() == 'custom'){
                            $('.nes-series-start-end').fadeIn('fast');
                            $('.nes-series-start-end').addClass('nes-custom');
                            $('.nes-frequency-end-date').hide();
                            $('.nes-custom-datepicker-wrap').fadeIn('fast');
                            $('.nes-frequency-weekly').hide();
                            $('.nes-frequency-monthly').hide();
                        }
                    });  

                    // toggle weekly if monthly is empty
                    $('select[name=nes_frequency_monthly]').on('change',function(){
                        if($(this).val()){
                            $('.nes-frequency-weekly').fadeIn('fast');
                        }else{
                            $('.nes-frequency-weekly').hide();
                        }
                    });

                    // inititialize and include validation script
                    $('.nes-tel-input').intlTelInput({
                        utilsScript: "<?php echo $this->nes_settings['dir']; ?>/js/intl-tel-input/build/js/utils.js",
                        preferredCountries: ['us'],
                        initialCountry: 'us'
                    });



                    ////////// VALIDATION //////////////////
                    $(document).on('click', '#nes-event-submitted', function(e){

                        // clear previous errors
                        $('.nes-error-list').empty();
                        $('.nes-required-field').each(function(){
                            $(this).removeClass('nes-required-field');
                        });

                        // check for missing fields
                        var errors = new Array();

                        // venue name/address
                        if(!$('input[name=nes_offsite_venue_name]').val()){
                            $('input[name=nes_offsite_venue_name]').addClass('nes-required-field');
                            errors.push("<?php echo $this->nes_settings['venue_single']; ?>" + ' Name');
                        }                        
                        if(!$('input[name=nes_offsite_venue_address]').val()){
                            $('input[name=nes_offsite_venue_address]').addClass('nes-required-field');
                            errors.push("<?php echo $this->nes_settings['venue_single']; ?>" + ' Address');
                        }

                        // event time
                        if(!$('select[name=nes_start_time]').val()){
                            $('select[name=nes_start_time]').addClass('nes-required-field');
                            errors.push("<?php echo $this->nes_settings['event_single']; ?>" + ' Start Time');
                        }
                        if(!$('select[name=nes_end_time]').val()){
                            $('select[name=nes_end_time]').addClass('nes-required-field');
                            errors.push("<?php echo $this->nes_settings['event_single']; ?>" + ' End Time');
                        }

                        var start = $('select[name=nes_start_time]').val();
                        var end = $('select[name=nes_end_time]').val();

                        if(start >= end){
                            $('select[name=nes_start_time]').addClass('nes-required-field');
                            errors.push('Conflicting Start Time');
                        }            
                        if(end <= start){
                            $('select[name=nes_end_time]').addClass('nes-required-field');
                            errors.push('Conflicting End Time');
                        }                       

                        // recurring event fields
                        if($('input[name=nes_frequency_type]:checked').val() != 'one' && $('input[name=nes_frequency_type]:checked').val() != 'custom'){           
                            if(!$('input[name=nes_frequency_end_date]').val()){
                                $('#nes-frequency-end-datepicker').addClass('nes-required-field');
                                errors.push('Series End Date');
                            }
                        }

                        // event information fields
                        if(!$('input[name=nes_event_title]').val()){
                            $('input[name=nes_event_title]').addClass('nes-required-field');
                            errors.push("<?php echo $this->nes_settings['event_single']; ?>" + ' Title');
                        }                        
                        if(!$('input[name=nes_event_name]').val()){
                            $('input[name=nes_event_name]').addClass('nes-required-field');
                            errors.push('Your Name');
                        }                        
                        if(!$('input[name=nes_event_email]').val() || !nes_validate_email($('input[name=nes_event_email]').val())){
                            $('input[name=nes_event_email]').addClass('nes-required-field');
                            errors.push('Email Address');
                        }

                        // tel field validate
                        if($.trim($('.nes-tel-input').val())){
                            if(!$('.nes-tel-input').intlTelInput('isValidNumber')){
                                $('.nes-tel-input').addClass("nes-required-field");
                                errors.push('Phone Number');
                            }
                        }      

                        if(!$('.event-send-to').val()){
                            $('.event-send-to').addClass('nes-required-field');
                            errors.push('Event Type');
                        }

                        // generate validation error popup
                        if(errors.length > 0){
                            e.preventDefault();
                            $.each(errors, function(key, value){
                                $('.nes-error-list').append('<li>'+ value + '</li>');
                            });
                            $('.nes-missing-fields').dialog({
                                width:400,
                                modal:true,
                                buttons: [{
                                    text: "OK",
                                    click: function(){
                                        $(this).dialog("close");
                                    }
                                }]
                            });
                        }else{
                            $(this).hide();
                            $('.nes-sending-message').show();
                        }
                    });

                    function nes_validate_email(email){
                        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                        return re.test(email);
                    } 

                });
            </script>
            <?php
        }

        function nes_send_notification($to, $subject, $content){
            add_filter('wp_mail_content_type', function() {
                return "text/html";
            });
            $to = str_replace(' ', '', $to);
            $headers = 'From: "'.$this->nes_settings['from_email_name'].'" <'.$this->nes_settings['from_email_address'].'>' . "\r\n";
            $status = wp_mail($to, $subject, $content, $headers);
        }
 
        function nes_get_times($lower = 0, $upper = 23.75, $step = 1, $format = NULL){
            
			// set default format
			if($format === NULL){
                $format = 'H:i';
            }
			
			// convert time to decimal
			if(strpos($lower,':') !== false) {
				$lower = $this->nes_time_to_decimal($lower);
			}
			if(strpos($upper,':') !== false) {
				$upper = $this->nes_time_to_decimal($upper);
			}

			// build array
            $times = array();
            foreach(range($lower, $upper, $step) as $increment){
                $increment = number_format($increment, 2);
                list($hour, $minutes) = explode('.', $increment);
                $date = new DateTime($hour . ':' . $minutes * .6);
                $times[(string) $increment] = $date->format($format);
            }
            return $times;
        }
		
		function nes_time_to_decimal($time){
			$time_arr = explode(':', $time);
			$hour = intval($time_arr[0]);
			$minute = intval($time_arr[1])/60;
			$decimal = number_format($hour + $minute, 2);
			return $decimal;
		}

        function nes_decimal_to_minutes($decimal){
            return $decimal * 60;
        }
		
        function nes_get_time_select($name, $value = null, $id = null, $required = false, $start = false, $end = false, $interval = 0.25, $reverse = false, $empty = false){
            ob_start();
            ?>
            <select id="<?php echo $id; ?>" name="<?php echo $name; ?>">
            
                <?php 
                    $start_at = 0;
                    $end_at = 23.75;

                    if($start){
                        $time_arr = explode(':',$start);
                        $start_at = $time_arr[0] + ($time_arr[1]/60);
                    }
                    if($end){
                        $time_arr = explode(':',$end);
                        $end_at = $time_arr[0] + ($time_arr[1]/60);
                    }

                    $times = $this->nes_get_times($start_at,$end_at,$interval);
                    if($reverse){
                        arsort($times); 
                    }
                ?>
                <?php if($empty) : ?>
                    <option value=""> - </option>
                <?php endif; ?>
                <?php foreach ($times as $time) : ?>
                    <option value="<?php echo $time; ?>" <?php if($time == $value){echo 'selected';} ?>>
                        <?php echo date('g:i a', strtotime($time)); ?>
                    </option>
                <?php endforeach; ?>
            </select>
            <?php
            return ob_get_clean();
        }

        // ajax on event end time update
        function nes_ajax_get_time_select(){

            $name = $_POST['name'];
            $value = null;
            $id = null;
            $required = false;
            $start = $_POST['start'];
            $start_time = date('H:i',strtotime("+ 15 minutes", strtotime($start)));
            $end = false;
            $interval = 0.25;
            $reverse = false;
            ob_start();
            ?>
            <select id="<?php echo $id; ?>" name="<?php echo $name; ?>">
            
                <?php 
                    $start_at = 0;
                    $end_at = 23.75;

                    if($start_time){
                        $time_arr = explode(':',$start_time);
                        $start_at = $time_arr[0] + ($time_arr[1]/60);
                    }
                    if($end){
                        $time_arr = explode(':',$end);
                        $end_at = $time_arr[0] + ($time_arr[1]/60);
                    }

                    $times = $this->nes_get_times($start_at,$end_at,$interval);
                    if($reverse){
                        arsort($times); 
                    }
                ?>
                <?php foreach ($times as $time) : ?>
                    <option value="<?php echo $time; ?>" <?php if($time == $value){echo 'selected';} ?>>
                        <?php echo date('g:i a', strtotime($time)); ?>
                    </option>
                <?php endforeach; ?>
            </select>
            <?php
            echo ob_get_clean();
            die();
        }

        // export events from the backend
        function nes_export_event_schedule(){
            if(isset($_POST['nes_export_schedule_nonce']) && check_admin_referer('nes_export_schedule', 'nes_export_schedule_nonce')){
                
                // create a file pointer connected to the output stream
                $output = fopen('php://output', 'w');
                ob_start();

                // get fields
                $from = sanitize_text_field($_POST['nes_from_date']);
                $from_date = date('Y-m-d', strtotime($from));
                $to = sanitize_text_field($_POST['nes_to_date']);
                $to_date = date('Y-m-d', strtotime($to));
                $venue_id = sanitize_text_field($_POST['nes_default_venue']);

                global $wpdb;
                $query_string = "
                    SELECT DISTINCT p.ID
                    FROM $wpdb->posts p
                    LEFT JOIN $wpdb->postmeta m1 ON (p.ID = m1.post_id)
                    LEFT JOIN $wpdb->postmeta m2 ON (p.ID = m2.post_id)
                    WHERE p.post_type = 'nes_event'
                    AND p.post_status IN ('publish', 'past_events')
                    AND (m1.meta_key = 'nes_event_date' AND (m1.meta_value BETWEEN '$from_date' AND '$to_date'))
                    AND (m2.meta_key = 'nes_venue_id' AND m2.meta_value = '$venue_id')
                ";
    
                $events = $wpdb->get_results($query_string, OBJECT);

                if($events){
                    $row = array('Date', 'Start Setup Time', 'Start Time', 'End Time', 'End Cleanup Time', 'Location(s)', 'Title', 'Description', 'Setup Needs', 'A/V Needs', 'Event Status');
                    fputcsv($output, $row);
                    foreach($events as $event){
                        $event_id = $event->ID;
                        $date = get_post_meta($event_id, 'nes_event_date', true);
                        $start_setup_time = date('g:i a', strtotime(get_post_meta($event_id, 'nes_start_setup_time', true)));
                        $start_time = date('g:i a', strtotime(get_post_meta($event_id, 'nes_start_time', true)));
                        $end_time = date('g:i a', strtotime(get_post_meta($event_id, 'nes_end_time', true)));
                        $end_cleanup_time = date('g:i a', strtotime(get_post_meta($event_id, 'nes_end_cleanup_time', true)));
                       
                        // check if onsite
                        $event_type = get_post_meta($event_id, 'nes_event_type', true);
                        if($event_type == 'onsite'){

                            // get the locations
                            $locations = get_post_meta($event_id, 'nes_location_id', true);
                            $location_str = '';
                            $location_arr = array();
                            if($locations && is_array($locations)){
                                foreach($locations as $location){
                                    $location_arr[] = get_the_title($location);
                                }
                                $location_str = implode(', ', $location_arr);

                            }elseif($locations){
                                $location_str = get_the_title($locations);
                            }
                        }else{
                            $location_str = 'offsite';
                        }

                        $title = get_the_title($event_id);
                        $description = get_post_field('post_content', $event_id);
                        $setup =  get_post_meta($event_id, 'nes_event_setup', true);
                        $av =  get_post_meta($event_id, 'nes_event_av', true);
                        $status = get_post_meta($event_id, 'nes_event_status', true);

                        $row = array($date, $start_setup_time, $start_time, $end_time, $end_cleanup_time, $location_str, $title, $description, $setup, $av, $status);
                        fputcsv($output, $row);
                    }
                }

                $filename = 'nes_export_schedule_' . $from_date .'_' . $to_date;

                // Output CSV-specific headers
                header('Content-Encoding: UTF-8');
                header('Content-type: text/csv; charset=UTF-8');
                header("Content-Disposition: attachment; filename=\"$filename.csv\"");
                // Disable caching
                header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
                header("Pragma: no-cache"); // HTTP 1.0
                header("Expires: 0"); // Proxies
                
                $csv = ob_get_clean();
                exit($csv);

            }
            return;
        }        

        function nes_export_attendee_list(){
            if(isset($_POST['nes_export_attendee_list_nonce'])){
                // create a file pointer connected to the output stream
                $output = fopen('php://output', 'w');
                ob_start();

                // get event info
                $event_id = $_POST['nes_event_id'];
                $event_title = get_the_title($event_id);
                $event_date = date('l - F jS, Y', strtotime(get_post_meta($event_id, 'nes_event_date', true)));
                $event_start = date('g:ia', strtotime(get_post_meta($event_id, 'nes_start_time', true)));
                $event_end = date('g:ia', strtotime(get_post_meta($event_id, 'nes_end_time', true)));
                $event_date_time = $event_date . ' @ ' . $event_start . ' - ' . $event_end; 

                // get attendee list
                $attendees = get_post_meta($event_id, 'nes_attendees', true);

                if($attendees){
                    $row = array('Event Title', 'Event Date/Time', 'Date Purchased', 'Ticket Number', 'Purchaser', 'Attendee', 'Ticket Description', 'Ticket Price');
                    fputcsv($output, $row);
                    foreach($attendees as $attendee){
                        $row = array(
                            $event_title,
                            $event_date_time,
                            date('F jS, Y', substr($attendee['number'], 0, -4)),
                            '#'.$attendee['number'],
                            $attendee['purchaser'],
                            $attendee['attendee'],
                            $attendee['description'],
                            $attendee['price'],
                        );
                        fputcsv($output, $row);
                    }
                }

                $filename = 'nes_attendee_list_export';

                // Output CSV-specific headers
                header('Content-Encoding: UTF-8');
                header('Content-type: text/csv; charset=UTF-8');
                header("Content-Disposition: attachment; filename=\"$filename.csv\"");

                // Disable caching
                header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
                header("Pragma: no-cache"); // HTTP 1.0
                header("Expires: 0"); // Proxies
                
                $csv = ob_get_clean();
                exit($csv);

                return;
            }
            return;
        }

        function nes_google_calendar_export($post_id){

            // get start and end date/time
            $date = get_post_meta($post_id, 'nes_event_date', true);
            $start = get_post_meta($post_id, 'nes_start_time', true);
            $end = get_post_meta($post_id, 'nes_end_time', true);

            $start_date = strtotime($start . ' ' . $date);
            $end_date = strtotime($end . ' ' . $date);
            $dates = date('Ymd', $start_date) . 'T' . date('Hi00', $start_date) . '/' . date('Ymd', $end_date) . 'T' . date('Hi00', $end_date);
           
            // get location
            $address = $this->nes_get_venue_address($post_id);

            // get details
            $event_details = apply_filters('the_content', get_the_content($post_id));
            $event_details = str_replace('</p>', '</p> ', $event_details);
            $event_details = strip_tags($event_details);

            // get title
            $title = get_the_title($post_id);

            //Truncate Event Description and add permalink if greater than 996 characters
            if(strlen($event_details) > 996){

                $event_url = get_permalink($post_id);
                $event_details = substr($event_details, 0, 996);

                //Only add the permalink if it's shorter than 900 characters, so we don't exceed the browser's URL limits
                if(strlen($event_url) < 900){
                    $event_details .= ' (View Full Event Description Here: '.$event_url.')';
                }
            }

            $params = array(
                'action' => 'TEMPLATE',
                'text' => urlencode(strip_tags($title)),
                'dates' => $dates,
                'details' => urlencode($event_details),
                'location' => urlencode($address),
                'trp' => 'false',
                'sprop' => 'website:' . home_url(),
                'ctz' => urlencode(get_option('timezone_string'))
            );

            // return query string
            return esc_url(add_query_arg($params, 'http://www.google.com/calendar'));
        }


        function nes_ajax_vacancy_import(){

            // localize db connection 
            global $wpdb;

            // define vars
            $event_imported = 0;
            $venue_imported = 0;
            $location_imported = 0;

            // get all vacancy reservations that are not already imported to nessi
            $query_string = "
                SELECT p.*
                FROM $wpdb->posts AS p
                WHERE p.post_type = 'va_reservation'
                AND p.post_status = 'publish'
                AND NOT EXISTS (
                    SELECT p2.*
                    FROM $wpdb->posts AS p2
                    WHERE p2.post_type = 'nes_event'
                    AND (p.post_name = p2.post_name AND p2.post_name IS NOT NULL)
                    AND p2.post_status IN ('publish','past_events')
                )
                LIMIT 1
            ";
            $va_reservations = $wpdb->get_results($query_string, OBJECT);

            // check if query result
            if($va_reservations){

                // save "uncategorized" since no vacancy reservations had categories
                $exists = term_exists('Uncategorized', 'event_category');
                if($exists !== 0 && $exists !== null){
                    $term = wp_insert_term('Uncategorized', 'event_category', array('slug' => 'uncategorized'));
                }

                foreach($va_reservations as $va_reservation){

                    // set the reservation ID
                    $va_reservation_id = $va_reservation->ID;

                    // defer term and comment counting to improve insert performance
                    // wp_defer_term_counting(true);
                    // wp_defer_comment_counting(true);

                    $post_status = 'publish';
                    $va_event_date = get_post_meta($va_reservation_id, 'va_reservation_date', true);
                    if($va_event_date < date('Y-m-d', strtotime('now'))){
                        $post_status = 'past_events';
                    }

                    // create new nessie event
                    $nes_event_args = array(
                        'post_title' => $va_reservation->post_title,
                        'post_name' => $va_reservation->post_name,
                        'post_content' => $va_reservation->post_content,
                        'post_status' => $post_status,
                        'post_type' => 'nes_event'
                    );
                    $nes_event_id = wp_insert_post($nes_event_args);
                    wp_set_object_terms($nes_event_id, 'uncategorized', 'event_category');
                   
                    // increment
                    $event_imported++;

                    // get old venue and convert if necessary
                    $va_venue_id = get_post_meta($va_reservation_id, 'va_venue_id', true);                      

                    // get the vacancy venue
                    $va_venue = get_post($va_venue_id);

                    $nes_venue_exists = $wpdb->get_row("SELECT ID FROM $wpdb->posts WHERE post_name = '" . $va_venue->post_name . "' && post_type = 'nes_venue' ", 'ARRAY_N');
                    $va_nes_venue_equivalent_id = $nes_venue_exists[0];

                    if(!$nes_venue_exists){

                        // defer term and comment counting to improve insert performance
                        wp_defer_term_counting(true);
                        wp_defer_comment_counting(true);

                        // create new nessie venue
                        $nes_venue_args = array(
                            'post_title' => $va_venue->post_title,
                            'post_name' => $va_venue->post_name,
                            'post_content' => $va_venue->post_content,
                            'post_status' => $va_venue->post_status,
                            'post_type' => 'nes_venue'
                        );
                        $nes_venue_id = wp_insert_post($nes_venue_args);
                        $va_nes_venue_equivalent_id = $nes_venue_id;

                        // increment
                        $venue_imported++;
                        
                        // Update the new venue meta fields in the database.
                        update_post_meta($nes_venue_id, 'nes_address', get_post_meta($va_venue_id, 'va_address', true));
                        update_post_meta($nes_venue_id, 'nes_city', get_post_meta($va_venue_id, 'va_city', true));
                        update_post_meta($nes_venue_id, 'nes_state', get_post_meta($va_venue_id, 'va_state', true));
                        update_post_meta($nes_venue_id, 'nes_zipcode', get_post_meta($va_venue_id, 'va_zipcode', true));
                        update_post_meta($nes_venue_id, 'nes_country', get_post_meta($va_venue_id, 'va_country', true));
                        update_post_meta($nes_venue_id, 'nes_contact_email', get_post_meta($va_venue_id, 'va_contact_email', true));
                        update_post_meta($nes_venue_id, 'nes_phone', get_post_meta($va_venue_id, 'va_phone', true));
                        update_post_meta($nes_venue_id, 'nes_website', get_post_meta($va_venue_id, 'va_website', true));
                        update_post_meta($nes_venue_id, 'nes_venue_monday_start', get_post_meta($va_venue_id, 'va_venue_monday_start', true));
                        update_post_meta($nes_venue_id, 'nes_venue_monday_end', get_post_meta($va_venue_id, 'va_venue_monday_end', true));
                        update_post_meta($nes_venue_id, 'nes_venue_tuesday_start', get_post_meta($va_venue_id, 'va_venue_tuesday_start', true));
                        update_post_meta($nes_venue_id, 'nes_venue_tuesday_end', get_post_meta($va_venue_id, 'va_venue_tuesday_end', true));
                        update_post_meta($nes_venue_id, 'nes_venue_wednesday_start', get_post_meta($va_venue_id, 'va_venue_wednesday_start', true));
                        update_post_meta($nes_venue_id, 'nes_venue_wednesday_end', get_post_meta($va_venue_id, 'va_venue_wednesday_end', true));
                        update_post_meta($nes_venue_id, 'nes_venue_thursday_start', get_post_meta($va_venue_id, 'va_venue_thursday_start', true));
                        update_post_meta($nes_venue_id, 'nes_venue_thursday_end', get_post_meta($va_venue_id, 'va_venue_thursday_end', true));
                        update_post_meta($nes_venue_id, 'nes_venue_friday_start', get_post_meta($va_venue_id, 'va_venue_friday_start', true));
                        update_post_meta($nes_venue_id, 'nes_venue_friday_end', get_post_meta($va_venue_id, 'va_venue_friday_end', true));
                        update_post_meta($nes_venue_id, 'nes_venue_saturday_start', get_post_meta($va_venue_id, 'va_venue_saturday_start', true));
                        update_post_meta($nes_venue_id, 'nes_venue_saturday_end', get_post_meta($va_venue_id, 'va_venue_saturday_end', true));
                        update_post_meta($nes_venue_id, 'nes_venue_sunday_start', get_post_meta($va_venue_id, 'va_venue_sunday_start', true));
                        update_post_meta($nes_venue_id, 'nes_venue_sunday_end', get_post_meta($va_venue_id, 'va_venue_sunday_end', true));
                        update_post_meta($nes_venue_id, 'nes_google_map', 'yes');
                        update_post_meta($nes_venue_id, 'nes_venue_color', '74ace8');
                        update_post_meta($nes_venue_id, 'nes_venue_text_color', 'ffffff');
                    }

                    // get old locations and convert if necessary
                    $va_location_ids = get_post_meta($va_reservation_id, 'va_location_id', true);

                    if($va_location_ids){

                        // instantiate new location ids array
                        $nes_location_ids_arr = array();

                        foreach($va_location_ids as $va_location_id){

                            // get the vacancy location
                            $va_location = get_post($va_location_id);

                            // check if location already exists                     
                            $nes_location_exists = $wpdb->get_row("SELECT ID FROM $wpdb->posts WHERE post_name = '" . $va_location->post_name . "' && post_type = 'nes_location' ", 'ARRAY_N');
                            $va_nes_location_equivalent_id = $nes_location_exists[0];

                            if(!$nes_location_exists){

                                // defer term and comment counting to improve insert performance
                                wp_defer_term_counting(true);
                                wp_defer_comment_counting(true);

                                // create new nessie venue
                                $nes_venue_args = array(
                                    'post_title' => $va_location->post_title,
                                    'post_name' => $va_location->post_name,
                                    'post_content' => $va_location->post_content,
                                    'post_status' => $va_location->post_status,
                                    'post_type' => 'nes_location'
                                );
                                $nes_location_id = wp_insert_post($nes_venue_args);
                                $va_nes_location_equivalent_id = $nes_location_id;

                                // increment
                                $location_imported++;

                                // Update the new venue meta fields in the database.
                                update_post_meta($nes_location_id , 'nes_venue_id', $va_nes_venue_equivalent_id);
                                update_post_meta($nes_location_id , 'nes_venue_availability', get_post_meta($va_location_id, 'va_venue_availability', true));
                                update_post_meta($nes_location_id , 'nes_location_monday_start', get_post_meta($va_location_id, 'va_location_monday_start', true));
                                update_post_meta($nes_location_id , 'nes_location_monday_end', get_post_meta($va_location_id, 'va_location_monday_end', true));
                                update_post_meta($nes_location_id , 'nes_location_tuesday_start', get_post_meta($va_location_id, 'va_location_tuesday_start', true));
                                update_post_meta($nes_location_id , 'nes_location_tuesday_end', get_post_meta($va_location_id, 'va_location_tuesday_end', true));
                                update_post_meta($nes_location_id , 'nes_location_wednesday_start', get_post_meta($va_location_id, 'va_location_wednesday_start', true));
                                update_post_meta($nes_location_id , 'nes_location_wednesday_end', get_post_meta($va_location_id, 'va_location_wednesday_end', true));
                                update_post_meta($nes_location_id , 'nes_location_thursday_start', get_post_meta($va_location_id, 'va_location_thursday_start', true));
                                update_post_meta($nes_location_id , 'nes_location_thursday_end', get_post_meta($va_location_id, 'va_location_thursday_end', true));
                                update_post_meta($nes_location_id , 'nes_location_friday_start', get_post_meta($va_location_id, 'va_location_friday_start', true));
                                update_post_meta($nes_location_id , 'nes_location_friday_end', get_post_meta($va_location_id, 'va_location_friday_end', true));
                                update_post_meta($nes_location_id , 'nes_location_saturday_start', get_post_meta($va_location_id, 'va_location_saturday_start', true));
                                update_post_meta($nes_location_id , 'nes_location_saturday_end', get_post_meta($va_location_id, 'va_location_saturday_end', true));
                                update_post_meta($nes_location_id , 'nes_location_sunday_start', get_post_meta($va_location_id, 'va_location_sunday_start', true));
                                update_post_meta($nes_location_id , 'nes_location_sunday_end', get_post_meta($va_location_id, 'va_location_sunday_end', true));
                            }

                            $nes_location_ids_arr[] = $va_nes_location_equivalent_id;

                        }
                    }
                    
                    // copy over vacancy reservation meta to nessie event meta
                    update_post_meta($nes_event_id, 'nes_offsite_venue_name', '');
                    update_post_meta($nes_event_id, 'nes_offsite_venue_address', '');
                    update_post_meta($nes_event_id, 'nes_event_type', 'onsite');
                    update_post_meta($nes_event_id, 'nes_event_status', get_post_meta($va_reservation_id, 'va_reservation_status', true));
                    update_post_meta($nes_event_id, 'nes_venue_id', $va_nes_venue_equivalent_id);
                    update_post_meta($nes_event_id, 'nes_location_id', $nes_location_ids_arr);
                    update_post_meta($nes_event_id, 'nes_event_date', get_post_meta($va_reservation_id, 'va_reservation_date', true));
                    update_post_meta($nes_event_id, 'nes_start_setup_time', get_post_meta($va_reservation_id, 'va_start_setup_time', true));
                    update_post_meta($nes_event_id, 'nes_start_time', get_post_meta($va_reservation_id, 'va_start_time', true));
                    update_post_meta($nes_event_id, 'nes_end_time', get_post_meta($va_reservation_id, 'va_end_time', true));
                    update_post_meta($nes_event_id, 'nes_end_cleanup_time', get_post_meta($va_reservation_id, 'va_end_cleanup_time', true));
                    update_post_meta($nes_event_id, 'nes_event_name', get_post_meta($va_reservation_id, 'va_reservation_name', true));
                    update_post_meta($nes_event_id, 'nes_event_phone', get_post_meta($va_reservation_id, 'va_reservation_phone', true));
                    update_post_meta($nes_event_id, 'nes_event_email', get_post_meta($va_reservation_id, 'va_reservation_email', true));
                    update_post_meta($nes_event_id, 'nes_event_setup', get_post_meta($va_reservation_id, 'va_reservation_setup', true));
                    update_post_meta($nes_event_id, 'nes_event_av', get_post_meta($va_reservation_id, 'va_reservation_av', true));
                    update_post_meta($nes_event_id, 'nes_event_comments', get_post_meta($va_reservation_id, 'va_reservation_comments', true));

                    // check if previously was pushed to ECP (public)
                    $va_ecp = get_post_meta($va_reservation_id, 'va_push_to_ecp', true);
                    $nes_private_public = 'private';
                    if($va_ecp == 'on'){$nes_private_public = 'public';}
                    update_post_meta($nes_event_id, 'nes_private_public', $nes_private_public);
                }
            }

            // check if we need to run it again
            $again = 0;
            $query_string = "
                SELECT p.*
                FROM $wpdb->posts AS p
                WHERE p.post_type = 'va_reservation'
                AND p.post_status = 'publish'
                AND NOT EXISTS (
                    SELECT p2.*
                    FROM $wpdb->posts AS p2
                    WHERE p2.post_type = 'nes_event'
                    AND (p.post_name = p2.post_name AND p2.post_name IS NOT NULL)
                    AND p2.post_status IN ('publish','past_events')
                )
                LIMIT 1
            ";
            $va_reservations = $wpdb->get_results($query_string, OBJECT);
            if($va_reservations){$again = 1;}

            $results = array('again' => $again, 'events' => $event_imported, 'locations' => $location_imported, 'venues' => $venue_imported);
            echo json_encode($results);

            die();
        }


        function nes_ajax_ecp_import(){

            // localize db connection 
            global $wpdb;

            // define vars
            $event_imported = 0;

            // get all vacancy reservations that are not already imported to nessie
            $query_string = "
                SELECT p.*
                FROM $wpdb->posts AS p
                WHERE p.post_type = 'tribe_events'
                AND p.post_status = 'publish'
                AND NOT EXISTS (
                    SELECT p2.*                 
                    FROM $wpdb->posts AS p2
                    WHERE (p2.post_type = 'nes_event' OR p2.post_type = 'va_reservation')
                    AND (p.post_name = p2.post_name AND p2.post_name IS NOT NULL)
                    AND p2.post_status IN ('publish','past_events')
                ) 
                LIMIT 1
            ";
            $ecp_events = $wpdb->get_results($query_string, OBJECT);

            // check if query result
            if($ecp_events){

                // save "uncategorized" in case it doesn't already exist
                $exists = term_exists('Uncategorized', 'event_category');
                if($exists !== 0 && $exists !== null){
                    $term = wp_insert_term('Uncategorized', 'event_category', array('slug' => 'uncategorized'));  
                }

                foreach($ecp_events as $ecp_event){

                    // set the reservation ID
                    $ecp_event_id = $ecp_event->ID;

                    // defer term and comment counting to improve insert performance
                    // wp_defer_term_counting(true);
                    // wp_defer_comment_counting(true);

                    // check event date for post status 'publish' or 'past_events'
                    $post_status = 'publish';
                    $ecp_start = get_post_meta($ecp_event_id, '_EventStartDate', true);
                    $ecp_start_arr = explode(' ', $ecp_start);
                    if($ecp_start_arr[0] < date('Y-m-d', strtotime('now'))){
                        $post_status = 'past_events';
                    }

                    // create new nessie event
                    $nes_event_args = array(
                        'post_title' => $ecp_event->post_title,
                        'post_name' => $ecp_event->post_name,
                        'post_content' => $ecp_event->post_content,
                        'post_status' => $post_status,
                        'post_type' => 'nes_event'
                    );
                    $nes_event_id = wp_insert_post($nes_event_args);

                    // check for ECP categories
                    if($ecp_cats = get_the_terms($ecp_event_id, 'tribe_events_cat')){
                        $ecp_cat_arr = array();
                        foreach($ecp_cats as $ecp_cat){
                            $the_cat = $ecp_cat->term_id;
                            $term = term_exists($ecp_cat->name, 'event_category');
                            if($term !== 0 && $term !== null){
                                $the_cat_arr = wp_insert_term($ecp_cat->name, 'event_category', array('slug' => $ecp_cat->slug));  
                                $the_cat = $the_cat_arr['term_id'];
                            }
                            $ecp_cat_arr[] = $the_cat;
                        }
                        wp_set_object_terms($nes_event_id, $ecp_cat_arr, 'event_category');
                    }else{
                        wp_set_object_terms($nes_event_id, 'uncategorized', 'event_category');
                    }
                    
                    // increment
                    $event_imported++;

                    // get old venue and convert if necessary
                    $ecp_venue_id = get_post_meta($ecp_event_id, '_EventVenueID', true);                      

                    // get the vacancy venue
                    $ecp_venue = get_post($ecp_venue_id);

                    // set and maybe update
                    if($ecp_venue->post_title){
                        update_post_meta($nes_event_id, 'nes_offsite_venue_name', $ecp_venue->post_title);
                    }

                    // maybe update address
                    $ecp_address = get_post_meta($ecp_venue_id, '_VenueAddress', true);
                    $ecp_city = get_post_meta($ecp_venue_id, '_VenueCity', true);
                    $ecp_state = get_post_meta($ecp_venue_id, '_VenueState', true);
                    $ecp_zip = get_post_meta($ecp_venue_id, '_VenueZip', true);
                    $ecp_country = get_post_meta($ecp_venue_id, '_VenueCountry', true);  

                    if($ecp_address && $ecp_city && $ecp_state && $ecp_zip && $ecp_country){
                        $nes_offsite_venue_address = $ecp_address . ' ' . $ecp_city . ', ' . $ecp_state . ' ' . $ecp_zip . ' ' . $ecp_country;
                        update_post_meta($nes_event_id, 'nes_offsite_venue_address', $nes_offsite_venue_address);      
                    } 

                    // set nessie meta
                    update_post_meta($nes_event_id, 'nes_event_type', 'offsite');
                    update_post_meta($nes_event_id, 'nes_event_status', 'approved');
                    update_post_meta($nes_event_id, 'nes_private_public', 'public');
  
                    // parse ecp event start/end date/time
                    $ecp_start = get_post_meta($ecp_event_id, '_EventStartDate', true);
                    $ecp_start_arr = explode(' ', $ecp_start);
                    update_post_meta($nes_event_id, 'nes_event_date', $ecp_start_arr[0]);

                    $ecp_start_time_arr = explode(':', $ecp_start_arr[1]);
                    update_post_meta($nes_event_id, 'nes_start_setup_time', $ecp_start_time_arr[0] . ':' . $ecp_start_time_arr[1]);
                    update_post_meta($nes_event_id, 'nes_start_time', $ecp_start_time_arr[0] . ':' . $ecp_start_time_arr[1]);

                    $ecp_end = get_post_meta($ecp_event_id, '_EventEndDate', true);
                    $ecp_end_arr = explode(' ', $ecp_end);
                    $ecp_end_time_arr = explode(':', $ecp_end_arr[1]);
                    update_post_meta($nes_event_id, 'nes_end_time', $ecp_end_time_arr[0] . ':' . $ecp_end_time_arr[1]);
                    update_post_meta($nes_event_id, 'nes_end_cleanup_time', $ecp_end_time_arr[0] . ':' . $ecp_end_time_arr[1]);

                    // get the organizer
                    $ecp_organizer_id = get_post_meta($ecp_event_id, '_EventOrganizerID', true);
                    update_post_meta($nes_event_id, 'nes_event_name', get_post_meta($ecp_organizer_id, '_OrganizerOrganizer', true));
                    update_post_meta($nes_event_id, 'nes_event_phone', get_post_meta($ecp_organizer_id, '_OrganizerPhone', true));
                    update_post_meta($nes_event_id, 'nes_event_email', get_post_meta($ecp_organizer_id, '_OrganizerEmail', true));
                }
            }

            // check if we need to run it again
            $again = 0;
            $query_string = "
                SELECT p.*
                FROM $wpdb->posts AS p
                WHERE p.post_type = 'tribe_events'
                AND p.post_status = 'publish'
                AND NOT EXISTS (
                    SELECT p2.*                 
                    FROM $wpdb->posts AS p2
                    WHERE (p2.post_type = 'nes_event' OR p2.post_type = 'va_reservation')
                    AND (p.post_name = p2.post_name AND p2.post_name IS NOT NULL)
                    AND p2.post_status IN ('publish','past_events')
                ) 
                LIMIT 1
            ";
            $ecp_events = $wpdb->get_results($query_string, OBJECT);
            if($ecp_events){$again = 1;}

            $results = array('again' => $again, 'events' => $event_imported);
            echo json_encode($results);

            die();
        }

        function nes_ajax_ical_import(){

            // include ical reader
            require_once($this->nes_settings['path'] . 'inc/nes-ical-reader.php');

            $file = $_POST['ical_url'];
            $ical = new ICal($file);

            $events = $ical->events();      

            // define vars
            $again = 0;
            $event_imported = 0;
            $event_skipped = 0;
            $events_found = count($events);

            $current_event_count = $_POST['current_event_count'];

            // make event for current count
            if($ical_event = $events[$current_event_count]){

                // localize db connection 
                global $wpdb;

                // save "uncategorized" since we have no idea what to expect here
                $exists = term_exists('Uncategorized', 'event_category');
                if($exists !== 0 && $exists !== null){
                    $term = wp_insert_term('Uncategorized', 'event_category', array('slug' => 'uncategorized'));
                }

                // check for existing nessie event with this title
                $ical_title = $ical_event['SUMMARY'];
                $ical_event_date = date('Y-m-d', strtotime($ical_event['DTSTART']));
                $ical_start_time = date('H:i', strtotime($ical_event['DTSTART']));
                $ical_end_time = date('H:i', strtotime($ical_event['DTEND']));

                $query_string = "
                    SELECT p.*
                    FROM $wpdb->posts p
                    INNER JOIN $wpdb->postmeta m1 ON (p.ID = m1.post_id)
                    INNER JOIN $wpdb->postmeta m2 ON (p.ID = m2.post_id)                    
                    INNER JOIN $wpdb->postmeta m3 ON (p.ID = m3.post_id)
                    WHERE p.post_type = 'nes_event'
                    AND p.post_title = '$ical_title'
                    AND (m1.meta_key = 'nes_event_date' AND m1.meta_value = '$ical_event_date')                  
                    AND (m2.meta_key = 'nes_start_time' AND m2.meta_value = '$ical_start_time')
                    AND (m3.meta_key = 'nes_end_time' AND m3.meta_value = '$ical_end_time')
                ";

                $existing_events = $wpdb->get_results($query_string, OBJECT);

                if(!$existing_events){

                    // check if post_status 'publish' or 'past_events'
                    $post_status = 'publish';
                    if($ical_event_date < date('Y-m-d', strtotime('now'))){
                        $post_status = 'past_events';
                    }

                    // create new nessie event
                    $nes_event_args = array(
                        'post_title' => $ical_event['SUMMARY'],
                        'post_content' => nl2br($ical_event['DESCRIPTION']),
                        'post_status' => $post_status,
                        'post_type' => 'nes_event'
                    );
                    $nes_event_id = wp_insert_post($nes_event_args);
                    wp_set_object_terms($nes_event_id, 'uncategorized', 'event_category');
                   
                    // set nessie meta
                    update_post_meta($nes_event_id, 'nes_offsite_venue_name', $ical_event['LOCATION']);
                    update_post_meta($nes_event_id, 'nes_event_type', 'offsite');
                    update_post_meta($nes_event_id, 'nes_event_status', 'approved');
                    update_post_meta($nes_event_id, 'nes_private_public', 'public');
                    update_post_meta($nes_event_id, 'nes_frequency_type', 'one');
                    update_post_meta($nes_event_id, 'nes_event_date', $ical_event_date);
                    update_post_meta($nes_event_id, 'nes_start_setup_time', $ical_start_time);
                    update_post_meta($nes_event_id, 'nes_start_time', $ical_start_time);
                    update_post_meta($nes_event_id, 'nes_end_time', $ical_end_time);
                    update_post_meta($nes_event_id, 'nes_end_cleanup_time', $ical_end_time);

                    $event_imported = 1;

                }else{
                    $event_skipped = 1;
                }
            }

            $current_event_count++;

            if($current_event_count <= $events_found){$again = 1;}
            $results = array('again' => $again, 'events_found' => $events_found, 'current_event_count' => $current_event_count, 'event_imported' => $event_imported, 'event_skipped' => $event_skipped);

            echo json_encode($results);
            die();
        }


        function nes_event_background($content, $status){
            switch($status) {
                case 'pending':
                    $content = 'background-color:#'.$this->nes_settings['pending_color'];
                    break;                
                case 'approved':
                    $content = 'background-color:#'.$this->nes_settings['approved_color'];
                    break;                
                case 'denied':
                    $content = 'background-color:#'.$this->nes_settings['denied_color'];
                    break;
                default:
                    break;
            }
            return $content;
        }

        function nes_table_header_background($content){
            $content = ' style="background-color:#'.$this->nes_settings['table_header_color'].'"';
            return $content;
        }        

        function nes_unavailable_background($content, $status){
            switch ($status) {
                case 'not-available':
                    $content = ' style="background-color:#'.$this->nes_settings['unavailable_color'].'"';
                    break;
                default:
                    break;
            }
            return $content;
        }

        function nes_custom_excerpt($length = 150, $post_id, $linktext = "learn more") {
            $content = get_post_field('post_content', $post_id);
            if(!$content){
                $content = '';
            }
            $content = strip_tags($content);
            if(strlen($content) > $length) {
                while($content[$length] !== ' ') {
                    $length--;  
                }
                $content = substr($content, 0, $length); 
            }
            return $content . '... <a class="nes-read-more" href="'.get_permalink($post_id).'"> '. $linktext . '</a>'; 
        } 

        function nes_ordinal($number){
            $ends = array('th','st','nd','rd','th','th','th','th','th','th');
            if((($number % 100) >= 11) && (($number%100) <= 13)){
                return $number. 'th';
            }else{
                return $number. $ends[$number % 10];
            }
        }

        function nes_body_class($classes){
            global $post;
            if(isset($post) && !empty($post)){
                if((isset($post->post_content) && has_shortcode($post->post_content, 'nessie')) || ($this->nes_settings['event_calendar_page'] == $post->ID) || ($post->post_type == 'nes_event')){
                    $classes[] = 'nessie-page';
                }
            }
            return $classes;
        }

        function nes_get_path($file){
            return trailingslashit(dirname($file));
        }
        
        function nes_get_dir($file){
            $dir = trailingslashit(dirname($file));
            $count = 0;
            
            // sanitize for Win32 installs
            $dir = str_replace('\\' ,'/', $dir); 
            
            // if file is in plugins folder
            $wp_plugin_dir = str_replace('\\' ,'/', WP_PLUGIN_DIR); 
            $dir = str_replace($wp_plugin_dir, plugins_url(), $dir, $count);
            
            if($count < 1){
                // if file is in wp-content folder
                $wp_content_dir = str_replace('\\' ,'/', WP_CONTENT_DIR); 
                $dir = str_replace($wp_content_dir, content_url(), $dir, $count);
            }
           
            if($count < 1){
                // if file is in ??? folder
                $wp_dir = str_replace('\\' ,'/', ABSPATH); 
                $dir = str_replace($wp_dir, site_url('/'), $dir);
            }
            
            return $dir;
        }

        function nes_debug_log($msg, $title = ''){
            $error_dir = dirname(__file__).'/debug.log';
            $date = date('d.m.Y h:i:s');
            $msg = print_r($msg, true);
            $log = $title . "  |  " . $msg . "\n";
            error_log($log, 3, $error_dir);
        }

        // var dump for buildcreate only
        function bc_dump($val){
            $user = wp_get_current_user(); 
            if($user->user_login == 'buildcreate'){
                var_dump($val);
            }
        }

        function nes_screen_help($contextual_help, $screen_id, $screen){
         
            // The add_help_tab function for screen was introduced in WordPress 3.3.
            if ( ! method_exists( $screen, 'add_help_tab' ) )
                return $contextual_help;
         
            global $hook_suffix;
         
            // List screen properties
            $variables = '<ul style="width:50%;float:left;"> <strong>Screen variables </strong>'
                . sprintf( '<li> Screen id : %s</li>', $screen_id )
                . sprintf( '<li> Screen base : %s</li>', $screen->base )
                . sprintf( '<li>Parent base : %s</li>', $screen->parent_base )
                . sprintf( '<li> Parent file : %s</li>', $screen->parent_file )
                . sprintf( '<li> Hook suffix : %s</li>', $hook_suffix )
                . '</ul>';
         
            // Append global $hook_suffix to the hook stems
            $hooks = array(
                "load-$hook_suffix",
                "admin_print_styles-$hook_suffix",
                "admin_print_scripts-$hook_suffix",
                "admin_head-$hook_suffix",
                "admin_footer-$hook_suffix"
            );
         
            // If add_meta_boxes or add_meta_boxes_{screen_id} is used, list these too
            if ( did_action( 'add_meta_boxes_' . $screen_id ) )
                $hooks[] = 'add_meta_boxes_' . $screen_id;
         
            if ( did_action( 'add_meta_boxes' ) )
                $hooks[] = 'add_meta_boxes';
         
            // Get List HTML for the hooks
            $hooks = '<ul style="width:50%;float:left;"> <strong>Hooks </strong> <li>' . implode( '</li><li>', $hooks ) . '</li></ul>';
         
            // Combine $variables list with $hooks list.
            $help_content = $variables . $hooks;
         
            // Add help panel
            $screen->add_help_tab( array(
                'id'      => 'wptuts-screen-help',
                'title'   => 'Screen Information',
                'content' => $help_content,
            ));
         
            return $contextual_help;
        }


        function nes_add_dashboard_widgets(){

            // pending
            wp_add_dashboard_widget(
                'nes-pending-dashboard-widget', // Widget slug.
                'Pending Events | Nessie', // Title.
                array($this, 'nes_pending_dashboard_widget') // Display function.
            );              

            // upcoming
            wp_add_dashboard_widget(
                'nes-upcoming-dashboard-widget', // Widget slug.
                'Upcoming Events | Nessie', // Title.
                array($this, 'nes_upcoming_dashboard_widget') // Display function.
            ); 
        }

        // pending events dashboard widget
        function nes_pending_dashboard_widget(){

            global $wpdb;
            $now = date('Y-m-d');
            $query_string = "
                SELECT DISTINCT p.*
                FROM $wpdb->posts p
                LEFT JOIN $wpdb->postmeta m1 ON (p.ID = m1.post_id AND m1.meta_key = 'nes_event_status')
                LEFT JOIN $wpdb->postmeta m2 ON (p.ID = m2.post_id AND m2.meta_key = 'nes_event_date')
                LEFT JOIN $wpdb->postmeta m3 ON (p.ID = m3.post_id AND m3.meta_key = 'nes_start_time')
                WHERE p.post_type = 'nes_event'
                AND p.post_status = 'publish'
                AND m1.meta_value = 'pending'
                AND m2.meta_value >= '$now'
                ORDER BY m2.meta_value ASC, m3.meta_value ASC
                LIMIT 5
            ";

            // get events
            $events = $wpdb->get_results($query_string, OBJECT);
            if($events){
                echo '<ul class="nes-widget-list">';
                foreach($events as $event){
                    $post_id = $event->ID;
                    echo '<li class="nes-list-item">';
                        echo '<h5>';
                            echo '<a href="/wp-admin/post.php?post='.$post_id.'&action=edit">' . $event->post_title . '</a>';
                            if(get_post_meta($post_id,'nes_ticketed_event',true) == 'yes'){
                                echo ' <i class="fas fa-tags"></i>';
                            }
                        echo '</h5>';
                        echo '<p>' . date('F jS, Y', strtotime(get_post_meta($post_id, 'nes_event_date', true))) .' @ '. date('g:ia', strtotime(get_post_meta($post_id, 'nes_start_time', true))) .' - '. date('g:ia', strtotime(get_post_meta($post_id, 'nes_end_time', true))) .'</p>';
                        
                        // check if offsite
                        if(get_post_meta($post_id, 'nes_event_type',true) == 'offsite'){
                            // check if offsite name entered
                            if($offsite_name = get_post_meta($post_id,'nes_offsite_venue_name',true)){
                                echo '<p class="nes-venue-location">Offsite: '.$offsite_name.'</p>';
                            }
                       }else{
                            $venue_id = get_post_meta($post_id,'nes_venue_id',true);
                            if($venue_id){    
                                $location_ids = get_post_meta($post_id,'nes_location_id',true);
                                echo '<p class="nes-venue-location">Venue: <a href="/wp-admin/post.php?post='.$venue_id.'&action=edit">'.$this->nes_get_venue_name($venue_id).'</a>';
                                if($location_ids){
                                    echo '<br/>Location: ';
                                    $delimeter = '';
                                    foreach($location_ids as $location_id){
                                        echo $delimeter . '<a href="/wp-admin/post.php?post='.$location_id.'&action=edit">'.get_the_title($location_id).'</a>';
                                        $delimeter = ', ';
                                    }
                                }
                                echo '</p>';
                            }
                        }
                    echo '</li>';
                }
                echo '</ul>';
                echo '<p class="view-all-events"><a href="/wp-admin/edit.php?s&post_status=publish&post_type=nes_event&action=-1&m=0&nes_event_status_filter=pending&filter_action=Filter&paged=1&action2=-1">View All Pending Events <i class="far fa-angle-right"></i></a></p>';
                echo '<span class="nes-clearer"></span>';
            }else{
                echo '<p>Good news! No pending events were found.</p>';
            }
        }        

        // upcoming events dashboard widget
        function nes_upcoming_dashboard_widget(){

            global $wpdb;
            $now = date('Y-m-d');
            $query_string = "
                SELECT DISTINCT p.*
                FROM $wpdb->posts p
                LEFT JOIN $wpdb->postmeta m1 ON (p.ID = m1.post_id AND m1.meta_key = 'nes_event_status')
                LEFT JOIN $wpdb->postmeta m2 ON (p.ID = m2.post_id AND m2.meta_key = 'nes_event_date')
                LEFT JOIN $wpdb->postmeta m3 ON (p.ID = m3.post_id AND m3.meta_key = 'nes_start_time')
                LEFT JOIN $wpdb->postmeta m4 ON (p.ID = m4.post_id AND m4.meta_key = 'nes_private_public')
                WHERE p.post_type = 'nes_event'
                AND p.post_status = 'publish'
                AND m1.meta_value = 'approved'
                AND m2.meta_value >= '$now'
                AND m4.meta_value = 'public'
                ORDER BY m2.meta_value ASC, m3.meta_value ASC
                LIMIT 5
            ";

            // get events
            $events = $wpdb->get_results($query_string, OBJECT);
            if($events){
                echo '<ul class="nes-widget-list">';
                foreach($events as $event){
                    $post_id = $event->ID;
                    echo '<li class="nes-list-item">';
                        echo '<h5>';
                            echo '<a href="/wp-admin/post.php?post='.$post_id.'&action=edit">' . $event->post_title . '</a>';
                            if(get_post_meta($post_id,'nes_ticketed_event',true) == 'yes'){
                                echo ' <i class="fas fa-tags"></i>';
                            }
                        echo '</h5>';
                        echo '<p>' . date('F jS, Y', strtotime(get_post_meta($post_id, 'nes_event_date', true))) .' @ '. date('g:ia', strtotime(get_post_meta($post_id, 'nes_start_time', true))) .' - '. date('g:ia', strtotime(get_post_meta($post_id, 'nes_end_time', true))) .'</p>';
                        
                        // check if offsite
                        if(get_post_meta($post_id, 'nes_event_type',true) == 'offsite'){
                            // check if offsite name entered
                            if($offsite_name = get_post_meta($post_id,'nes_offsite_venue_name',true)){
                                echo '<p class="nes-venue-location">Offsite: '.$offsite_name.'</p>';
                            }
                        }else{
                            $venue_id = get_post_meta($post_id,'nes_venue_id',true);
                            if($venue_id){    
                                $location_ids = get_post_meta($post_id,'nes_location_id',true);
                                echo '<p class="nes-venue-location">Venue: <a href="/wp-admin/post.php?post='.$venue_id.'&action=edit">'.$this->nes_get_venue_name($venue_id).'</a>';
                                if($location_ids){
                                    echo '<br/>Location: ';
                                    $delimeter = '';
                                    foreach($location_ids as $location_id){
                                        echo $delimeter . '<a href="/wp-admin/post.php?post='.$location_id.'&action=edit">'.get_the_title($location_id).'</a>';
                                        $delimeter = ', ';
                                    }
                                }
                                echo '</p>';
                            }
                        }
                    echo '</li>';
                }
                echo '</ul>';
                echo '<p class="view-all-events"><a href="/wp-admin/edit.php?post_status=publish&post_type=nes_event">View All Upcoming Events <i class="far fa-angle-right"></i></a></p>';
                echo '<span class="nes-clearer"></span>';
            }else{
                echo '<p>Sorry, no upcoming events were found.</p>';
            }
        }
    }
    $nessie = new Nessie();



    // sidebar event listing widget
    class NessieList extends WP_Widget {
 
        public function __construct() {
            parent::__construct(
                'nes_events_list',
                __('Nessie Events List', 'nes'),
                array(
                    'classname'   => 'nes_events_list_widget',
                    'description' => __('A listing of upcoming events', 'nes')
                )
            );
        }
     
        public function widget($args, $instance){    
            global $nessie;
            global $wpdb;
            $now = date('Y-m-d');

            if(isset($args['before_widget'])){
              echo $args['before_widget'];  
            }
            
            if(!empty($instance['title'])){
                echo $args['before_title'] . apply_filters('widget_title', $instance['title']). $args['after_title'];
            }

            $num_events = 5;
            if(!empty($instance['num_events'])){
                $num_events = $instance['num_events'];
            }

            // check for categories
            if (isset($instance['event_cats']) && !empty($instance['event_cats'])) {
                $cat_ids = $instance['event_cats'];
                $cat_str = implode("','", $cat_ids);

                $query_string = "
                    SELECT DISTINCT p.*
                    FROM $wpdb->posts p
                    INNER JOIN $wpdb->term_relationships tr ON (p.ID = tr.object_id)
                    INNER JOIN $wpdb->term_taxonomy tt ON (tr.term_taxonomy_id = tt.term_taxonomy_id)
                    INNER JOIN $wpdb->terms t ON (t.term_id = tt.term_id)
                    LEFT JOIN $wpdb->postmeta m1 ON (p.ID = m1.post_id AND m1.meta_key = 'nes_event_date')
                    LEFT JOIN $wpdb->postmeta m2 ON (p.ID = m2.post_id AND m2.meta_key = 'nes_event_status')
                    LEFT JOIN $wpdb->postmeta m3 ON (p.ID = m3.post_id AND m3.meta_key = 'nes_start_time')
                    LEFT JOIN $wpdb->postmeta m4 ON (p.ID = m4.post_id AND m4.meta_key = 'nes_private_public')
                    WHERE p.post_type = 'nes_event'
                    AND p.post_status = 'publish'
                    AND (tt.taxonomy = 'event_category' AND t.term_id IN ('$cat_str'))
                    AND m1.meta_value >= '$now'
                    AND m2.meta_value = 'approved'
                    AND m4.meta_value = 'public'
                    ORDER BY m1.meta_value ASC, m3.meta_value ASC
                    LIMIT $num_events
                ";

            } else {
                
                $query_string = "
                    SELECT DISTINCT p.*
                    FROM $wpdb->posts p
                    LEFT JOIN $wpdb->postmeta m1 ON (p.ID = m1.post_id AND m1.meta_key = 'nes_event_status')
                    LEFT JOIN $wpdb->postmeta m2 ON (p.ID = m2.post_id AND m2.meta_key = 'nes_event_date')
                    LEFT JOIN $wpdb->postmeta m3 ON (p.ID = m3.post_id AND m3.meta_key = 'nes_start_time')
                    LEFT JOIN $wpdb->postmeta m4 ON (p.ID = m4.post_id AND m4.meta_key = 'nes_private_public')
                    WHERE p.post_type = 'nes_event'
                    AND p.post_status = 'publish'
                    AND m1.meta_value = 'approved'
                    AND m2.meta_value >= '$now'
                    AND m4.meta_value = 'public'
                    ORDER BY m2.meta_value ASC, m3.meta_value ASC
                    LIMIT $num_events
                ";
            }

            // get events
            $events = $wpdb->get_results($query_string, OBJECT);
            if($events){
                echo '<ul class="nes-widget-list">';
                foreach($events as $event){
                    $post_id = $event->ID;
                    echo '<li class="nes-list-item">';
                        echo '<h5>';
                            echo '<a href="'.get_the_permalink($post_id).'">' . $event->post_title . '</a>';
                            if(get_post_meta($post_id,'nes_ticketed_event',true) == 'yes'){
                                echo ' <i class="fas fa-tags"></i>';
                            }
                        echo '</h5>';
                        echo '<p>' . date('F jS, Y', strtotime(get_post_meta($post_id, 'nes_event_date', true))) .' @ '. date('g:ia', strtotime(get_post_meta($post_id, 'nes_start_time', true))) .' - '. date('g:ia', strtotime(get_post_meta($post_id, 'nes_end_time', true))) .'</p>';
                        
                        // check if offsite
                        if(get_post_meta($post_id, 'nes_event_type',true) == 'offsite'){
                            // check if offsite name entered
                            if($offsite_name = get_post_meta($post_id,'nes_offsite_venue_name',true)){
                                echo '<p class="nes-venue-location">Offsite: '.$offsite_name.'</p>';
                            }
                        }else{
                            $venue_id = get_post_meta($post_id,'nes_venue_id',true);
                            if($venue_id){    
                                $location_ids = get_post_meta($post_id,'nes_location_id',true);
                                echo '<p class="nes-venue-location">Venue: '. $nessie->nes_get_venue_name($venue_id);
                                if($location_ids){
                                    echo '<br/>Location: ';
                                    $delimeter = '';
                                    foreach($location_ids as $location_id){
                                        echo $delimeter . get_the_title($location_id);
                                        $delimeter = ', ';
                                    }
                                }
                                echo '</p>';
                            }
                        }
                    echo '</li>';
                }
                echo '</ul>';
                if($event_calendar_page_id = $nessie->nes_settings['event_calendar_page']){
                    echo '<p class="view-all-events"><a href="'. get_the_permalink($event_calendar_page_id) .'">View All Upcoming Events <i class="far fa-angle-right"></i></a></p>';
                    echo '<span class="nes-clearer"></span>';
                }
            }else{
                echo '<p>Sorry, no upcoming events were found.</p>';
            }
            if(isset($args['after_widget'])){
                echo $args['after_widget'];
            }

            wp_reset_postdata();
        } 

        public function form($instance){
            $title = !empty($instance['title']) ? $instance['title'] : '';
            $num_events = !empty($instance['num_events']) ? $instance['num_events'] : 5;
            $event_cats = !empty($instance['event_cats']) ? $instance['event_cats'] : array();
            ?>
            <p>
                <label for="<?php echo $this->get_field_id('title' ); ?>"><?php _e( 'Title:', 'nes'); ?></label> 
                <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr($title); ?>">
            </p>            
            <p>
                <label for="<?php echo $this->get_field_id('num_events'); ?>"><?php _e('Number of Events:', 'nes'); ?></label> 
                <input class="widefat" id="<?php echo $this->get_field_id('num_events'); ?>" name="<?php echo $this->get_field_name('num_events'); ?>" type="number" value="<?php echo esc_attr($num_events); ?>">
            </p>
            <?php $all_event_cats = get_terms('event_category',array('hide_empty' => false)); ?>
            <?php if($all_event_cats) : ?>
                <label><?php _e('Event Categories:', 'nes');?></label>
                <ul style="margin-top:5px;margin-bottom:20px;">
                <?php foreach($all_event_cats as $cat) : ?>
                    <li><label><input class="widefat" id="<?php echo $this->get_field_id('event_cats') . $cat->term_id; ?>" name="<?php echo $this->get_field_name('event_cats'); ?>[]" type="checkbox" value="<?php echo $cat->term_id; ?>" <?php if(is_array($event_cats) && in_array($cat->term_id, $event_cats)){echo 'checked="checked"';} ?>/> <?php echo $cat->name; ?></label></li>
                <?php endforeach; ?>
                </ul>
            <?php endif; ?>
     
            <?php 
        }

        public function update($new_instance, $old_instance){
            $instance = array();
            $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
            $instance['num_events'] = ( ! empty( $new_instance['num_events'] ) ) ? strip_tags( $new_instance['num_events'] ) : '';
            $instance['event_cats'] = $new_instance['event_cats'];
            return $instance;
        }
    }

    // alternative list style
    class NessieListAlt extends WP_Widget {
 
        public function __construct() {
            parent::__construct(
                'nes_events_list_alt',
                __('Nessie Events List Alt', 'nes'),
                array(
                    'classname'   => 'nes_events_list_widget',
                    'description' => __('A listing of upcoming events', 'nes')
                )
            );
        }
     
        public function widget($args, $instance){    
            global $nessie;
            global $wpdb;
            $now = date('Y-m-d');

            if(isset($args['before_widget'])){
              echo $args['before_widget'];  
            }
            
            if(!empty($instance['title'])){
                echo $args['before_title'] . apply_filters('widget_title', $instance['title']). $args['after_title'];
            }

            $num_events = 5;
            if(!empty($instance['num_events'])){
                $num_events = $instance['num_events'];
            }

            // check for categories
            if(isset($instance['event_cats'])){
                $cat_ids = $instance['event_cats'];
                $cat_str = implode("','", $cat_ids);

                $query_string = "
                    SELECT DISTINCT p.*
                    FROM $wpdb->posts p
                    INNER JOIN $wpdb->term_relationships tr ON (p.ID = tr.object_id)
                    INNER JOIN $wpdb->term_taxonomy tt ON (tr.term_taxonomy_id = tt.term_taxonomy_id)
                    INNER JOIN $wpdb->terms t ON (t.term_id = tt.term_id)
                    LEFT JOIN $wpdb->postmeta m1 ON (p.ID = m1.post_id AND m1.meta_key = 'nes_event_date')
                    LEFT JOIN $wpdb->postmeta m2 ON (p.ID = m2.post_id AND m2.meta_key = 'nes_event_status')
                    LEFT JOIN $wpdb->postmeta m3 ON (p.ID = m3.post_id AND m3.meta_key = 'nes_start_time')
                    LEFT JOIN $wpdb->postmeta m4 ON (p.ID = m4.post_id AND m4.meta_key = 'nes_private_public')
                    WHERE p.post_type = 'nes_event'
                    AND p.post_status = 'publish'
                    AND (tt.taxonomy = 'event_category' AND t.term_id IN ('$cat_str'))
                    AND m1.meta_value >= '$now'
                    AND m2.meta_value = 'approved'
                    AND m4.meta_value = 'public'
                    ORDER BY m1.meta_value ASC, m3.meta_value ASC
                    LIMIT $num_events
                ";

            }else{
                
                $query_string = "
                    SELECT DISTINCT p.*
                    FROM $wpdb->posts p
                    LEFT JOIN $wpdb->postmeta m1 ON (p.ID = m1.post_id AND m1.meta_key = 'nes_event_status')
                    LEFT JOIN $wpdb->postmeta m2 ON (p.ID = m2.post_id AND m2.meta_key = 'nes_event_date')
                    LEFT JOIN $wpdb->postmeta m3 ON (p.ID = m3.post_id AND m3.meta_key = 'nes_start_time')
                    LEFT JOIN $wpdb->postmeta m4 ON (p.ID = m4.post_id AND m4.meta_key = 'nes_private_public')
                    WHERE p.post_type = 'nes_event'
                    AND p.post_status = 'publish'
                    AND m1.meta_value = 'approved'
                    AND m2.meta_value >= '$now'
                    AND m4.meta_value = 'public'
                    ORDER BY m2.meta_value ASC, m3.meta_value ASC
                    LIMIT $num_events
                ";
            }

            // get events
            $events = $wpdb->get_results($query_string, OBJECT);
            if($events){
                echo '<div class="nes-widget-list nes-widget-list-alt">';
                $i=0;
                foreach($events as $event){
                    if ($i <= 2) :
                        $post_id = $event->ID;
                        echo '<div class="nes-list-item">';
                            $i++;
                            if ($i == 1) :
                                echo '<div class="nes-list-item-container odd">';
                            elseif ($i == 2) :
                                echo '<div class="nes-list-item-container even">';
                            elseif ($i == 3) :
                                echo '<div class="nes-list-item-container odd last">';
                            endif;
                                echo '<div class="left">';
                                    echo '<div class="date">';
                                        echo '<div class="date-container">';
                                            echo '<div class="day">'   . date('j', strtotime(get_post_meta($post_id, 'nes_event_date', true))) . '</div>';
                                            echo '<div class="month">' . date('M', strtotime(get_post_meta($post_id, 'nes_event_date', true))) . '</div>';
                                        echo '</div>';
                                    echo '</div>';
                                echo '</div>';
                                echo '<div class="right">';
                                    echo '<h5>';
                                        echo '<a href="'.get_the_permalink($post_id).'">' . $event->post_title . '</a>';
                                        if(get_post_meta($post_id,'nes_ticketed_event',true) == 'yes'){
                                            echo ' <i class="fas fa-tags"></i>';
                                        }
                                    echo '</h5>';
                                    
                                    // check if offsite
                                    if(get_post_meta($post_id, 'nes_event_type',true) == 'offsite'){
                                        // check if offsite name entered
                                        if($offsite_name = get_post_meta($post_id,'nes_offsite_venue_name',true)){
                                            echo '<p class="nes-venue-location">' .$offsite_name.'</p>';
                                        }
                                    }else{
                                        $venue_id = get_post_meta($post_id,'nes_venue_id',true);
                                        if($venue_id){    
                                            $location_ids = get_post_meta($post_id,'nes_location_id',true);
                                            echo '<p class="nes-venue-location">'. $nessie->nes_get_venue_name($venue_id) . '</p>';
                                        }
                                    }
                                    echo '<a class="view" href="' . get_the_permalink($post_id) . '">' . 'VIEW DETAILS' . '<i class="fa-regular fa-arrow-right"></i>' . '</a>';
                                echo '</div>';
                            echo '</div>';
                        echo '</div>';
                    endif;
                }
                echo '</div>';
                if($event_calendar_page_id = $nessie->nes_settings['event_calendar_page']){
                    echo '<a class="nes-widget-list-alt-see-all" href="' . site_url() . '/events/' . '">' . '<i class="fa-regular fa-calendar-day"></i>' . 'See all events' . '</a>';
                    echo '<span class="nes-clearer"></span>';
                }
            }else{
                echo '<p>Sorry, no upcoming events were found.</p>';
            }
            if(isset($args['after_widget'])){
                echo $args['after_widget'];
            }

            wp_reset_postdata();
        } 

        public function form($instance){
            $title = !empty($instance['title']) ? $instance['title'] : '';
            $num_events = !empty($instance['num_events']) ? $instance['num_events'] : 5;
            $event_cats = !empty($instance['event_cats']) ? $instance['event_cats'] : array();
            ?>
            <p>
                <label for="<?php echo $this->get_field_id('title' ); ?>"><?php _e( 'Title:', 'nes'); ?></label> 
                <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr($title); ?>">
            </p>            
            <p>
                <label for="<?php echo $this->get_field_id('num_events'); ?>"><?php _e('Number of Events:', 'nes'); ?></label> 
                <input class="widefat" id="<?php echo $this->get_field_id('num_events'); ?>" name="<?php echo $this->get_field_name('num_events'); ?>" type="number" value="<?php echo esc_attr($num_events); ?>">
            </p>
            <?php $all_event_cats = get_terms('event_category',array('hide_empty' => false)); ?>
            <?php if($all_event_cats) : ?>
                <label><?php _e('Event Categories:', 'nes');?></label>
                <ul style="margin-top:5px;margin-bottom:20px;">
                <?php foreach($all_event_cats as $cat) : ?>
                    <li><label><input class="widefat" id="<?php echo $this->get_field_id('event_cats') . $cat->term_id; ?>" name="<?php echo $this->get_field_name('event_cats'); ?>[]" type="checkbox" value="<?php echo $cat->term_id; ?>" <?php if(is_array($event_cats) && in_array($cat->term_id, $event_cats)){echo 'checked="checked"';} ?>/> <?php echo $cat->name; ?></label></li>
                <?php endforeach; ?>
                </ul>
            <?php endif; ?>
     
            <?php 
        }

        public function update($new_instance, $old_instance){
            $instance = array();
            $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
            $instance['num_events'] = ( ! empty( $new_instance['num_events'] ) ) ? strip_tags( $new_instance['num_events'] ) : '';
            $instance['event_cats'] = $new_instance['event_cats'];
            return $instance;
        }
    }
?>