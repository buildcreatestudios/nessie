<?php
    // update posted data 
    if(isset($_POST['nes_event_submitted'])){

        // include wordpress header
        require_once('../../../../wp-load.php');

        // get nessie class
        $nessie = new NESSIE();

        // sanitize data
        $venue_id = sanitize_text_field($_POST['nes_venue_id']);
        $location_ids = $_POST['nes_location_id'];
        $date = sanitize_text_field($_POST['nes_event_date_formatted']);
        $custom_dates = sanitize_text_field($_POST['nes_custom_date']);

        // remove event date from custom dates
        if($custom_dates){
            $custom_dates_arr = explode(', ', $custom_dates);

            if(is_array($custom_dates_arr)){
                // remove the start date if included
                foreach($custom_dates_arr as $k => $custom_date){
                    if($custom_date == date('m/d/Y', strtotime($date))){
                        unset($custom_dates_arr[$k]);
                        $custom_dates = implode(', ', $custom_dates_arr); 
                        break;
                    }
                }
            }
        }    

        $series_start_date = $date;
        $start_time = sanitize_text_field($_POST['nes_start_time']);
        $end_time = sanitize_text_field($_POST['nes_end_time']);
        $name = sanitize_text_field($_POST['nes_event_name']);
        $email = sanitize_text_field($_POST['nes_event_email']);
        $phone = sanitize_text_field($_POST['nes_event_phone']);
        $event_type = sanitize_text_field($_POST['nes_event_type']);
        $admin_notification_label = sanitize_text_field($_POST['nes_event_send_to']);
        $admin_notification_email = get_option('nes_admin_email_'.$admin_notification_label);
       

        // check for data only offsite has
        $private_public = 'public'; // default offsite to public
        if(isset($_POST['nes_private_public'])){
            $private_public = sanitize_text_field($_POST['nes_private_public']);
        }

        $offsite_venue_name = '';
        if(isset($_POST['nes_offsite_venue_name'])){
            $offsite_venue_name = sanitize_text_field($_POST['nes_offsite_venue_name']);
        }        
       
        $offsite_venue_address = '';
        if(isset($_POST['nes_offsite_venue_address'])){
            $offsite_venue_address = sanitize_text_field($_POST['nes_offsite_venue_address']);
        }
       
        // Recurring info
        $frequency_type = sanitize_text_field($_POST['nes_frequency_type']);

        $frequency_monthly = '';
        if(isset($_POST['nes_frequency_monthly'])){
            $frequency_monthly = sanitize_text_field($_POST['nes_frequency_monthly']);
        }

        $frequency_end_date = '';
        if(isset($_POST['nes_frequency_end_date'])){
            $frequency_end_date = sanitize_text_field($_POST['nes_frequency_end_date']);
        }

        $frequency_weekly = '';
        if(isset($_POST['nes_frequency_weekly'])){
            $frequency_weekly = $_POST['nes_frequency_weekly'];
        }

        // set weekly to event date day if none submitted
        if(empty($frequency_weekly)){
            $frequency_weekly = array(strtolower(date('l', strtotime($date))));
        }
        
        // get optional data
        if(is_array($nessie->nes_settings['show_form_fields']) && in_array('title',$nessie->nes_settings['show_form_fields'])){
            $title = sanitize_text_field($_POST['nes_event_title']);
        }else{
            $title = $name;
        }
        if(is_array($nessie->nes_settings['show_form_fields']) && in_array('description',$nessie->nes_settings['show_form_fields'])){
            $post_content = sanitize_text_field($_POST['nes_event_content']);
        }else{
            $post_content = '';
        }

        $start_setup_time = false;
        if(is_array($nessie->nes_settings['show_form_fields']) && in_array('setup_time',$nessie->nes_settings['show_form_fields'])){
            // check if setup time
            if(isset($_POST['nes_need_start_setup_time']) && sanitize_text_field($_POST['nes_need_start_setup_time']) == 'yes'){
                $start_setup_time = sanitize_text_field($_POST['nes_start_setup_time']);
            }
        }

        $end_cleanup_time = false;
        if(is_array($nessie->nes_settings['show_form_fields']) && in_array('cleanup_time',$nessie->nes_settings['show_form_fields'])){
            // check if cleanup time
            if(isset($_POST['nes_need_cleanup_time']) && sanitize_text_field($_POST['nes_need_cleanup_time']) == 'yes'){
                $end_cleanup_time = sanitize_text_field($_POST['nes_end_cleanup_time']);
            }
        }

        $setup_needs = '';
        if(is_array($nessie->nes_settings['show_form_fields']) && in_array('setup_needs',$nessie->nes_settings['show_form_fields'])){
            $setup_needs = sanitize_text_field($_POST['nes_event_setup']);
        }

        if(is_array($nessie->nes_settings['show_form_fields']) && in_array('website',$nessie->nes_settings['show_form_fields'])){
            $website = sanitize_text_field($_POST['nes_event_website']);
        }else {
            $website = '';
        }  

        if(is_array($nessie->nes_settings['show_form_fields']) && in_array('av_needs',$nessie->nes_settings['show_form_fields'])){
         $av_needs = sanitize_text_field($_POST['nes_event_av']);
        }else {
            $av_needs = '';
        }
        
        if($start_setup_time){
            $event_start = $start_setup_time;
        }else{
            $event_start = $start_time;
        }   

        if($end_cleanup_time){
            $event_end = $end_cleanup_time;
        }else{
            $event_end = $end_time;
        }

        // defer term and comment counting to improve insert performance
        wp_defer_term_counting(true);
        wp_defer_comment_counting(true);

        // create event post
        $post = array(
            'post_type' => 'nes_event',
            'post_title' => $title,
            'post_content' => $post_content,
            'post_status' => 'publish'
        );
        $post_id = wp_insert_post($post);

        // save event meta
        update_post_meta($post_id, 'nes_offsite_venue_name', $offsite_venue_name);
        update_post_meta($post_id, 'nes_offsite_venue_address', $offsite_venue_address);
        update_post_meta($post_id, 'nes_event_type', $event_type);
        update_post_meta($post_id, 'nes_event_status', 'pending');
        update_post_meta($post_id, 'nes_venue_id', $venue_id);
        update_post_meta($post_id, 'nes_location_id', $location_ids);
        update_post_meta($post_id, 'nes_event_date', $date);
        update_post_meta($post_id, 'nes_start_setup_time', $event_start);
        update_post_meta($post_id, 'nes_start_time', $start_time);
        update_post_meta($post_id, 'nes_end_time', $end_time);
        update_post_meta($post_id, 'nes_end_cleanup_time', $event_end);
        update_post_meta($post_id, 'nes_event_name', $name);
        update_post_meta($post_id, 'nes_event_phone', $phone);
        update_post_meta($post_id, 'nes_event_email', $email);
        update_post_meta($post_id, 'nes_event_website', $website);
        update_post_meta($post_id, 'nes_event_setup', $setup_needs);
        update_post_meta($post_id, 'nes_event_av', $av_needs);
        update_post_meta($post_id, 'nes_frequency_type', $frequency_type);
        update_post_meta($post_id, 'nes_frequency_monthly', $frequency_monthly);
        update_post_meta($post_id, 'nes_frequency_weekly', $frequency_weekly);
        update_post_meta($post_id, 'nes_frequency_end_date', $frequency_end_date);
        update_post_meta($post_id, 'nes_series_start_date', $series_start_date);
        update_post_meta($post_id, 'nes_private_public', $private_public);

        // get category
        if(isset($_POST['nes_event_category'])){
            $category_id = sanitize_text_field($_POST['nes_event_category']);
            wp_set_object_terms($post_id, array(intval($category_id)), 'event_category');

        }elseif(!wp_get_object_terms($post_id, 'event_category')){

            // save "uncategorized" if no categories where chosen
            $exists = term_exists('Uncategorized', 'event_category');
            if($exists === 0 || $exists === null){
                $term = wp_insert_term('Uncategorized', 'event_category', array('slug' => 'uncategorized'));
            }

            wp_set_object_terms($post_id, 'uncategorized', 'event_category');
        }

        // setup recurring series if necessary
        $dates = array();
        if($frequency_type != 'one'){

            // get dates
            $dates = $nessie->nes_get_available_dates($post_id, $venue_id, $location_ids, $date, $event_type, $frequency_type, $frequency_end_date, $frequency_weekly, $frequency_monthly, $custom_dates);

            // create new series
            $event_post = get_post($post_id);

            $nessie->nes_create_new_event_series($post_id, $event_post, $dates, $date);
        }

        // add start date to available dates
        array_unshift($dates, $date);

        // maybe send admin notifications
        if($nessie->nes_settings['admin_new_notification'] == 'yes'){
            $subject = sprintf(__('A new %1$s has been submitted for review','nes'),$nessie->nes_settings['event_single']);

            $email_header = get_option('nes_notification_header');
            if(empty($email_header)){
                $content = '<p>'.sprintf(__('The following %1$s information has been submitted','nes'),$nessie->nes_settings['event_single']).'. <a href="'.admin_url().'post.php?post='.$post_id.'&action=edit">Review this '.$nessie->nes_settings['event_single'].' now</a></p>';
            }else{
                $content = '<p>'.nl2br(get_option('nes_notification_header')).'</p>';
            }

            $content .= '<ul><li><strong>'.__('Title','nes').': </strong> '.get_the_title($post_id).'</li>';
            $content .= '<li><strong>'.$nessie->nes_settings['venue_single'].': </strong> '.get_the_title($venue_id).'</li>';
            $first = true;
            $location_names = '';
            if($location_ids){
                foreach($location_ids as $location){
                    if($first){
                        $first = false;
                        $location_names = get_the_title($location);
                    }else{
                        $location_names .= ', '.get_the_title($location);
                    }
                }
                $content .= '<li><strong>'.$nessie->nes_settings['location_plural'].': </strong> '.$location_names.'</li>';
            }

            if($dates){
                $content .= '<li><strong>'.__('Date(s)','nes').': </strong><ul>';
                foreach($dates as $date){
                    $content .= '<li>' . date('F jS, Y', strtotime($date)) . '</li>';
                }
                $content .= '</ul></li>';
            }


            if($start_setup_time){
                $content .= '<li><strong>'.__('Setup Start Time').': </strong> '.date('g:i a', strtotime($start_setup_time)).'</li>';
            }
            $content .= '<li><strong>'.$nessie->nes_settings['event_single'].' '.__('Start Time','nes').': </strong> '.date('g:i a', strtotime($start_time)).'</li>';
            $content .= '<li><strong>'.$nessie->nes_settings['event_single'].' '.__('End Time','nes').': </strong> '.date('g:i a', strtotime($end_time)).'</li>';
            if($end_cleanup_time){
                $content .= '<li><strong>'.__('Cleanup End Time','nes').': </strong> '.date('g:i a', strtotime($end_cleanup_time)).'</li>';
            }
            $content .= '</ul>';
            $content .= '<p>'.__('Contact Information','nes').':</p>';
            $content .= '<ul><li><strong>'.__('Name','nes').': </strong> '.$name.'</li>';
            $content .= '<li><strong>'.__('Phone','nes').': </strong> '.$phone.'</li>';
            $content .= '<li><strong>'.__('Email','nes').': </strong> '.$email.'</li>';
            $content .= '<li><strong>'.__('Website','nes').': </strong> '.$website.'</li>';
            $content .= '<li><strong>'.__('Set Up Need','nes').': </strong> '.$setup_needs.'</li>';
            $content .= '<li><strong>'.__('A/V Needs','nes').': </strong> '.$av_needs.'</li></ul>';
            $content .= '<p><strong>'.$nessie->nes_settings['event_single'].__(' Description','nes').':</strong></p>';
            $content .= '<ul><li>'.$post_content.'</li></ul>';
            $content .= '<p>'.nl2br(get_option('nes_notification_footer')).'</p>';

            // send notification
            $nessie->nes_send_notification($admin_notification_email, $subject, $content); 
            $content = '';
        }

        // maybe send user notification
        if($nessie->nes_settings['user_new_notification'] == 'yes'){
            if(empty($nessie->nes_settings['user_subject_line_new'])){
                 $subject = __('Thank you for submitting your ','nes').$nessie->nes_settings['event_single'].__(' request','nes').'!';
                 $subject = sprintf(__('Thank you for submitting your %1$s','nes'),$nessie->nes_settings['event_single']).'!';
            }else{
                $subject = $nessie->nes_settings['user_subject_line_new'];
            }

            $email_header = get_option('nes_notification_header');
            if(empty($email_header)){
                $content = '<p>'.sprintf(__('The following %1$s information has been submitted','nes'),$nessie->nes_settings['event_single']).' for review.</p>';
            }else{
                $content = '<p>'.nl2br(get_option('nes_notification_header')).'</p>';
            }
            
            // check for offsite
            if($event_type == 'offsite'){
                $content .= '<ul><li><strong>'.__('Title','nes').': </strong> '.$title.'</li>';
                $content .= '<li><strong>'.$nessie->nes_settings['venue_single'].': </strong> '.$offsite_venue_name.'</li>';
                $content .= '<li><strong>'.__('Address','nes').': </strong> '.$offsite_venue_address.'</li>';

            }else{
                $content .= '<ul><li><strong>'.__('Title','nes').': </strong> '.$title.'</li>';
                $content .= '<li><strong>'.$nessie->nes_settings['venue_single'].': </strong> '.get_the_title($venue_id).'</li>';

                $location_names = '';
                if($location_ids){
                        foreach($location_ids as $location){
                            $delimeter = '';
                            $location_names = $delimeter . get_the_title($location);
                            $delimeter = ', ';
                        }
                    
                    $content .= '<li><strong>'.$nessie->nes_settings['location_plural'].': </strong> '.$location_names.'</li>';
                }
            }

            if($dates){
                $content .= '<li><strong>'.__('Date(s)','nes').': </strong><ul>';
                foreach($dates as $date){
                    $content .= '<li>' . date('F jS, Y', strtotime($date)) . '</li>';
                }
                $content .= '</ul></li>';
            }

            // check if setup time exists
            if($start_setup_time){
                $content .= '<li><strong>'.__('Setup Start Time','nes').': </strong> '.date('g:i a', strtotime($start_setup_time)).'</li>';
            }
            $content .= '<li><strong>'.$nessie->nes_settings['event_single'].' '.__('Start Time','nes').': </strong> '.date('g:i a', strtotime($start_time)).'</li>';
            $content .= '<li><strong>'.$nessie->nes_settings['event_single'].' '.__('End Time','nes').': </strong> '.date('g:i a', strtotime($end_time)).'</li>';
            
            // check if cleanup time exists
            if($end_cleanup_time){
                $content .= '<li><strong>'.__('Cleanup End Time','nes').': </strong> '.date('g:i a', strtotime($end_cleanup_time)).'</li>';
            }                    

            // other event info
            $content .= '<li><strong>'.$nessie->nes_settings['event_single'].' privacy: </strong> '.$private_public.'</li>';

            if($setup_needs){
                $content .= '<li><strong>'.__('Set Up Needs','nes').': </strong> '.$setup_needs.'</li>';
            }                            
            if($av_needs){
                $content .= '<li><strong>'.__('A/V Needs','nes').': </strong> '.$av_needs.'</li>';
            }
            $content .= '</ul>';
            $content .= '<p><strong>'.$nessie->nes_settings['event_single'].' '.__('Description','nes').':</strong></p>';
            $content .= '<ul><li>'.$post_content.'</li></ul>';
            $content .= '<p>'.nl2br(get_option('nes_notification_footer')).'</p>';

            // send notification
            $nessie->nes_send_notification($email, $subject, $content); 
            $content = '';
        }

        // define message variable
        $message = '';

        // set on screen confirmation message to user. check if custom or not
        if($nessie->nes_settings['event_success_message'] != ''){
            $message .= $nessie->nes_settings['event_success_message'] . '<br/>';
        }else{
            $message .= sprintf(__('Your %1$s request(s) have been submitted as pending and will be reviewed shortly. You will be notified at the email address provided regarding status updates for this submission.','nes'),$nessie->nes_settings['event_single']).'<br/>';
        }

        // spit out dates
        if($dates){
            $message .= '<ul>';
            foreach($dates as $date){
                $message .= '<li>' . date('F jS, Y', strtotime($date)) . '</li>';
            }
            $message .= '</ul>';
        }

        // set email/message
        $_SESSION['nes']['message'] = $message;

        // set sessions for later use (users re-submitting reservations on the front end)
        if(isset($_POST['nes_event_title'])){
            $_SESSION['nes']['event_title'] = sanitize_text_field($_POST['nes_event_title']);
        }

        if(isset($_POST['nes_event_content'])){
            $_SESSION['nes']['event_content'] = sanitize_text_field($_POST['nes_event_content']);
        }

        if(isset($_POST['nes_event_name'])){
            $_SESSION['nes']['event_name'] = sanitize_text_field($_POST['nes_event_name']);
        }

        if(isset($_POST['nes_event_phone'])){
            $_SESSION['nes']['event_phone'] = sanitize_text_field($_POST['nes_event_phone']);
        }

        if(isset($_POST['nes_event_email'])){
            $_SESSION['nes']['event_email'] = sanitize_text_field($_POST['nes_event_email']);       
        }                

        if(isset($_POST['nes_event_website'])){
            $_SESSION['nes']['event_website'] = sanitize_text_field($_POST['nes_event_website']);
        }

        if(isset($_POST['nes_event_setup'])){
            $_SESSION['nes']['event_setup'] = sanitize_text_field($_POST['nes_event_setup']);
        }

        if(isset($_POST['nes_event_av'])){
            $_SESSION['nes']['event_av'] = sanitize_text_field($_POST['nes_event_av']);
        }        

        if(isset($_POST['nes_offsite_venue_name'])){
            $_SESSION['nes']['offsite_venue_name'] = sanitize_text_field($_POST['nes_offsite_venue_name']);
        }

        if(isset($_POST['nes_offsite_venue_address'])){
            $_SESSION['nes']['offsite_venue_address'] = sanitize_text_field($_POST['nes_offsite_venue_address']);
        }        

        if(isset($_POST['nes_event_category'])){
            $_SESSION['nes']['event_category'] = sanitize_text_field($_POST['nes_event_category']);
        }
    }

    // go back to nessie
    header('Location: '.$_SERVER["HTTP_REFERER"].'#nes');
?>