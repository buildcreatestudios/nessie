<?php 	
	// get the DB var
    global $wpdb;

    // set default timezone
    date_default_timezone_set(get_option('timezone_string'));

    // ical feed page
    $site_name = nes_ical_escape(get_option('nes_ical_feed_title'));
    $timezone_string = get_option('timezone_string');
    $filename = nes_make_filename_safe($site_name).'.ics';

    // start the ical
	$ical = "BEGIN:VCALENDAR".PHP_EOL;
	$ical .= "VERSION:2.0".PHP_EOL;
	$ical .= "PRODID:-//".$site_name."//NONSGML v1.0//EN".PHP_EOL;
	$ical .= "CALSCALE:GREGORIAN".PHP_EOL;
	$ical .= "METHOD:PUBLISH".PHP_EOL;
	$ical .= "X-WR-CALNAME:".$site_name.PHP_EOL;
	$ical .= "X-ORIGINAL-URL:".get_bloginfo('url').PHP_EOL;
	$ical .= "X-WR-CALDESC:Events for ".$site_name.PHP_EOL;

    // get/set params
    $date = htmlentities($_GET['date']);
    if(empty($date)){$date = date('Y-m-d');}

    $type = htmlentities($_GET['type']);
    if(empty($type)){$type = 'all';}    

    $id = htmlentities($_GET['id']);
    if(!empty($id)){$id = array($id);}

    // build events query based on ical type and date
    if($type == 'month'){

        $from_date = date('Y-m-d',strtotime($date));
        $next_month = date('Y-m-d',strtotime("$date + 1 months"));
        $to_date = date('Y-m-d',strtotime("$next_month - 1 days"));
        $query_string = "
            SELECT DISTINCT p.ID
            FROM $wpdb->posts p
            LEFT JOIN $wpdb->postmeta m1 ON p.ID = m1.post_id
            LEFT JOIN $wpdb->postmeta m2 ON p.ID = m2.post_id
            LEFT JOIN $wpdb->postmeta m3 ON p.ID = m3.post_id
            LEFT JOIN $wpdb->postmeta m4 ON (p.ID = m4.post_id AND m4.meta_key = 'nes_start_time')
            WHERE p.post_type = 'nes_event'
            AND p.post_status IN ('publish', 'past_events')
            AND (m1.meta_key = 'nes_event_date' AND (DATE(m1.meta_value) BETWEEN '$from_date' AND '$to_date'))
            AND (m2.meta_key = 'nes_event_status' AND m2.meta_value = 'approved')
            AND (m3.meta_key = 'nes_private_public' AND m3.meta_value = 'public')
            ORDER BY m1.meta_value ASC, m4.meta_value ASC
        ";


    }elseif($id){

        $ids = implode("','", $id);
        $query_string = "
            SELECT DISTINCT p.ID
            FROM $wpdb->posts p
            WHERE p.post_type = 'nes_event'
            AND p.ID IN ('$ids')
        ";
    }else{

        $now = date('Y-m-d',strtotime($date));
        $query_string = "
            SELECT DISTINCT p.ID
            FROM $wpdb->posts p
            LEFT JOIN $wpdb->postmeta m1 ON p.ID = m1.post_id
            LEFT JOIN $wpdb->postmeta m2 ON p.ID = m2.post_id
            LEFT JOIN $wpdb->postmeta m3 ON p.ID = m3.post_id
            LEFT JOIN $wpdb->postmeta m4 ON (p.ID = m4.post_id AND m4.meta_key = 'nes_start_time')
            WHERE p.post_type = 'nes_event'
            AND p.post_status IN ('publish', 'past_events')
            AND (m1.meta_key = 'nes_event_date' AND (m1.meta_value >= '$now'))
            AND (m2.meta_key = 'nes_event_status' AND m2.meta_value = 'approved')
            AND (m3.meta_key = 'nes_private_public' AND m3.meta_value = 'public')
            ORDER BY m1.meta_value ASC, m4.meta_value ASC
        ";
    }

    // do the query
    $event_ids = $wpdb->get_results($query_string, OBJECT);
    if($event_ids){
        foreach($event_ids as $event){
            $post_id = $event->ID;

            // get event post object
            $event = get_post($post_id);

            // get relevant date/times
            $date = get_post_meta($post_id,'nes_event_date',true);
            $start = get_post_meta($post_id,'nes_start_time',true);
            $end = get_post_meta($post_id,'nes_end_time',true);

            // make date/time
            $start_date = $start . ' ' . $date;
            $end_date = $end . ' ' . $date;

            // get venue
            $where = get_the_title(get_post_meta($post_id, 'nes_venue_id', true));  

            // check for locations              
            $location_ids = get_post_meta($post_id, 'nes_location_id', true);
            if($location_ids){
                $delimiter = ' - ';
                foreach($location_ids as $location_id){
                    $where .= $delimiter . get_the_title($location_id);
                    $delimiter = ', ';
                }
            }

			$ical .= "BEGIN:VEVENT".PHP_EOL;;
			$ical .= "DTSTART;TZID=".$timezone_string.":".nes_ical_date($start_date).PHP_EOL;
			$ical .= "DTEND;TZID=".$timezone_string.":".nes_ical_date($end_date).PHP_EOL;
			$ical .= "DTSTAMP:".date('Ymd\THis', time()).PHP_EOL;
			$ical .= "CREATED:".date('Ymd\THis', strtotime($event->post_date)).PHP_EOL;
			$ical .= "LAST-MODIFIED:".date('Ymd\THis', strtotime($event->post_modified)).PHP_EOL;
			$ical .= "UID:".$post_id."-".$start."-".$end."@".parse_url(home_url('/'), PHP_URL_HOST).PHP_EOL;
			$ical .= "SUMMARY:".str_replace(array(',',"\n","\r","\t"), array('\,','\n','','\t'), html_entity_decode(strip_tags($event->post_title), ENT_QUOTES)).PHP_EOL;
			$ical .= "DESCRIPTION:".str_replace(array(',',"\n","\r","\t"), array('\,','\n','','\t'), html_entity_decode(strip_tags($event->post_content), ENT_QUOTES)).PHP_EOL;
			$ical .= "URL:".get_permalink($post_id).PHP_EOL;
			$ical .= "LOCATION:".$where.PHP_EOL;
			
			// check for event categories
        	$terms = get_the_terms($post_id, 'event_category');
            if(!empty($terms)){
                $term_arr = array();
                foreach($terms as $term){
                    $term_arr[] = $term->name;
                }
				$ical .= "CATEGORIES:".implode(', ', $term_arr).PHP_EOL;      
            }
			
			$ical .= "END:VEVENT".PHP_EOL;
        }
   	}
	
	$ical .= "END:VCALENDAR".PHP_EOL;

	// write the file
	file_put_contents(ABSPATH.'/wp-content/uploads/'.$filename, $ical);

    // format ical date
    function nes_ical_date($timestamp){
        return date('Ymd\THis', strtotime($timestamp));
    }
    
    // escapes ical string
    function nes_ical_escape($string){
        return preg_replace('/([\,;])/','\\\$1', $string);
    }

    function nes_make_filename_safe($string){
        $string = preg_replace(array('/\s/', '/\.[\.]+/', '/[^\w_\.\-]/'), array('_', '.', ''), $string);
        return $string;
    }
?>

?>